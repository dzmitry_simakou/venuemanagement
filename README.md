# **Venue Management**

---

### 1. Installation

  1. Clone repository.
  2. Open **VM** solution in **src\VenueManagement** folder and build it.
  3. Open **VenueManagement.Automation** solution in **src\VenueManagement.Automation** folder and build it (Make sure you have up-to-date **Selenuim.WebDriver.ChromeDriver** and **Selenuim.WebDriver.GeckoDriver** nuget packages).
  4. Import VM.Services Certificate to **Current User** certifacates to **My** storage (Manage user certificates).
  5. Publish database project **VM.Database** to local SQL Server:

### 2. Configuration

  1. In Visual Studio set following projects as startup projects: **VM.Web**, **VM.Services**, **VM.Authentication** (right click on solution > Set StartUp Projects) for **VM** solution.
  2. Add Visual Studio extension named **Specflow for Visual Studio 2019** (Extensions > Manage Extensions) to have ability to run UI auto tests from **VenueManagement.Automation** solution.
  3. Using **Web.config** in **VM.Web** project you can specify connection string to your published **VM.Database**:

```xml
<connectionStrings>
  <add name="VmDatabase" connectionString="Server=.\SQLEXPRESS;Database=VM.Database;Trusted_Connection=True;" />
</connectionStrings>
```
and **basket expiry** period in seconds:

```xml
<appSettings>
  <add key="BasketExpiry" value="60" />
</appSettings>
```

  4. Using **App.config** in **VenueManagement.Tests** project you can specify connection string to your published **VM.Database**
  
```xml
<connectionStrings>
  <add name="VmDatabase" connectionString="Server=.\SQLEXPRESS;Database=VM.Database;Trusted_Connection=True;" />
</connectionStrings>
```

  and **main url**, **browser type** for selenium

```xml
<appSettings>
  <add key="BrowserType" value="Chrome"/>
  <add key="MainUrl" value="https://localhost:44393/"/>
</appSettings>
```

  You can only set **Chrome** or **Firefox** to **BrowserType** as only they are supported for now.

### 3. Debug and run

  1. Run existing unit and integration auto tests using **Test Explorer** to make sure everything's fine.
  2. Start.

### 4. UI auto tests

  1. Start **VM** solution projects as they were configured earlier.
  2. Run existing UI auto tests using **Test Explorer**.
