﻿Feature: EventManagementTests

	Create, update, delete Event

Scenario: 01 - Create Event
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "admin" and password: "password"
	And Home page is loaded
	And User clicks Create Event button
	And User creates event:
	| Field       | Value			 |
	| Name        | EventTest1		 |
	| Description | EventDesc2		 |
	| LayoutId	  | 2				 |
	| Date		  | 01/02/2024 23:00 |
	| Img		  | no_link			 |

	And User wants to update 3 event
	Then User validates event values on update page:
	| Field       | Value            |
	| Name        | EventTest1       |
	| Description | EventDesc2       |
	| LayoutId    | 2                |
	| Date        | 01/02/2024 23:00 |
	| Img         | no_link          |

Scenario: 02 - Update Event
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "admin" and password: "password"
	And Home page is loaded
	And User clicks Events header button
	And User wants to update 2 event
	And User updates event:
	| Field       | Value			 |
	| Name        | EventTest4		 |
	| Description | EventDesc4		 |
	| LayoutId	  | 3				 |
	| Date		  | 01/02/2026 23:00 |
	| Img		  | no_link			 |
	And User wants to update 2 event

	Then User validates event values on update page:
	| Field       | Value            |
	| Name        | EventTest4       |
	| Description | EventDesc4       |
	| LayoutId    | 3                |
	| Date        | 01/02/2026 23:00 |
	| Img         | no_link          |

Scenario: 03 - Delete Event
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "admin" and password: "password"
	And Home page is loaded
	And User clicks Events header button
	And User wants to delete 1 event
	And User deletes event
	Then There is no event with name "Event Name 1"