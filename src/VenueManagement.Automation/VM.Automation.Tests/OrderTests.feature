﻿Feature: OrderTests

	Buy a ticket

Scenario: 01 - Buy Ticket
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "admin" and password: "password"
	And Home page is loaded
	And User clicks Events header button
	When User wants to buy tickets for 1 event
	And User clicks button of area with id 1
	And User clicks button of seat with id 1
	And User clicks button of seat with id 2
	And User clicks button of seat with id 3
	And User clicks Basket button
	And User confirms seats in basket
	And User clicks History button
	Then There is history record with order id "1"