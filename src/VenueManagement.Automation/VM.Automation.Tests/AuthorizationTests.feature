﻿Feature: AuthorizationTests

	Authorization (positive and negative tests, with different roles)

Scenario: 01 - Anonymous role authorization
	Given User opens home page
	And User clicks Events header button
	When User wants to buy tickets for 2 event
	And User clicks button of area with id 3
	And User clicks button of seat with id 21
	And Login page is loaded
	Then Current URL contains "/Account/Login"

Scenario: 02 - User role authorization
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "user" and password: "password"
	And Home page is loaded
	Then User is logged

Scenario: 03 - Admin role authorization
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "admin" and password: "password"
	And Home page is loaded
	Then User is in Admin role

Scenario: 04 - User role has no Admin role rights
	Given User opens home page
	And User clicks Login header button
	When User logins with username: "user" and password: "password"
	And Home page is loaded
	Then User is logged
