﻿using System;
using TechTalk.SpecFlow;
using VM.Automation.Framework.Browsers;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.Pages.Common;
using VM.Automation.Tests.Helpers;

namespace VM.Automation.Tests.Configuration
{
    [Binding]
    public class Hooks
    {
        private IBrowser Browser => BrowserManager.Current;

        private static Uri HomeUrl = new Uri("https://localhost:44393/");

        private static void ConfigureBrowser()
        {
            BrowserManager.SetDefaultBrowserType();
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            ConfigureBrowser();
            DatabaseHelper.CreateSnapshot();
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
            BrowserManager.Current.Url = HomeUrl.ToString();
        }

        [AfterScenario]
        public static void AfterScenario()
        {
            new LayoutPage().Logoff();
        }

        [AfterFeature]
        public static void AfterFeature()
        {
            DatabaseHelper.RestoreSnapshot();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            BrowserManager.Kill();
            DatabaseHelper.RemoveSnapshot();
        }
    }
}