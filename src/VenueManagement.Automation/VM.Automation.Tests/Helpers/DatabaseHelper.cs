﻿using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace VM.Automation.Tests.Helpers
{
    public static class DatabaseHelper
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["VmDatabase"].ConnectionString;

        public static readonly string CreateSnapshotQuery = @"CREATE DATABASE VmSnapshot on (name='VM.Database', filename='" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\VmSnapshot.dat') AS SNAPSHOT of [VM.Database];";

        public static readonly string RestoreSnapshotQuery = @"USE master; RESTORE DATABASE [VM.Database] FROM DATABASE_SNAPSHOT = 'VmSnapshot' WITH REPLACE,RECOVERY";

        public static readonly string RemoveSnapshotQuery = @"DROP DATABASE IF EXISTS VmSnapshot";

        public static readonly string SetSingleUserQuery = @"alter database [VM.Database] set single_user with rollback immediate";

        public static readonly string SetMultiUserQuery = @"alter database [VM.Database] set MULTI_USER";

        private static void ExecuteNonQuery(string query)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(query, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public static void CreateSnapshot()
        {
            ExecuteNonQuery(CreateSnapshotQuery);
        }

        public static void RestoreSnapshot()
        {
            ExecuteNonQuery(SetSingleUserQuery);
            ExecuteNonQuery(RestoreSnapshotQuery);
            ExecuteNonQuery(SetMultiUserQuery);
        }

        public static void RemoveSnapshot()
        {
            ExecuteNonQuery(SetSingleUserQuery);
            ExecuteNonQuery(RemoveSnapshotQuery);
            ExecuteNonQuery(SetMultiUserQuery);
        }
    }
}