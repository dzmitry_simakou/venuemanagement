﻿using System;

namespace VM.Automation.Framework.Exceptions
{
    public class BrowserNotFoundException : Exception
    {
        public const string DefaultMessage = "Cannot find specified driver";

        public BrowserNotFoundException(string message) : base(message)
        {

        }
    }
}
