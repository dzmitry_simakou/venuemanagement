﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Header.Drawer
{
    public class SideMenuButton : WebElementWrapper
    {
        public SideMenuButton(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}