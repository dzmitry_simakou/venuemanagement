﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Header
{
    public class HeaderButton : WebElementWrapper
    {
        public HeaderButton(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}