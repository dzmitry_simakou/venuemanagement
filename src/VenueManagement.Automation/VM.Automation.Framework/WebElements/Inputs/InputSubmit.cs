﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Inputs
{
    public class InputSubmit : WebElementWrapper
    {
        public InputSubmit(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}