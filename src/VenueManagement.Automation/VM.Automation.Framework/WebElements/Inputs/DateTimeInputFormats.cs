﻿namespace VM.Automation.Framework.WebElements.Inputs
{
    internal static class RussianDateTimeInputFormats
    {
        public static string Date => "ddMMyyyy";

        public static string Time => "HHmm";
    }

    internal static class EnglishDateTimeInputFormats
    {
        public static string Date => "ddMMyyyy";

        public static string Time => "HHmm";
    }

    internal static class BelarussianDateTimeInputFormats
    {
        public static string Date => "ddMMyyyy";

        public static string Time => "HHmm";
    }
}
