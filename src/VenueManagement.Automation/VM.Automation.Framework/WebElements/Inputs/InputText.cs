﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements.Inputs
{
    public class InputText : WebElementWrapper, ITextInputWebElement
    {
        public InputText(IWebElement webElement)
        {
            WebElement = webElement;
        }

        public void SetValue(string value)
        {
            WebElement.Clear();
            WebElement.SendKeys(value);
        }
    }
}