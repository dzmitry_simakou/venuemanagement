﻿using OpenQA.Selenium;
using System;
using VM.Automation.Framework.Helpers;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements.Inputs
{
    public class InputTime : WebElementWrapper, ITextInputWebElement
    {
        public const string ValueFormat = "HH:mm";

        public InputTime(IWebElement webElement)
        {
            WebElement = webElement;
        }

        public void SetTime(int hour, int minute)
        {
            var format = LocaleHelper.GetLocaleTimeFormat();
            var dateString = new DateTime(1990, 01, 01, hour, minute, 0).ToString(format);

            SetValue(dateString);
        }

        public void SetValue(string text)
        {
            WebElement.SendKeys(text);
        }
    }
}