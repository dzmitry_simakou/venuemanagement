﻿using OpenQA.Selenium;
using System;
using VM.Automation.Framework.Helpers;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements.Inputs
{
    public class InputDate : WebElementWrapper, ITextInputWebElement
    {
        public const string ValueFormat = "yyyy-MM-dd";

        public InputDate(IWebElement webElement)
        {
            WebElement = webElement;
        }

        public void SetDate(int year, int month, int day)
        {
            var format = LocaleHelper.GetLocaleDateFormat();
            var dateString = new DateTime(year, month, day).ToString(format);

            SetValue(dateString);
        }

        public void SetValue(string text)
        {
            WebElement.SendKeys(text);
        }
    }
}