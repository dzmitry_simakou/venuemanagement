﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Inputs
{
    public class InputHidden : WebElementWrapper
    {
        public InputHidden(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}