﻿namespace VM.Automation.Framework.WebElements
{
    public static class WebElementAttributes
    {
        public static string Id = "id";

        public static string Name = "name";

        public static string Class = "class";

        public static string Source = "src";

        public static string Value = "value";

        public static string EventId = "event-id";

        public static string EventSeatId = "eventseat-id";

        public static string OrderId = "order-id";
    }
}