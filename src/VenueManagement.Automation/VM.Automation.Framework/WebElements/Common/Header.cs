﻿using OpenQA.Selenium;
using System;
namespace VM.Automation.Framework.WebElements.Common
{
    public class Header : WebElementWrapper
    {
        public Header(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}