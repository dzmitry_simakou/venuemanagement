﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements.Common
{
    public class Select : WebElementWrapper, ISelectListWebElement
    {
        private SelectElement _selectList;

        public Select(IWebElement webElement)
        {
            WebElement = webElement;
            _selectList = new SelectElement(WebElement);
        }

        public SelectOption SelectedItem => new SelectOption(_selectList.SelectedOption);

        public void SelectByIndex(int index)
        {
            _selectList.SelectByIndex(index);
        }

        public void SelectByName(string name)
        {
            _selectList.SelectByText(name);
        }

        public void SelectByValue(string value)
        {
            _selectList.SelectByValue(value);
        }
    }
}
