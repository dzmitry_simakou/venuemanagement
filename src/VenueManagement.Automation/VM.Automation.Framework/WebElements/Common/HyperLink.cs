﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Common
{
    public class HyperLink : WebElementWrapper
    {
        public HyperLink(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}