﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Common
{
    public class Button : WebElementWrapper
    {
        public Button(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}