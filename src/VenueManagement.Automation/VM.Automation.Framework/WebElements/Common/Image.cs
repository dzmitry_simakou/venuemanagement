﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements.Common
{
    public class Image : WebElementWrapper, IImageWebElement
    {
        public string Source => WebElement.GetAttribute(WebElementAttributes.Source);

        public Image(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}