﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Common
{
    public class ListItem : WebElementWrapper
    {
        public ListItem(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}
