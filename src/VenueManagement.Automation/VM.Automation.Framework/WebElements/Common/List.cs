﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.WebElements.Common
{
    public class List : WebElementWrapper
    {
        public List(IWebElement webElement)
        {
            WebElement = webElement;
        }
    }
}