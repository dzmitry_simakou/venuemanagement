﻿using OpenQA.Selenium;
using System.Drawing;
using VM.Automation.Framework.Helpers;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.WebElements
{
    public abstract class WebElementWrapper : IWebElementWrapper
    {
        public IWebElement WebElement { get; set; }

        public string Id => WebElement.GetAttribute(WebElementAttributes.Id);

        public string Class => WebElement.GetAttribute(WebElementAttributes.Class);

        public string TagName => WebElement.TagName;

        public Size Size => WebElement.Size;

        public bool IsPresent => !Size.IsEmpty;

        public string Value => WebElement.GetAttribute(WebElementAttributes.Value);

        public string Text => WebElement.Text;

        public void Click() => Wait.ThenExecute(() => WebElement.Click(), Wait.DefaultActionWaitMs, TimeUnit.Milliseconds);
    }
}