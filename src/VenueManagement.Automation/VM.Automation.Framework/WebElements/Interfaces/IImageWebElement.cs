﻿namespace VM.Automation.Framework.WebElements.Interfaces
{
    public interface IImageWebElement
    {
        string Source { get; }
    }
}