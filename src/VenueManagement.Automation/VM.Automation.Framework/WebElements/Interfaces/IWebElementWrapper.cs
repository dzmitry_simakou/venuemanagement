﻿using OpenQA.Selenium;
using System.Drawing;

namespace VM.Automation.Framework.WebElements.Interfaces
{
    public interface IWebElementWrapper
    {
        IWebElement WebElement { get; }

        string Id { get; }

        string Class { get; }

        string TagName{ get; }

        string Value { get; }

        string Text { get; }

        Size Size { get; }

        bool IsPresent { get; }

        void Click();
    }
}