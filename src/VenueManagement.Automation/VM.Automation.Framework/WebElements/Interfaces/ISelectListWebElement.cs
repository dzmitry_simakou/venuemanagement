﻿
using VM.Automation.Framework.WebElements.Common;

namespace VM.Automation.Framework.WebElements.Interfaces
{
    public interface ISelectListWebElement
    {
        void SelectByName(string name);

        void SelectByValue(string value);

        void SelectByIndex(int index);

        SelectOption SelectedItem { get; }
    }
}
