﻿namespace VM.Automation.Framework.WebElements.Interfaces
{
    public interface ITextInputWebElement
    {
        void SetValue(string text);
    }
}