﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VM.Automation.Framework.Browsers;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Helpers
{
    internal static class LocaleHelper
    {
        private static string MainUrl = ConfigurationManager.AppSettings["MainUrl"];

        private static string RussianUrl => $"{MainUrl}ru";

        private static string EnglishUrl => $"{MainUrl}en";

        private static string BelarussianUrl => $"{MainUrl}be";

        private static IDictionary<string, string> LocaleToUrlDictionary = new Dictionary<string, string>
        {
            { "ru", RussianUrl },
            { "be", BelarussianUrl },
            { "en", EnglishUrl }

        };

        private static IBrowser Browser => BrowserManager.Current;

        public static string GetLocale()
        {
            var currentUrl = BrowserManager.Current.Url;
            var locale = LocaleToUrlDictionary.SingleOrDefault(localeRoute => currentUrl.Contains(localeRoute.Value)).Key;
            if (string.IsNullOrEmpty(locale))
            {
                return Browser.GetUserLocale();
            }

            return locale;
        }

        public static string GetLocaleDateFormat()
        {
            var locale = GetLocale();
            switch (locale)
            {
                case "ru":
                {
                    return RussianDateTimeInputFormats.Date;
                }

                case "en":
                {
                    return EnglishDateTimeInputFormats.Date;
                }

                case "be":
                {
                    return BelarussianDateTimeInputFormats.Date;
                }
            }

            return null;
        }

        public static string GetLocaleTimeFormat()
        {
            var locale = GetLocale();
            switch (locale)
            {
                case "ru":
                {
                    return RussianDateTimeInputFormats.Time;
                }

                case "en":
                {
                    return EnglishDateTimeInputFormats.Time;
                }

                case "be":
                {
                    return BelarussianDateTimeInputFormats.Time;
                }
            }

            return null;
        }
    }
}