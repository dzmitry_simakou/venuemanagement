﻿using System;
using System.Threading;

namespace VM.Automation.Framework.Helpers
{
    public enum TimeUnit
    {
        Milliseconds,
        Seconds,
        Minutes,
        Hours,
        Days
    }

    public static class Wait
    {
        private const int DefaultWaitMs = 10000;

        private const int DefaultRetryInterval = 100;

        public const int DefaultActionWaitMs = 500;

        public static void For(int time, TimeUnit timeUnit)
        {
            TimeSpan timeSpan = default(TimeSpan);
            switch (timeUnit)
            {
                case TimeUnit.Milliseconds:
                {
                    timeSpan = new TimeSpan(0, 0, 0, 0, time);
                    break;
                }

                case TimeUnit.Seconds:
                {
                    timeSpan = new TimeSpan(0, 0, time);
                    break;
                }

                case TimeUnit.Minutes:
                {
                    timeSpan = new TimeSpan(0, time, 0);
                    break;
                }

                case TimeUnit.Hours:
                {
                    timeSpan = new TimeSpan(time, 0, 0);
                    break;
                }

                case TimeUnit.Days:
                {
                    timeSpan = new TimeSpan(time, 0, 0, 0);
                    break;
                }
            }

            Thread.Sleep(timeSpan);
        }

        public static void While(Func<bool> shouldRetry, int ms = DefaultWaitMs)
        {
            var startTime = DateTime.Now;
            while (DateTime.Now.Subtract(startTime).TotalMilliseconds < ms)
            {
                if (shouldRetry())
                {
                    System.Threading.Thread.Sleep(DefaultRetryInterval);
                }
                else
                {
                    break;
                }
            }
        }

        public static void ThenExecute(Action action, int time, TimeUnit timeUnit)
        {
            For(time, timeUnit);
            action.Invoke();
        }
    }
}