﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using VM.Automation.Framework.Browsers.Interfaces;

namespace VM.Automation.Framework.Browsers
{
    public class Browser : IBrowser
    {
        private IWebDriver _webDriver;

        private TimeSpan _defaultTimeout = TimeSpan.FromSeconds(5);

        public string Url 
        { 
            get => _webDriver.Url; 
            set => _webDriver.Url = value; 
        }

        private IJavaScriptExecutor AsJavaScriptExecutor() => _webDriver as IJavaScriptExecutor;

        public string Title => _webDriver.Title;

        public void WaitPageLoaded()
        {
            var wait = new WebDriverWait(_webDriver, _defaultTimeout);
            wait.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void WaitElementLoaded(By by)
        {
            var wait = new WebDriverWait(_webDriver, _defaultTimeout);
            wait.Until(driver => driver.FindElement(by));
        }

        public string GetUserLocale()
        {
            var script = "return window.navigator.language";
            return AsJavaScriptExecutor().ExecuteScript(script).ToString().Substring(0, 2);
        }

        public Browser(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public IWebElement Find(By by)
        {
            return _webDriver.FindElement(by);
        }

        public IEnumerable<IWebElement> FindAll(By by)
        {
            return _webDriver.FindElements(by);
        }

        public void Kill()
        {
            _webDriver.Quit();
            _webDriver.Dispose();
        }
    }
}