﻿namespace VM.Automation.Framework.Browsers.Enums
{
    public enum BrowserType
    {
        Chrome,
        Firefox
    }
}
