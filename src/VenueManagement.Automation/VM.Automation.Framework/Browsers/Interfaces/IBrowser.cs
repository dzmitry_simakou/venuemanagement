﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace VM.Automation.Framework.Browsers.Interfaces
{
    public interface IBrowser
    {
        IWebElement Find(By by);

        IEnumerable<IWebElement> FindAll(By by);

        string Url { get; set; }

        string Title { get; }

        string GetUserLocale();

        void WaitPageLoaded();

        void WaitElementLoaded(By by);

        void Kill();
    }
}