﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Configuration;
using System.IO;
using VM.Automation.Framework.Browsers.Enums;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.Exceptions;

namespace VM.Automation.Framework.Browsers
{
    public static class BrowserFactory
    {
        private static string ExecutionPath => Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        private static string ChromeDriverPath => ConfigurationManager.AppSettings.Get("ChromeDriverPath") ?? ExecutionPath;

        private static string FirefoxDriverPath => ConfigurationManager.AppSettings.Get("FirefoxDriverPath") ?? ExecutionPath;

        private static string ConfigBrowserType => ConfigurationManager.AppSettings.Get("BrowserType");

        public static BrowserType DefaultBrowserType = BrowserType.Chrome;

        private static IWebDriver GetChromeDriver()
        {
            var options = new ChromeOptions 
            {
                // Some options if necessary
            };

            return new ChromeDriver(ChromeDriverPath, options);
        }

        private static IWebDriver GetFirefoxDriver()
        {
            var options = new FirefoxOptions
            {
                // Some options if necessary
            };

            return new FirefoxDriver(FirefoxDriverPath, options);
        }

        public static IBrowser GetBrowser()
        {
            if(ConfigBrowserType != null)
            {
                Enum.TryParse(ConfigBrowserType, out BrowserType browserType);
                return GetBrowser(browserType);
            }

            return GetBrowser(DefaultBrowserType);
        }

        public static IBrowser GetBrowser(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                {
                    return new Browser(GetChromeDriver());
                }

                case BrowserType.Firefox:
                {
                    return new Browser(GetFirefoxDriver());
                }

                default:
                {
                    throw new BrowserNotFoundException(BrowserNotFoundException.DefaultMessage);
                }
            }
        }
    }
}