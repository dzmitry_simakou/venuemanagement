﻿using VM.Automation.Framework.Browsers.Enums;
using VM.Automation.Framework.Browsers.Interfaces;

namespace VM.Automation.Framework.Browsers
{
    public static class BrowserManager
    {
        public static IBrowser _current;

        public static IBrowser Current 
        {
            get
            {
                if(_current == null)
                {
                    SetDefaultBrowserType();
                }

                return _current;
            }

            private set
            {
                if (_current != null)
                {
                    _current.Kill();
                }
                
                _current = value;
            }
        }

        public static void SetDefaultBrowserType()
        {
            Current = BrowserFactory.GetBrowser();
        }

        public static void SetBrowserType(BrowserType browserType)
        {
            Current = BrowserFactory.GetBrowser(browserType);
        }

        public static void Kill()
        {
            Current.Kill();
        }
    }
}