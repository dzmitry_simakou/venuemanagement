﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Account
{
    public class LoginPage : BasePage
    {
        public InputText UserNameInputText => Get<InputText>(By.Id("UserName"));

        public InputText PasswordInputText => Get<InputText>(By.Id("Password"));

        public InputSubmit LoginInputButton => Get<InputSubmit>(By.Id("login-submit"));

        public void Login(string login, string password)
        {
            UserNameInputText.SetValue(login);
            PasswordInputText.SetValue(password);
            LoginInputButton.Click();
        }

        public override void WaitForLoad()
        {
            Browser.WaitElementLoaded(By.Id("login-submit"));
        }
    }
}