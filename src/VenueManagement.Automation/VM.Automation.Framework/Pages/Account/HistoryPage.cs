﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using VM.Automation.Framework.WebElements;
using VM.Automation.Framework.WebElements.Common;

namespace VM.Automation.Framework.Pages.Account
{
    public class HistoryPage : BasePage
    {
        public By HistoryRecordListSelector = By.CssSelector("ul.mdl-list");

        public IEnumerable<List> HistoryRecordLists => GetAll<List>(HistoryRecordListSelector);

        public List GetHistoryRecordByOrderId(string id)
        {
            return HistoryRecordLists.SingleOrDefault(list => list.WebElement.GetAttribute(WebElementAttributes.OrderId).Equals(id));
        }

        public string GetHistoryRecordTitleByOrderId(int id)
        {
            var selector = By.CssSelector($"ul.mdl-list[order-id='{id}'] > li:first-child");

            return Get<ListItem>(selector).Text;
        }

        public override void WaitForLoad()
        {
            var footerSelector = By.ClassName("mdl-mega-footer");

            Browser.WaitElementLoaded(footerSelector);
        }
    }
}