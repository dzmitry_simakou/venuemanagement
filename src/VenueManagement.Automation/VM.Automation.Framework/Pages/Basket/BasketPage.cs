﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Basket
{
    public class BasketPage : BasePage
    {
        public InputSubmit ConfirmBasketInputSubmit => Get<InputSubmit>(By.Id("basket-confirm"));
    }
}