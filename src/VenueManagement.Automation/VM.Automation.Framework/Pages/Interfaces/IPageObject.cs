﻿using OpenQA.Selenium;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.Pages.Interfaces
{
    public interface IPageObject
    {
        IBrowser Browser { get; }

        T Get<T>(By by) where T : IWebElementWrapper;

        void WaitForLoad();
    }
}