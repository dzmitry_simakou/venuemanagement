﻿using OpenQA.Selenium;

namespace VM.Automation.Framework.Pages.Home
{
    public class HomePage : BasePage
    {

        public By HomePage_H2_HeaderSelector = By.XPath("//h2");

        public override void WaitForLoad()
        {
            Browser.WaitElementLoaded(HomePage_H2_HeaderSelector);
        }
    }
}