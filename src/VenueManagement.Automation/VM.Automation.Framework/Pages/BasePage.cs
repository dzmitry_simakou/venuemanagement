﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VM.Automation.Framework.Browsers;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.Pages.Interfaces;
using VM.Automation.Framework.WebElements.Interfaces;

namespace VM.Automation.Framework.Pages
{
    public abstract class BasePage : IPageObject
    {
        public IBrowser Browser => BrowserManager.Current;

        private static string MainUrl = ConfigurationManager.AppSettings["MainUrl"];

        public void GoHome() => Browser.Url = MainUrl;

        public virtual T Get<T>(By by) where T : IWebElementWrapper
        {
            var element = Browser.FindAll(by).FirstOrDefault();
            if (element == null)
            {
                return default(T);
            }

            return (T)Activator.CreateInstance(typeof(T), element);
        }

        public virtual IEnumerable<T> GetAll<T>(By by) where T : IWebElementWrapper
        {
            var elements = Browser.FindAll(by);
            if (elements.Any())
            {
                foreach (var element in elements)
                {
                    yield return (T)Activator.CreateInstance(typeof(T), element);
                }
            }
        }

        public virtual void WaitForLoad()
        {

        }
    }
}