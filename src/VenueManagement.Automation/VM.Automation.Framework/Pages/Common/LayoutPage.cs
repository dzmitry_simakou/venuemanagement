﻿using OpenQA.Selenium;
using VM.Automation.Framework.Pages.Home;
using VM.Automation.Framework.WebElements.Header;
using VM.Automation.Framework.WebElements.Header.Drawer;

namespace VM.Automation.Framework.Pages.Common
{
    public class LayoutPage : BasePage
    {
        public HomePage HomePage => new HomePage(); 

        public HeaderButton HomeHeaderButton => Get<HeaderButton>(By.Id("headerbutton-home"));

        public HeaderButton EventsHeaderButton => Get<HeaderButton>(By.Id("headerbutton-events"));

        public HeaderButton BasketHeaderButton => Get<HeaderButton>(By.Id("headerbutton-basket"));

        public HeaderButton MoreHeaderButton => Get<HeaderButton>(By.Id("more-button"));

        public HeaderButton HistoryHeaderButton => Get<HeaderButton>(By.Id("headerbutton-history"));

        public HeaderButton AccountHeaderButton => Get<HeaderButton>(By.Id("headerbutton-account"));

        public HeaderButton LogoffHeaderButton => Get<HeaderButton>(By.Id("headerbutton-logoff"));

        public HeaderButton RegisterHeaderButton => Get<HeaderButton>(By.Id("headerbutton-register"));

        public HeaderButton LoginHeaderButton => Get<HeaderButton>(By.Id("headerbutton-login"));

        public HeaderButton DrawerHeaderButton => Get<HeaderButton>(By.ClassName("mdl-layout__drawer-button"));

        public SideMenuButton RolesSideMenuButton => Get<SideMenuButton>(By.Id("drawer-roles"));

        public SideMenuButton RoleManagerSideMenuButton => Get<SideMenuButton>(By.Id("drawer-rolemaanager"));

        public SideMenuButton CreateNewEventSideMenuButton => Get<SideMenuButton>(By.Id("drawer-createevent"));

        public bool UserIsLogged => MoreHeaderButton.IsPresent;

        public void OpenCreateEventPage()
        {
            DrawerHeaderButton.Click();
            CreateNewEventSideMenuButton.Click();
        }

        public void OpenHistoryPage()
        {
            MoreHeaderButton.Click();
            HistoryHeaderButton.Click();
        }

        public void OpenBasketPage()
        {
            BasketHeaderButton.Click();
        }

        public void Logoff()
        {
            if (MoreHeaderButton != null)
            {
                MoreHeaderButton.Click();
                LogoffHeaderButton.Click();
                HomePage.WaitForLoad();
            }
            else
            {
                GoHome();
            }
        }
    }
}