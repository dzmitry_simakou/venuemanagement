﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Event
{
    public class DeleteEventPage : BasePage
    {
        public InputSubmit DeleteEventInputSubmit => Get<InputSubmit>(By.Id("event-deletesubmit"));

        public void DeleteEvent()
        {
            DeleteEventInputSubmit.Click();
        }
    }
}