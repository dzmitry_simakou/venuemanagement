﻿using OpenQA.Selenium;
using VM.Automation.Framework.WebElements.Common;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Event
{
    public class UpdateEventPage : BasePage
    {
        public InputText NameInputText => Get<InputText>(By.Id("event-name"));

        public InputText DescriptionInputText => Get<InputText>(By.Id("event-desc"));

        public InputText ImageLinkInputText => Get<InputText>(By.Id("event-imagelink"));

        public InputDate EventDateInputDate => Get<InputDate>(By.Id("event-date"));

        public InputTime EventTimeInputTime => Get<InputTime>(By.Id("event-time"));

        public Select LayoutSelect => Get<Select>(By.Id("event-layout"));

        public InputSubmit UpdateEventInputSubmit => Get<InputSubmit>(By.Id("event-updatesubmit"));

        public void UpdateEvent(Entities.Event ev)
        {
            NameInputText.SetValue(ev.Name);
            DescriptionInputText.SetValue(ev.Description);
            ImageLinkInputText.SetValue(ev.Img);
            EventDateInputDate.SetDate(ev.Date.Year, ev.Date.Month, ev.Date.Day);
            EventTimeInputTime.SetTime(ev.Date.Hour, ev.Date.Minute);
            LayoutSelect.SelectByValue(ev.LayoutId.ToString());
            UpdateEventInputSubmit.Click();
        }
    }
}