﻿using OpenQA.Selenium;
using System.Linq;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Event
{
    public class SeatsPage : BasePage
    {
        public By SeatInputButtonSelector = By.XPath("//input[contains(@class, 'btn')]");

        public void ClickSeatBySeatId(int seatId)
        {
            GetAll<InputSubmit>(SeatInputButtonSelector).SingleOrDefault(element => element.Id.ToString().Equals(seatId.ToString())).Click();
        }
    }
}