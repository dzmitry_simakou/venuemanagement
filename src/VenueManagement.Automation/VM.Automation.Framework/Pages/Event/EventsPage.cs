﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using VM.Automation.Framework.WebElements;
using VM.Automation.Framework.WebElements.Common;

namespace VM.Automation.Framework.Pages.Event
{
    public class EventsPage : BasePage
    {
        public By BuyTicketsHyperLinkSelector = By.XPath("//a[contains(@class,'mdl-button')]");

        public By EventName_H2_Header_Selector = By.XPath("//h2[contains(@class,'mdl-card__title-text')]");

        public By LastEventName_H2_Header_Selector = By.XPath("(//h2[contains(@class,'mdl-card__title-text')])[last()]");

        public By UpdateEventButtonSelector = By.XPath("//button[contains(@class,'mdl-button-update-event')]");

        public By DeleteEventButtonSelector = By.XPath("//button[contains(@class,'mdl-button-delete-event')]");

        public IEnumerable<HyperLink> BuyTicketsHyperLinks => GetAll<HyperLink>(BuyTicketsHyperLinkSelector);

        public IEnumerable<Header> EventName_H2_Headers => GetAll<Header>(EventName_H2_Header_Selector);

        public IEnumerable<Button> UpdateEventButtons => GetAll<Button>(UpdateEventButtonSelector);

        public IEnumerable<Button> DeleteEventButtons => GetAll<Button>(DeleteEventButtonSelector);

        public void ClickBuyTicketsButtonByIndex(int eventIndex)
        {
            BuyTicketsHyperLinks.ToArray()[eventIndex - 1].Click();
        }

        public void ClickUpdateEventButtonByIndex(int eventIndex)
        {
            UpdateEventButtons.ToArray()[eventIndex - 1].Click();
        }

        public void ClickUpdateEventButtonByEventId(int eventId)
        {
            UpdateEventButtons.SingleOrDefault(button => button.WebElement.GetAttribute(WebElementAttributes.EventId).Equals(eventId.ToString())).Click();
        }

        public void ClickDeleteEventButtonByIndex(int eventIndex)
        {
            DeleteEventButtons.ToArray()[eventIndex - 1].Click();
        }

        public override void WaitForLoad()
        {
            Browser.WaitElementLoaded(LastEventName_H2_Header_Selector);
        }
    }
}