﻿using OpenQA.Selenium;
using System;
using VM.Automation.Framework.WebElements.Common;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Event
{
    public class CreateEventPage : BasePage
    {
        public InputText NameInputText => Get<InputText>(By.Id("event-name"));

        public InputText DescriptionInputText => Get<InputText>(By.Id("event-desc"));

        public InputText ImageLinkInputText => Get<InputText>(By.Id("event-imagelink"));

        public InputDate EventDateInputDate => Get<InputDate>(By.Id("event-date"));

        public InputTime EventTimeInputTime => Get<InputTime>(By.Id("event-time"));

        public Select LayoutSelect => Get<Select>(By.Id("event-layout"));

        public InputSubmit CreateEventInputSubmit => Get<InputSubmit>(By.Id("event-createsubmit"));

        public void CreateEvent(Entities.Event ev)
        {
            NameInputText.SetValue(ev.Name);
            DescriptionInputText.SetValue(ev.Description);
            ImageLinkInputText.SetValue(ev.Img);
            EventDateInputDate.SetDate(ev.Date.Year, ev.Date.Month, ev.Date.Day);
            EventTimeInputTime.SetTime(ev.Date.Hour, ev.Date.Minute);
            LayoutSelect.SelectByValue(ev.LayoutId.ToString());
            CreateEventInputSubmit.Click();
        }
    }
}