﻿using OpenQA.Selenium;
using System.Linq;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Pages.Event
{
    public class AreasPage : BasePage
    {
        public By AreaInputButtonSelector = By.XPath("//input[contains(@class, 'btn')]");

        public void ClickAreaByAreaId(int areaId)
        {
            GetAll<InputSubmit>(AreaInputButtonSelector).SingleOrDefault(button => button.Id.ToString().Equals(areaId.ToString())).Click();
        }
    }
}