﻿using TechTalk.SpecFlow;
using VM.Automation.Framework.Pages.Basket;

namespace VM.Automation.Framework.Steps.Basket
{
    [Binding]
    public class BasketSteps
    {
        public BasketPage BasketPage = new BasketPage();

        [When(@"User confirms seats in basket")]
        public void UserConfirmsSeatsInBasket()
        {
            BasketPage.ConfirmBasketInputSubmit.Click();
        }
    }
}
