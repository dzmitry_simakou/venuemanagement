﻿using TechTalk.SpecFlow;
using NUnit.Framework;
using VM.Automation.Framework.Pages.Account;

namespace VM.Automation.Framework.Steps.Account
{
    [Binding]
    public class HistorySteps
    {
        public HistoryPage HistoryPage = new HistoryPage();

        [Then(@"There is history record with order id ""(.*)""")]
        public void ThereIsHistoryRecordWithOrderId(int id)
        {
            HistoryPage.WaitForLoad();

            Assert.That(HistoryPage.GetHistoryRecordByOrderId(id.ToString()), Is.Not.Null);
        }
    }
}
