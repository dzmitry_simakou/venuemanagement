﻿using TechTalk.SpecFlow;
using VM.Automation.Framework.Pages.Account;

namespace VM.Automation.Framework.Steps.Account
{
    [Binding]
    public class LoginSteps
    {
        public LoginPage LoginPage = new LoginPage();

        [When(@"User types username ""(.*)""")]
        public void UserTypesLogin(string username)
        {
            LoginPage.UserNameInputText.SetValue(username);
        }

        [When(@"User types password ""(.*)""")]
        public void UserTypesPassword(string password)
        {
            LoginPage.PasswordInputText.SetValue(password);
        }

        [When(@"User clicks login button")]
        public void UserClicksLoginButton()
        {
            LoginPage.LoginInputButton.Click();
        }

        [When(@"User logins with username: ""(.*)"" and password: ""(.*)""")]
        public void UserClicksLoginButton(string username, string password)
        {
            LoginPage.Login(username, password);
        }

        [When(@"Login page is loaded")]
        [Then(@"Login page is loaded")]
        public void LoginPageIsLoaded()
        {
            LoginPage.WaitForLoad();
        }
    }
}