﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using VM.Automation.Framework.Pages.Home;

namespace VM.Automation.Framework.Steps.Home
{
    [Binding]
    public class HomeSteps
    {
        private HomePage HomePage = new HomePage();

        [When(@"Home page is loaded")]
        [Then(@"Home page is loaded")]
        public void HomePageIsLoaded()
        {
            HomePage.WaitForLoad();
        }
    }
}