﻿using NUnit.Framework;
using System.Linq;
using TechTalk.SpecFlow;
using VM.Automation.Framework.Pages.Event;

namespace VM.Automation.Framework.Steps.Event
{
    [Binding]
    public class EventSteps
    {
        public EventsPage EventsPage = new EventsPage();

        public AreasPage AreasPage = new AreasPage();

        public SeatsPage SeatsPage = new SeatsPage();

        [When(@"User wants to buy tickets for (.*) event")]
        public void UserWantsToBuyTicketsForEventByIndex(int index)
        {
            EventsPage.ClickBuyTicketsButtonByIndex(index);
        }

        [When(@"User wants to update event with event id (.*)")]
        public void UserWantsToUpdateEventByEventId(int eventId)
        {
            EventsPage.ClickUpdateEventButtonByEventId(eventId);
        }

        [When(@"User wants to update (.*) event")]
        public void UserWantsToUpdateEventByIndex(int index)
        {
            EventsPage.ClickUpdateEventButtonByIndex(index);
        }

        [When(@"User wants to delete (.*) event")]
        public void UserWantsToDeleteEventByIndex(int index)
        {
            EventsPage.ClickDeleteEventButtonByIndex(index);
        }

        [When(@"User clicks button of area with id (.*)")]
        public void UserClicksButtonOfAreaWithId(int id)
        {
            AreasPage.ClickAreaByAreaId(id);
        }

        [When(@"User clicks button of seat with id (.*)")]
        public void UserClicksButtonOfSeatWithId(int id)
        {
            SeatsPage.ClickSeatBySeatId(id);
        }

        [Then(@"There is no event with name ""(.*)""")]
        public void ThereIsNoEventWithName(string name)
        {
            EventsPage.WaitForLoad();
            Assert.That(EventsPage.EventName_H2_Headers.Any(header => header.Text.Equals(name)), Is.False);
        }
    }
}