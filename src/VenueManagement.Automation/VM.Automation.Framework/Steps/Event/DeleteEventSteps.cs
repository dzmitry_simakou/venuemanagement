﻿using TechTalk.SpecFlow;
using VM.Automation.Framework.Pages.Event;

namespace VM.Automation.Framework.Steps.Event
{
    [Binding]
    public class DeleteEventSteps
    {
        public DeleteEventPage DeleteEventPage = new DeleteEventPage();
        
        [When(@"User deletes event")]
        public void UserDeletesEvent()
        {
            DeleteEventPage.DeleteEvent();
        }
    }
}