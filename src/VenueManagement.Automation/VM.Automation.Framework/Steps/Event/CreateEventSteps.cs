﻿using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using VM.Automation.Framework.Pages.Event;

namespace VM.Automation.Framework.Steps.Event
{
    [Binding]
    public class CreateEventSteps
    {
        public CreateEventPage CreateEventPage = new CreateEventPage();

        [When(@"User creates event:")]
        public void UserCreatesEvent(Table table)
        {
            var ev = table.CreateInstance<Entities.Event>();
            CreateEventPage.CreateEvent(ev);
        }
    }
}