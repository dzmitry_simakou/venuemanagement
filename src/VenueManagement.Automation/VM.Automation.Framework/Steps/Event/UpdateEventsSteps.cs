﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using VM.Automation.Framework.Pages.Event;
using VM.Automation.Framework.WebElements.Inputs;

namespace VM.Automation.Framework.Steps.Event
{
    [Binding]
    public class UpdateEventsSteps
    {
        public UpdateEventPage UpdateEventPage = new UpdateEventPage();

        [When(@"User updates event:")]
        public void UserCreatesEvent(Table table)
        {
            var ev = table.CreateInstance<Entities.Event>();
            UpdateEventPage.UpdateEvent(ev);
        }

        [Then(@"User validates event values on update page:")]
        public void UserValidatesEventValues(Table table)
        {
            var ev = table.CreateInstance<Entities.Event>();
            Assert.Multiple(() =>
            {
                Assert.That(UpdateEventPage.NameInputText.Value, Is.EqualTo(ev.Name));
                Assert.That(UpdateEventPage.DescriptionInputText.Value, Is.EqualTo(ev.Description));
                Assert.That(UpdateEventPage.LayoutSelect.SelectedItem.Value, Is.EqualTo(ev.LayoutId.ToString()));
                Assert.That(UpdateEventPage.EventDateInputDate.Value, Is.EqualTo(ev.Date.ToString(InputDate.ValueFormat)));
                Assert.That(UpdateEventPage.EventTimeInputTime.Value, Is.EqualTo(ev.Date.ToString(InputTime.ValueFormat)));
                Assert.That(UpdateEventPage.ImageLinkInputText.Value, Is.EqualTo(ev.Img));
            });
        }
    }
}
