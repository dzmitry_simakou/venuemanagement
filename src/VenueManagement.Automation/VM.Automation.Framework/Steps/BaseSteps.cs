﻿using NUnit.Framework;
using System.Configuration;
using TechTalk.SpecFlow;
using VM.Automation.Framework.Browsers;
using VM.Automation.Framework.Browsers.Interfaces;
using VM.Automation.Framework.Pages.Common;

namespace VM.Automation.Framework.Steps
{
    [Binding]
    public class BaseSteps
    {
        private string HomeUrl = ConfigurationManager.AppSettings["MainUrl"];

        private IBrowser Browser => BrowserManager.Current;

        private LayoutPage Header = new LayoutPage();

        [Given(@"User opens home page")]
        public void UserOpensHomePage()
        {
            Browser.Url = HomeUrl;
        }

        [Given(@"User clicks Events header button")]
        [When(@"User clicks Events header button")]
        public void UserClicksEventsHeaderButton()
        {
            Header.EventsHeaderButton.Click();
        }

        [Given(@"User clicks Login header button")]
        [When(@"User clicks Login header button")]
        public void UserClicksLoginHeaderButton()
        {
            Header.LoginHeaderButton.Click();
        }

        [Given(@"User clicks Create Event button")]
        [When(@"User clicks Create Event button")]
        public void UserClicksCreateEventButton()
        {
            Header.OpenCreateEventPage();
        }

        [Given(@"User clicks History button")]
        [When(@"User clicks History button")]
        public void UserClicksHistoryButton()
        {
            Header.OpenHistoryPage();
        }

        [Given(@"User clicks Basket button")]
        [When(@"User clicks Basket button")]
        public void UserBasketButton()
        {
            Header.OpenBasketPage();
        }

        [Then(@"Current page title is ""(.*)""")]
        public void CurrentPageTitleIsValue(string value)
        {
            BrowserManager.Current.WaitPageLoaded();
            Assert.That(BrowserManager.Current.Title, Is.EqualTo(value));
        }

        [Then(@"Current URL is ""(.*)""")]
        public void CurrentUrlIsValue(string value)
        {
            Assert.That(BrowserManager.Current.Url, Is.EqualTo(value));
        }

        [Then(@"Current URL contains ""(.*)""")]
        public void CurrentUrlContainsValue(string value)
        {
            Assert.That(BrowserManager.Current.Url, Does.Contain(value));
        }

        [Then(@"User is logged")]
        public void UserIsLogged()
        {
            Assert.That(Header.UserIsLogged, Is.True);
        }

        [Then(@"User is in Admin role")]
        public void UserIsInAdminRole()
        {
            Assert.That(Header.DrawerHeaderButton.IsPresent, Is.True);
        }

        [Then(@"User is not in Admin role")]
        public void UserIsNotInAdminRole()
        {
            Assert.That(Header.DrawerHeaderButton.IsPresent, Is.Null);
        }
    }
}