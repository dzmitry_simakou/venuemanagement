﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Role : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}