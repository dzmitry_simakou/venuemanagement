﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Venue : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var venue = (Venue) obj;
            return Id == venue.Id && Name == venue.Name && Description == venue.Description && Address == venue.Address && Phone == venue.Phone;
        }
    }
}