﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class EventArea : IEntity
    {
        public int Id { get; set; }

        public int EventId { get; set; }

        public string Description { get; set; }

        public int CoordX { get; set; }

        public int CoordY { get; set; }

        public int Price { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var eventArea = (EventArea)obj;
            return Id == eventArea.Id && EventId == eventArea.EventId && Description == eventArea.Description && CoordX == eventArea.CoordX && CoordY == eventArea.CoordY && Price == eventArea.Price;
        }
    }
}