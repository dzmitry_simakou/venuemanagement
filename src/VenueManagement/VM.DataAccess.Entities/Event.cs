﻿using System;
using System.ComponentModel.DataAnnotations;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Event : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int LayoutId { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        public string Img { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var ev = (Event)obj;
            return Id == ev.Id && Name == ev.Name && Description == ev.Description && LayoutId == ev.LayoutId && Date == ev.Date && Img == ev.Img;
        }
    }
}