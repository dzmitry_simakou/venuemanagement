﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class EventSeat : IEntity
    {
        public int Id { get; set; }

        public int EventAreaId { get; set; }

        public int Row { get; set; }

        public int Number { get; set; }

        public int? OrderId { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var eventSeat = (EventSeat)obj;
            return Id == eventSeat.Id && EventAreaId == eventSeat.EventAreaId && Row == eventSeat.Row && Number == eventSeat.Number && OrderId == eventSeat.OrderId;
        }
    }
}