﻿using System;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public int Price { get; set; }
    }
}