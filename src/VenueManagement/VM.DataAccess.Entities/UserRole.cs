﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class UserRole : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var userRole = (UserRole)obj;
            return Id == userRole.Id && UserId == userRole.UserId && RoleId == userRole.RoleId;
        }
    }
}