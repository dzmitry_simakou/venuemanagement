﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Seat : IEntity
    {
        public int Id { get; set; }

        public int AreaId { get; set; }

        public int Row { get; set; }

        public int Number { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var seat = (Seat)obj;
            return Id == seat.Id && AreaId == seat.AreaId && Row == seat.Row && Number == seat.Number;
        }
    }
}