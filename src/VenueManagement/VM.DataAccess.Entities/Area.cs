﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Area : IEntity
    {
        public int Id { get; set; }

        public int LayoutId { get; set; }

        public string Description { get; set; }

        public int CoordX { get; set; }

        public int CoordY { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var area = (Area)obj;
            return Id == area.Id && LayoutId == area.LayoutId && Description == area.Description && CoordX == area.CoordX && CoordY == area.CoordY;
        }
    }
}