﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public int Balance { get; set; }

        public string Lang { get; set; }

        public string TimeZone { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }        
    }
}