﻿using System;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class BasketItem : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int SeatId { get; set; }

        public DateTime ExpiryDate { get; set; }

        public override string ToString()
        {
            return string.Join(";", Id, UserId, SeatId, ExpiryDate);
        }
    }
}