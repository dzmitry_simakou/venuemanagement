﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class Layout : IEntity
    {
        public int Id { get; set; }

        public int VenueId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var layout = (Layout)obj;
            return Id == layout.Id && VenueId == layout.VenueId && Description == layout.Description && Name == layout.Name;
        }
    }
}
