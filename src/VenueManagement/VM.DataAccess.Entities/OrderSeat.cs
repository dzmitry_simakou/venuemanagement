﻿using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Entities
{
    public class OrderSeat : IEntity
    {
        public int Id { get; set; }

        public int OrderId { get; set; }

        public int EventSeatId { get; set; }

        public int Price { get; set; }
    }
}