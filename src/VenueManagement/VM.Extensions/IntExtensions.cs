﻿using System.Globalization;

namespace VM.Extensions
{
    public static class IntExtensions
    {
        public static string ToPriceString(this int value, CultureInfo cultureInfo)
        {
            return (value/100).ToString("C", cultureInfo);
        }
    }
}