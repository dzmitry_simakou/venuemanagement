﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace VM.Extensions
{
    public static class DbCommandExtension
    {
        public static IDbDataParameter CreateParameter(this IDbCommand command, string parameterName, object value)
        {
            var parameter = command.CreateParameter();

            parameter.ParameterName = parameterName;
            parameter.Value = value;
            parameter.DbType = Parameter.ConvertTypeCodeToDbType(Type.GetTypeCode(value.GetType()));

            return parameter;
        }
    }
}