﻿using System;

namespace VM.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime WithTimeZone(this DateTime date, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(date.ToUniversalTime(), timeZoneInfo);
        }
    }
}
