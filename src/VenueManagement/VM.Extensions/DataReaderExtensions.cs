﻿using System;
using System.Collections.Generic;
using System.Data;

namespace VM.Extensions
{
    public static class DataReaderExtension
    {
        public static IEnumerable<T> ReadAll<T>(this IDataReader reader)
        {
            var array = new List<T>();

            while (reader.Read())
            {
                var obj = Activator.CreateInstance<T>();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    if(reader[prop.Name] != DBNull.Value)
                    {
                        prop.SetValue(obj, reader[prop.Name]);
                    }
                }

                array.Add(obj);
            }

            return array;
        }

        public static T Read<T>(this IDataReader reader)
        {
            var obj = Activator.CreateInstance<T>();

            reader.Read();

            foreach (var prop in obj.GetType().GetProperties())
            {
                if(reader[prop.Name] != DBNull.Value)
                {
                    prop.SetValue(obj, reader[prop.Name]);
                }
            }

            return obj;
        }
    }
}