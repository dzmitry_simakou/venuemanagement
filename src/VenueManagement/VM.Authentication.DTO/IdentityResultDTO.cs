﻿using System.Collections.Generic;

namespace VM.Authentication.DTO
{
    /// <summary>
    ///     Represents the result of an identity operation
    /// </summary>
    public class IdentityResultDTO
    {

        public IdentityResultDTO()
        {
        }

        /// <summary>
        ///     Failure constructor that takes error messages
        /// </summary>
        /// <param name="errors"></param>
        public IdentityResultDTO(IEnumerable<string> errors)
        {
            if (errors == null)
            {
                errors = new[] { "Identity Error" };
            }
            Succeeded = false;
            Errors = errors;
        }

        /// <summary>
        ///     True if the operation was successful
        /// </summary>
        public bool Succeeded { get; }

        /// <summary>
        ///     List of errors
        /// </summary>
        public IEnumerable<string> Errors { get; }

    }
}