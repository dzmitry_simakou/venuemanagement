﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using VM.Authentication.Clients.Interfaces;

namespace VM.Authentication.Clients
{
    public class AuthenticationClient : BaseClient, IAuthenticationClient
    {
        public async Task<string> Test()
        {
            var url = "https://localhost:44392/api/Authentication";

            var responseMessage = await Client.GetAsync(url);

            var result = await responseMessage.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<string>(result);
        }
    }
}