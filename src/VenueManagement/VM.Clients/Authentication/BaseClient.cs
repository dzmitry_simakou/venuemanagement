﻿using System.Net.Http;
using VM.Authentication.Clients.Interfaces;

namespace VM.Authentication.Clients
{
    public abstract class BaseClient : IClient
    {
        private readonly HttpClient _client = new HttpClient();

        public HttpClient Client => _client;

    }
}