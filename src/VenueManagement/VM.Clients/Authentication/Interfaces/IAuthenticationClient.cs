﻿using VM.Authentication.Contracts;

namespace VM.Clients.Interfaces
{
    public interface IAuthenticationClient : IAuthenticationContract, IClient
    {

    }
}