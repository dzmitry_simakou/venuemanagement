﻿using System.Net.Http;

namespace VM.Authentication.Clients.Interfaces
{
    public interface IClient
    {
        HttpClient Client { get; }
    }
}