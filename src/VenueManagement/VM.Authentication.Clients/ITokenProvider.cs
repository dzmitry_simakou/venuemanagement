﻿namespace VM.Authentication.Clients
{
    public interface ITokenProvider
    {
        string GetToken();
    }
}