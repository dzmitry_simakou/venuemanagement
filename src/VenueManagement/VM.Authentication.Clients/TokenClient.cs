﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using VM.Authentication.Clients.Interfaces;
using VM.DataAccess.Entities;

namespace VM.Authentication.Clients
{
    public class TokenClient : BaseClient, ITokenClient
    {
        public override string ResourceName => "Token";

        public async Task<string> GetToken(string username, string password)
        {
            var response = await Client.GetAsync($"{ApiUrl}/{ResourceName}?username={username}&password={password}");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> RegisterUser(User user, string password)
        {
            var response = await Client.PostAsync($"{ApiUrl}/{ResourceName}?password={password}", Serialize(user));

            return await response.Content.ReadAsStringAsync();
        }
    }
}