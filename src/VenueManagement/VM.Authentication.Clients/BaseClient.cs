﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using VM.Authentication.Clients.Interfaces;

namespace VM.Authentication.Clients
{
    public abstract class BaseClient : IClient
    {
        private bool _disposed;

        protected readonly HttpClient Client = new HttpClient();

        public virtual string ApiUrl => ConfigurationManager.AppSettings.Get("AuthenticationServiceUrl");

        public abstract string ResourceName { get; }

        protected StringContent Serialize(object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
        }

        protected T Deserialize<T>(string obj)
        {
            if(obj != string.Empty)
            {
                return JsonConvert.DeserializeObject<T>(obj);
            }

            return default(T);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                Client.Dispose();
            }

            _disposed = true;
        }

        ~BaseClient()
        {
            Dispose(false);
        }
    }
}