﻿using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using VM.Authentication.Clients.Interfaces;

namespace VM.Authentication.Clients
{
    public class RoleClient : BaseClient, IRoleClient
    {
        public override string ResourceName => "Account/Role";

        public RoleClient(ITokenProvider provider)
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, provider.GetToken());
        }

        public async Task<bool> AddToRole(int userId, string roleName)
        {
            var response = await Client.PostAsync($"{ApiUrl}/{ResourceName}?userId={userId}&roleName={roleName}", null);
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<bool>(result);
        }

        public async Task<bool> RemoveFromRole(int userId, string roleName)
        {
            var response = await Client.DeleteAsync($"{ApiUrl}/{ResourceName}?userId={userId}&roleName={roleName}");
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<bool>(result);
        }

        public async Task<List<string>> GetUserRoles(string username)
        {
            var response = await Client.GetAsync($"{ApiUrl}/{ResourceName}?username={username}");
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<List<string>>(result);
        }

        public async Task<List<string>> GetAllRoles()
        {
            var response = await Client.GetAsync($"{ApiUrl}/{ResourceName}");
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<List<string>>(result);
        }
    }
}