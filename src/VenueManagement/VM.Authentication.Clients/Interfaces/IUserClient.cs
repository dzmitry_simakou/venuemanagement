﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VM.Authentication.DTO;

namespace VM.Authentication.Clients.Interfaces
{
    public interface IUserClient : IClient
    {

        Task<IdentityResultDTO> CreateUser(UserDTO user, string password);

        Task<IdentityResultDTO> UpdateUser(UserDTO user);

        Task<IdentityResultDTO> UpdatePassword(UserDTO user, string oldPassword, string newPassword);

        Task<UserDTO> GetUser(string username);

        Task<List<UserDTO>> GetAllUsers();
    }
}