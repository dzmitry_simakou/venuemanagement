﻿using System;

namespace VM.Authentication.Clients.Interfaces
{
    public interface IClient : IDisposable
    {
        string ApiUrl { get;  }

        string ResourceName { get;  }
    }
}