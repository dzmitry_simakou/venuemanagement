﻿using System.Threading.Tasks;
using VM.DataAccess.Entities;

namespace VM.Authentication.Clients.Interfaces
{
    public interface ITokenClient : IClient
    {
        Task<string> GetToken(string username, string password);

        Task<string> RegisterUser(User user, string password);
    }
}