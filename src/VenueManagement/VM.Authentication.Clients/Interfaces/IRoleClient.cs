﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VM.Authentication.Clients.Interfaces
{
    public interface IRoleClient : IClient
    {
        Task<bool> AddToRole(int userId, string roleName);

        Task<bool> RemoveFromRole(int userId, string roleName);

        Task<List<string>> GetAllRoles();

        Task<List<string>> GetUserRoles(string username);
    }
}