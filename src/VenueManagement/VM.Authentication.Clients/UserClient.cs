﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VM.Authentication.Clients.Interfaces;
using VM.Authentication.DTO;
using System.Net.Http.Headers;

namespace VM.Authentication.Clients
{
    public class UserClient : BaseClient, IUserClient
    {
        public override string ResourceName => "Account/User";

        public UserClient(ITokenProvider provider)
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, provider.GetToken());
        }

        public async Task<IdentityResultDTO> CreateUser(UserDTO user, string password)
        {
            var content = Serialize(user);
            var response = await Client.PostAsync($"{ApiUrl}/{ResourceName}?password={password}", content);
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<IdentityResultDTO>(result);
        }

        public async Task<IdentityResultDTO> UpdateUser(UserDTO user)
        {
            var content = Serialize(user);
            var response = await Client.PutAsync($"{ApiUrl}/{ResourceName}", content);
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<IdentityResultDTO>(result);
        }

        public async Task<IdentityResultDTO> UpdatePassword(UserDTO user, string oldPassword, string newPassword)
        {
            var content = Serialize(user);
            var response = await Client.PutAsync($"{ApiUrl}/{ResourceName}?oldPassword={oldPassword}&newPassword={newPassword}", content);
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<IdentityResultDTO>(result);
        }

        public async Task<UserDTO> GetUser(string username)
        {
            var response = await Client.GetAsync($"{ApiUrl}/{ResourceName}?username={username}");
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<UserDTO>(result);
        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            var response = await Client.GetAsync($"{ApiUrl}/{ResourceName}");
            var result = await response.Content.ReadAsStringAsync();

            return Deserialize<List<UserDTO>>(result);
        }
    }
}