﻿namespace VM.Authentication.Clients
{
    internal class JwtBearerDefaults
    {
        public const string AuthenticationScheme = "Bearer";
    }
}