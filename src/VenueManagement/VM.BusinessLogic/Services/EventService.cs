﻿using System;
using System.Collections.Generic;
using System.Linq;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.BusinessLogic.Services
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IEventAreaRepository _eventAreaRepository;
        private readonly IEventSeatRepository _eventSeatRepository;
        private readonly ILayoutRepository _layoutRepository;
        private readonly IAreaRepository _areaRepository;
        private readonly ISeatRepository _seatRepository;
        private readonly IBasketItemRepository _basketItemRepository;

        public EventService(IEventRepository eventRepository,
                            IEventAreaRepository eventAreaRepository,
                            IEventSeatRepository eventSeatRepository,
                            ILayoutRepository layoutRepository,
                            IAreaRepository areaRepository,
                            ISeatRepository seatRepository,
                            IBasketItemRepository basketItemRepository)
        {
            _eventRepository = eventRepository;
            _eventAreaRepository = eventAreaRepository;
            _eventSeatRepository = eventSeatRepository;
            _layoutRepository = layoutRepository;
            _areaRepository = areaRepository;
            _seatRepository = seatRepository;
            _basketItemRepository = basketItemRepository;
        }

        private void ValidateEventSeat(EventSeat eventSeat)
        {
            var repSeats = _eventSeatRepository.GetAll().Where(repSeat => repSeat.EventAreaId.Equals(eventSeat.EventAreaId) && repSeat.Id != eventSeat.Id);
            if (repSeats.Any(any => any.Row.Equals(eventSeat.Row) && any.Number.Equals(eventSeat.Number)))
            {
                throw new OverlapException($"EventArea {eventSeat.EventAreaId} already contains seat with row {eventSeat.Row} and number {eventSeat.Number}");
            }
        }

        private void ValidateEventArea(EventArea eventArea)
        {
            var repAreas = _eventAreaRepository.GetAll().Where(repArea => repArea.EventId.Equals(eventArea.EventId) && repArea.Id != eventArea.Id).ToList();
            if (repAreas.Any(any => any.Description.Equals(eventArea.Description)))
            {
                throw new NotUniqueException($"Event {eventArea.EventId} already contains area with description {eventArea.Description}");
            }

            if (repAreas.Any(any => any.CoordX.Equals(eventArea.CoordX) && any.CoordY.Equals(eventArea.CoordY)))
            {
                throw new OverlapException($"Event {eventArea.EventId} already contains area with CoordX: {eventArea.CoordX} and CoordY: {eventArea.CoordY}");
            }
        }

        private void ValidateEvent(Event ev)
        {
            if (ev.Date < DateTime.Now)
            {
                throw new EventDateException($"Date of new event should be in the future");
            }

            var areasIds = _areaRepository.GetAll().Where(area => area.LayoutId == ev.LayoutId).Select(area => area.Id).ToList();
            var seats = _seatRepository.GetAll().Where(seat => areasIds.Contains(seat.AreaId));

            if (!seats.Any())
            {
                throw new NoSeatsException($"Layout {ev.LayoutId} doesn't contain any seat");
            }

            var venueId = _layoutRepository.GetAll().Single(layout => layout.Id == ev.LayoutId).VenueId;
            var venueLayoutsIds = _layoutRepository.GetAll().Where(layout => layout.VenueId == venueId).Select(layout => layout.Id).ToList();
            var venueEvents = _eventRepository.GetAll().Where(x => venueLayoutsIds.Contains(x.LayoutId) && x.Id != ev.Id);

            foreach(var existingEvent in venueEvents)
            {
                if (existingEvent.Date == ev.Date)
                {
                    throw new EventDateException($"Date of new event {ev.Date} overlap date {existingEvent.Date} of existing event {existingEvent.Id}");
                }
            }
        }

        private void ValidatePurchasedEventSeats(Event ev)
        {
            if (HasPurchasedSeats(ev.Id))
            {
                throw new AlreadyPaidException($"This event has purchased seats");
            }
        }

        public void CreateEventSeat(EventSeat seat)
        {
            ValidateEventSeat(seat);

            _eventSeatRepository.Create(seat);
        }

        public void UpdateEventSeat(EventSeat seat)
        {
            ValidateEventSeat(seat);

            _eventSeatRepository.Update(seat);
        }

        public void DeleteEventSeat(EventSeat seat)
        {
            _eventSeatRepository.Delete(seat);
        }
        public IEnumerable<EventSeat> GetEventSeats()
        {
            return _eventSeatRepository.GetAll();
        }

        public IEnumerable<EventSeat> GetEventSeatsByEventAreaId(int eventAreaId)
        {
            return _eventSeatRepository.GetAll().Where(eventSeat => eventSeat.EventAreaId == eventAreaId);
        }

        public IEnumerable<EventSeat> GetAvailableEventSeats(int eventAreaId)
        {
            var basketItemIds = _basketItemRepository.GetAll().Select(item => item.SeatId).ToList();
            return GetEventSeatsByEventAreaId(eventAreaId).Where(seat => seat.OrderId == null && !basketItemIds.Contains(seat.Id));
        }

        public void CreateEventArea(EventArea area)
        {
            ValidateEventArea(area);

            _eventAreaRepository.Create(area);
        }

        public void UpdateEventArea(EventArea area)
        {
            ValidateEventArea(area);

            _eventAreaRepository.Update(area);
        }

        public void DeleteEventArea(EventArea area)
        {
            _eventAreaRepository.Delete(area);
        }

        public EventArea GetEventArea(int eventAreaId)
        {
            return _eventAreaRepository.GetAll().SingleOrDefault(area => area.Id == eventAreaId);
        }

        public IEnumerable<EventArea> GetEventAreas()
        {
            return _eventAreaRepository.GetAll();
        }

        public IEnumerable<EventArea> GetEventAreasByEventId(int eventId)
        {
            return _eventAreaRepository.GetAll().Where(eventArea => eventArea.EventId == eventId);
        }

        public void CreateEvent(Event ev)
        {
            ValidateEvent(ev);

            _eventRepository.Create(ev);
        }

        public void UpdateEvent(Event ev)
        {
            ValidatePurchasedEventSeats(ev);
            ValidateEvent(ev);

            _eventRepository.Update(ev);
        }

        public void DeleteEvent(Event ev)
        {
            ValidatePurchasedEventSeats(ev);

            _eventRepository.Delete(ev);
        }

        public void DeleteEventByEventId(int eventId)
        {
            var ev = new Event { Id = eventId };

            ValidatePurchasedEventSeats(ev);

            _eventRepository.Delete(new Event { Id = eventId });
        }

        public Event GetEvent(int eventId)
        {
            return _eventRepository.GetAll().SingleOrDefault(ev => ev.Id == eventId);
        }

        public IEnumerable<Event> GetEvents()
        {
            return _eventRepository.GetAll();
        }

        public IEnumerable<Event> GetEventsByLayoutId(int layoutId)
        {
            return _eventRepository.GetAll().Where(ev => ev.LayoutId == layoutId);
        }

        public bool HasPurchasedSeats(int eventId)
        {
            var eventAreaIds = _eventAreaRepository.GetAll().Where(area => area.EventId == eventId).Select(area => area.Id);
            var eventSeats = _eventSeatRepository.GetAll().Where(seat => eventAreaIds.Contains(seat.EventAreaId));

            if (eventSeats.Where(seat => seat.OrderId != null).Any())
            {
                return true;
            }

            return false;
        }

        public void ClearExpiredBasketItemsByEventAreaId(int eventAreaId)
        {
            var seatsIdsByEventAreaId = _eventSeatRepository.GetAll().Where(seat => seat.EventAreaId == eventAreaId).Select(seat => seat.Id).ToList();
            var expiredSeats = _basketItemRepository.GetAll().Where(seat => seat.ExpiryDate < DateTime.Now && seatsIdsByEventAreaId.Contains(seat.SeatId));
            foreach (var seat in expiredSeats)
            {
                _basketItemRepository.Delete(seat);
            }
        }
    }
}