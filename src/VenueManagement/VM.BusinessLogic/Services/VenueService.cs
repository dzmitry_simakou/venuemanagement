﻿using System.Collections.Generic;
using System.Linq;
using VM.BusinessLogic.Exceptions;
using VM.DataAccess.Repositories.Interfaces;
using VM.DataAccess.Entities;
using VM.BusinessLogic.Interfaces;

namespace VM.BusinessLogic.Services
{
    
    public class VenueService : IVenueService
    {
        private readonly ISeatRepository _seatRepository;
        private readonly IAreaRepository _areaRepository;
        private readonly ILayoutRepository _layoutRepository;
        private readonly IVenueRepository _venueRepository;

        public VenueService(ISeatRepository seatRepository,
                            IAreaRepository areaRepository,
                            ILayoutRepository layoutRepository,
                            IVenueRepository venueRepository)
        {
            _seatRepository = seatRepository;
            _areaRepository = areaRepository;
            _layoutRepository = layoutRepository;
            _venueRepository = venueRepository;
        }

        private void ValidateSeat(Seat seat)
        {
            if(seat.Row < 0 || seat.Number < 0)
            {
                throw new NegativeCoordinateException($"Seat row: \"{seat.Row}\" or number \"{seat.Number}\" is less than 0");
            }

            var repSeats = _seatRepository.GetAll().Where(repSeat => repSeat.AreaId.Equals(seat.AreaId) && repSeat.Id != seat.Id);
            if (repSeats.Any(any => any.Row.Equals(seat.Row) && any.Number.Equals(seat.Number)))
            {
                throw new OverlapException($"Area {seat.AreaId} already contains seat with row {seat.Row} and number {seat.Number}");
            }
        }

        private void ValidateArea(Area area)
        {
            if (area.CoordX < 0 || area.CoordY < 0)
            {
                throw new NegativeCoordinateException($"Area X: \"{area.CoordX}\" or Y \"{area.CoordY}\" is less than 0");
            }

            var repAreas = _areaRepository.GetAll().Where(repArea => repArea.LayoutId.Equals(area.LayoutId) && repArea.Id != area.Id).ToList();
            if (repAreas.Any(any => any.Description.Equals(area.Description)))
            {
                throw new NotUniqueException($"Layout {area.LayoutId} already contains area with description {area.Description}");
            }

            if (repAreas.Any(any => any.CoordX.Equals(area.CoordX) && any.CoordY.Equals(area.CoordY)))
            {
                throw new OverlapException($"Layout {area.LayoutId} already contains area with CoordX: {area.CoordX} and CoordY: {area.CoordY}");
            }
        }

        private void ValidateLayout(Layout layout)
        {
            var repLayouts = _layoutRepository.GetAll().Where(repLayout => repLayout.VenueId.Equals(layout.VenueId) && repLayout.Id != layout.Id);
            if (repLayouts.Any(any => any.Name.Equals(layout.Name)))
            {
                throw new NotUniqueException($"Venue {layout.VenueId} already contains layout with name {layout.Name}");
            }
        }

        private void ValidateVenue(Venue venue)
        {
            var repVenues = _venueRepository.GetAll().Where(repVenue => repVenue.Id != venue.Id);
            if (repVenues.Any(any => any.Name.Equals(venue.Name)))
            {
                throw new NotUniqueException($"Venue with name {venue.Name} already exists");
            }
        }

        public void CreateSeat(Seat seat)
        {
            ValidateSeat(seat);

            _seatRepository.Create(seat);
        }

        public void UpdateSeat(Seat seat)
        {
            ValidateSeat(seat);

            _seatRepository.Update(seat);
        }

        public void DeleteSeat(Seat seat)
        {
            _seatRepository.Delete(seat);
        }

        public IEnumerable<Seat> GetSeats(int areaId)
        {
            return _seatRepository.GetAll().Where(seat => seat.AreaId == areaId);
        }

        public void CreateArea(Area area)
        {
            ValidateArea(area);

            _areaRepository.Create(area);
        }

        public void UpdateArea(Area area)
        {
            ValidateArea(area);
            _areaRepository.Update(area);
        }

        public void DeleteArea(Area area)
        {
            _areaRepository.Delete(area);
        }

        public IEnumerable<Area> GetAreas(int layoutId)
        {
            return _areaRepository.GetAll().Where(area => area.LayoutId == layoutId);
        }

        public void CreateLayout(Layout layout)
        {
            ValidateLayout(layout);

            _layoutRepository.Create(layout);
        }

        public void UpdateLayout(Layout layout)
        {
            ValidateLayout(layout);

            _layoutRepository.Update(layout);
        }

        public void DeleteLayout(Layout layout)
        {
            _layoutRepository.Delete(layout);
        }

        public IEnumerable<Layout> GetLayouts()
        {
            return _layoutRepository.GetAll();
        }

        public IEnumerable<Layout> GetLayoutsByVenueId(int venueId)
        {
            return _layoutRepository.GetAll().Where(layout => layout.VenueId == venueId);
        }

        public void CreateVenue(Venue venue)
        {
            ValidateVenue(venue);

            _venueRepository.Create(venue);
        }

        public void UpdateVenue(Venue venue)
        {
            ValidateVenue(venue);

            _venueRepository.Update(venue);
        }

        public void DeleteVenue(Venue venue)
        {
            _venueRepository.Delete(venue);
        }

        public IEnumerable<Venue> GetVenues()
        {
            return _venueRepository.GetAll();
        }
    }
}