﻿using System;
using System.Collections.Generic;
using System.Linq;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.BusinessLogic.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderSeatRepository _orderSeatRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IUserRepository _userRepository;

        private void ValidateUser(User user)
        {
            if (user.Balance < 0)
            {
                throw new BalanceException($"User with Id {user.Id} has balance {user.Balance} that less than 0");
            }
        }

        private void ChangeBalance(int userId, int balanceDifference)
        {
            var user = _userRepository.GetAll().Single(repUser => repUser.Id == userId);
            user.Balance += balanceDifference;

            ValidateUser(user);

            _userRepository.Update(user);
        }

        public OrderService(IOrderRepository orderRepository, 
                            IOrderSeatRepository orderSeatRepository,
                            IUserRepository userRepository)
        {
            _orderRepository = orderRepository;
            _orderSeatRepository = orderSeatRepository;
            _userRepository = userRepository;
        }

        public IEnumerable<Order> GetOrders(int userId)
        {
            return _orderRepository.GetAll().Where(order => order.UserId == userId);
        }

        public void CreateOrderByUserIdAndPrice(int userId, int price)
        {
            var order = new Order { UserId = userId, Date = DateTime.Now, Price = price };

            ChangeBalance(userId, -price);

            _orderRepository.Create(order);
        }

        public void CreateOrder(Order order)
        {
            ChangeBalance(order.UserId, -order.Price);

            _orderRepository.Create(order);
        }

        public IEnumerable<OrderSeat> GetOrderSeats(int userId)
        {
            var orders = _orderRepository.GetAll().Where(order => order.UserId == userId).Select(order => order.Id);
            return _orderSeatRepository.GetAll().Where(seat => orders.Contains(seat.OrderId));
        }
    }
}