﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.BusinessLogic.Services
{
    public class BasketService : IBasketService
    {
        private const int DefaultBasketExpiry = 30;
        private readonly IEventSeatRepository _eventSeatRepository;
        private readonly IEventAreaRepository _eventAreaRepository;
        private readonly IBasketItemRepository _basketItemRepository;

        public BasketService(IEventSeatRepository eventSeatRepository,
                             IEventAreaRepository eventAreaRepository,
                             IBasketItemRepository basketItemRepository)
        {
            _eventSeatRepository = eventSeatRepository;
            _eventAreaRepository = eventAreaRepository;
            _basketItemRepository = basketItemRepository;
        }

        private DateTime GetExpiryDate()
        {
            var configValue = ConfigurationManager.AppSettings.Get("BasketExpiry");
            if(configValue == null)
            {
                return DateTime.Now.AddSeconds(DefaultBasketExpiry);
            }

            return DateTime.Now.AddSeconds(int.Parse(configValue));
        }

        public void CreateBasketItem(BasketItem basket)
        {
            if (basket.ExpiryDate == default(DateTime))
            {
                basket.ExpiryDate = GetExpiryDate();
            }

            _basketItemRepository.Create(basket);
        }

        public void DeleteBasketItemById(int id)
        {
            _basketItemRepository.Delete(new BasketItem { Id = id });
        }

        public void DeleteBasketItem(BasketItem basket)
        {
            _basketItemRepository.Delete(basket);
        }

        public int GetBasketSummaryPrice(int userId)
        {
            var basketSeatIds = _basketItemRepository.GetAll().Where(link => link.UserId == userId && link.ExpiryDate > DateTime.Now).Select(seat => seat.SeatId).ToList();
            if (!basketSeatIds.Any())
            {
                return 0;
            }

            var seats = _eventSeatRepository.GetAll().Where(seat => basketSeatIds.Contains(seat.Id));
            var areas = _eventAreaRepository.GetAll();

            var seatPrices = from seat in seats
                             join area in areas on seat.EventAreaId equals area.Id
                             select area.Price;

            return seatPrices.Sum();
        }

        public bool IsBasketItemAvailable(int id)
        {
            return _basketItemRepository.GetAll().Where(link => link.ExpiryDate > DateTime.Now).Any(item => item.SeatId == id);
        }

        public IEnumerable<BasketItem> GetExpiredBasketItems(int userId)
        {
            return _basketItemRepository.GetAll().Where(seat => seat.UserId == userId && seat.ExpiryDate < DateTime.Now).ToList();
        }

        public void ClearExpiredBasketItems(int userId)
        {
            var expiredSeats = GetExpiredBasketItems(userId);
            foreach (var seat in expiredSeats)
            {
                _basketItemRepository.Delete(seat);
            }
        }

        public void ClearBasketItems(int userId)
        {
            var userBasketItems = _basketItemRepository.GetAll().Where(item => item.UserId == userId).ToList();
            foreach (var seat in userBasketItems)
            {
                _basketItemRepository.Delete(seat);
            }
        }

        public void UpdateBasketItemExpiry(int userId)
        {
            var expiryDate = GetExpiryDate();
            UpdateBasketItemExpiryWithExpiryDate(userId, expiryDate);
        }

        public void UpdateBasketItemExpiryWithExpiryDate(int userId, DateTime expiry)
        {
            var currentDate = DateTime.Now;
            var userSeats = _basketItemRepository.GetAll().Where(seat => seat.UserId == userId && seat.ExpiryDate > currentDate).ToList();
            foreach(var seat in userSeats)
            {
                seat.ExpiryDate = expiry;
                _basketItemRepository.Update(seat);
            }
        }

        public IEnumerable<BasketItem> GetCurrentBasketItems(int userId)
        {
            return _basketItemRepository.GetAll().Where(link => link.UserId == userId && link.ExpiryDate > DateTime.Now);
        }
    }
}