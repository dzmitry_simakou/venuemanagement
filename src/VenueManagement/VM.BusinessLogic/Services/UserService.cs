﻿using System.Collections.Generic;
using System.Linq;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRoleRepository _userRoleRepository;

        public UserService(IUserRepository userRepository,
            IRoleRepository roleRepository,
            IUserRoleRepository userRoleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
        }

        public void CreateUserRole(UserRole userRole)
        {
            _userRoleRepository.Create(userRole);
        }

        public void CreateUserRole(int userId, int roleId)
        {
            _userRoleRepository.Create(new UserRole { UserId = userId, RoleId = roleId });
        }

        public void CreateUser(User user)
        {
            _userRepository.Create(user);
        }

        public void DeleteUser(User user)
        {
            _userRepository.Delete(user);
        }

        public Role GetRole(string roleName)
        {
            return _roleRepository.GetAll().Single(role => role.Name.Equals(roleName));
        }

        public IEnumerable<Role> GetRoles(int userId)
        {
            var userRolesIds = _userRoleRepository.GetAll().Where(userRole => userRole.UserId.Equals(userId)).Select(userRole => userRole.RoleId).Distinct();

            return _roleRepository.GetAll().Where(role => userRolesIds.Contains(role.Id));
        }

        public IEnumerable<Role> GetRoles(string username)
        {
            var userId = _userRepository.GetAll().Single(user => user.UserName.Equals(username)).Id;
            var userRolesIds = _userRoleRepository.GetAll().Where(userRole => userRole.UserId.Equals(userId)).Select(userRole => userRole.RoleId).Distinct();

            return _roleRepository.GetAll().Where(role => userRolesIds.Contains(role.Id));
        }

        public IEnumerable<Role> GetRoles()
        {
            return _roleRepository.GetAll();
        }

        public User GetUser(int id)
        {
            return _userRepository.GetAll().SingleOrDefault(user => user.Id == id);
        }

        public User GetUser(string username)
        {
            return _userRepository.GetAll().SingleOrDefault(user => user.UserName == username);
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepository.GetAll();
        }

        public void DeleteUserRole(UserRole userRole)
        {
            _userRoleRepository.Delete(userRole);
        }

        public void DeleteUserRole(int userId, int roleId)
        {
            var userRole = _userRoleRepository.GetAll().Single(link => link.UserId.Equals(userId) && link.RoleId.Equals(roleId));

            _userRoleRepository.Delete(userRole);
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
        }

        public IEnumerable<UserRole> GetUserRoles()
        {
            return _userRoleRepository.GetAll();
        }

        public Role GetRole(int id)
        {
            return _roleRepository.GetAll().Single(role => role.Id.Equals(id));
        }
    }
}
