﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class NoSeatsException : Exception
    {
        public NoSeatsException(string message) : base(message)
        {

        }
    }
}