﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class NotUniqueException : Exception
    {
        public NotUniqueException(string message) : base(message)
        {

        }
    }
}