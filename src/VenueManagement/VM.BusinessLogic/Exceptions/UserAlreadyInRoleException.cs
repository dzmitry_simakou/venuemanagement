﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class UserAlreadyInRoleException : Exception
    {
        public UserAlreadyInRoleException(string message) : base(message)
        {

        }
    }
}