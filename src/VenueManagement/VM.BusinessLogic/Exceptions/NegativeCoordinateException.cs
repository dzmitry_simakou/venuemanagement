﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class NegativeCoordinateException : Exception
    {
        public NegativeCoordinateException(string message) : base(message)
        {

        }
    }
}