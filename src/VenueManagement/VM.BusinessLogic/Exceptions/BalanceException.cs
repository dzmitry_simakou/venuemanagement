﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class BalanceException : Exception
    {
        public BalanceException(string message) : base(message)
        {

        }
    }
}
