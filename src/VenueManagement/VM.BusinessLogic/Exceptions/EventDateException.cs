﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class EventDateException : Exception
    {
        public EventDateException(string message) : base(message)
        {

        }
    }
}
