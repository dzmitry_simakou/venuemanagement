﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class OverlapException : Exception
    {
        public OverlapException(string message) : base(message)
        {

        }
    }
}