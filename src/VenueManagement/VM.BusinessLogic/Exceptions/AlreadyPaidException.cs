﻿using System;

namespace VM.BusinessLogic.Exceptions
{
    public class AlreadyPaidException : Exception
    {
        public AlreadyPaidException(string message) : base(message)
        {

        }
    }
}