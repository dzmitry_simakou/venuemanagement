﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.BusinessLogic.Interfaces
{
    public interface IOrderService : IService
    {
        IEnumerable<Order> GetOrders(int userId);

        void CreateOrderByUserIdAndPrice(int userId, int price);

        void CreateOrder(Order order);

        IEnumerable<OrderSeat> GetOrderSeats(int userId);
    }
}