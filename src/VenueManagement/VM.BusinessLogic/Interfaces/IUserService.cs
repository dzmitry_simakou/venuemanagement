﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.BusinessLogic.Interfaces
{
    public interface IUserService : IService
    {
        void CreateUser(User user);

        void UpdateUser(User user);

        void DeleteUser(User user);

        IEnumerable<User> GetUsers();

        User GetUser(int id);

        User GetUser(string username);

        Role GetRole(string roleName);

        Role GetRole(int id);

        IEnumerable<Role> GetRoles();

        IEnumerable<Role> GetRoles(int userId);

        IEnumerable<Role> GetRoles(string username);

        IEnumerable<UserRole> GetUserRoles();

        void CreateUserRole(UserRole userRole);

        void CreateUserRole(int userId, int roleId);

        void DeleteUserRole(UserRole userRole);

        void DeleteUserRole(int userId, int roleId);
    }
}