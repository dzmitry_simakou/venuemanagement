﻿using System;
using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.BusinessLogic.Interfaces
{
    public interface IBasketService : IService
    {
        void CreateBasketItem(BasketItem basket);

        void DeleteBasketItemById(int id);

        void DeleteBasketItem(BasketItem basket);

        int GetBasketSummaryPrice(int userId);

        bool IsBasketItemAvailable(int id);

        IEnumerable<BasketItem> GetCurrentBasketItems(int userId);

        IEnumerable<BasketItem> GetExpiredBasketItems(int userId);

        void ClearExpiredBasketItems(int userId);

        void ClearBasketItems(int userId);

        void UpdateBasketItemExpiry(int userId);

        void UpdateBasketItemExpiryWithExpiryDate(int userId, DateTime expiry);
    }
}