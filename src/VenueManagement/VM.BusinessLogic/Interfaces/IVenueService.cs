﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.BusinessLogic.Interfaces
{
    public interface IVenueService : IService
    {
        void CreateSeat(Seat seat);

        void UpdateSeat(Seat seat);

        void DeleteSeat(Seat seat);

        IEnumerable<Seat> GetSeats(int areaId);

        void CreateArea(Area area);

        void UpdateArea(Area area);

        void DeleteArea(Area area);

        IEnumerable<Area> GetAreas(int layoutId);

        void CreateLayout(Layout layout);

        void UpdateLayout(Layout layout);

        void DeleteLayout(Layout layout);

        IEnumerable<Layout> GetLayouts();

        IEnumerable<Layout> GetLayoutsByVenueId(int venueId);

        void CreateVenue(Venue venue);

        void UpdateVenue(Venue venue);

        void DeleteVenue(Venue venue);

        IEnumerable<Venue> GetVenues();
    }
}