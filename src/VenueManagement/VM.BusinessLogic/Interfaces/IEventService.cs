﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.BusinessLogic.Interfaces
{
    public interface IEventService : IService
    {
        void CreateEventSeat(EventSeat seat);

        void UpdateEventSeat(EventSeat seat);

        void DeleteEventSeat(EventSeat seat);

        IEnumerable<EventSeat> GetEventSeats();

        void CreateEventArea(EventArea area);

        void UpdateEventArea(EventArea area);

        void DeleteEventArea(EventArea area);

        IEnumerable<EventArea> GetEventAreas();

        void CreateEvent(Event ev);

        void UpdateEvent(Event ev);

        void DeleteEvent(Event ev);

        void DeleteEventByEventId(int eventId);

        bool HasPurchasedSeats(int eventId);

        Event GetEvent(int eventId);

        IEnumerable<Event> GetEvents();

        IEnumerable<Event> GetEventsByLayoutId(int layoutId);

        void ClearExpiredBasketItemsByEventAreaId(int eventAreaId);

        EventArea GetEventArea(int eventAreaId);

        IEnumerable<EventArea> GetEventAreasByEventId(int eventId);

        IEnumerable<EventSeat> GetAvailableEventSeats(int eventAreaId);

        IEnumerable<EventSeat> GetEventSeatsByEventAreaId(int eventAreaId);
    }
}