using System;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VM.Authentication.Infrastructure.Authentication.Stores;
using VM.DataAccess.Context;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories;
using VM.DataAccess.Repositories.Interfaces;
using VM.Authentication.Infrastructure.Authentication.JWT;
using Microsoft.OpenApi.Models;
using VM.Authentication.Infrastructure.Authentication.JWT.Interfaces;
using VM.Authentication.Infrastructure.Options;
using VM.BusinessLogic.Interfaces;
using VM.BusinessLogic.Services;

namespace VM.Authentication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private JwtOptions GetJwtOptions()
        {
            JwtOptions options = new JwtOptions();

            Configuration.GetSection(JwtOptions.SectionName).Bind(options);

            return options;
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            var jwtOptions = GetJwtOptions();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options => 
            {
                options.SaveToken = true;
                options.TokenValidationParameters = jwtOptions.GetTokenValidationParameters();
            });
        }

        private void ConfigureRepositories(IServiceCollection services)
        {
            services.AddTransient<IDbContext, DbContext>(context => new DbContext(Configuration.GetConnectionString("VmDatabase")));
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserRoleRepository, UserRoleRepository>();
        }

        private void ConfigureStores(IServiceCollection services)
        {
            services.AddTransient<IUserStore<User>, UserStore>();
            services.AddTransient<IRoleStore<Role>, RoleStore>();
        }

        private void ConfigureMapping(IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }

        private void ConfigureIdentity(IServiceCollection services)
        {
            services.AddIdentity<User, Role>().AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
            });
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Authentication API", Version = "v1" });

                var securityScheme = new OpenApiSecurityScheme
                {
                    Name = "JWT Authentication",
                    Description = "Enter JWT token from /token action",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };

                options.AddSecurityDefinition("Bearer", securityScheme);

                var securityRequirements = new OpenApiSecurityRequirement
                {
                    {securityScheme, new string[]{ } }
                };

                options.AddSecurityRequirement(securityRequirements);
            });
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JwtOptions>(Configuration.GetSection(JwtOptions.SectionName));

            services.AddTransient<IJwtTokenManager, JwtTokenManager>();
            services.AddTransient<IUserService, UserService>();

            ConfigureRepositories(services);
            ConfigureAuthentication(services);
            ConfigureMapping(services);
            ConfigureIdentity(services);
            ConfigureStores(services);
            ConfigureSwagger(services);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Authentication API");
                options.RoutePrefix = string.Empty;
            });
        }
    }
}
