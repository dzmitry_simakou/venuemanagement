﻿using AutoMapper;
using VM.Authentication.DTO;
using VM.DataAccess.Entities;

namespace VM.Authentication.AutoMapper
{
    public class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
        }
    }
}