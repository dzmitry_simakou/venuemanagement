﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace VM.Authentication.Infrastructure.Options
{
    public class JwtOptions
    {
        public const string SectionName = "JWT";

        public string JwtEncryptionKey { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public int Lifetime { get; set; }
        
        public string SecurityAlgorithm { get; set; }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtEncryptionKey));
        }

        public TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuer,

                ValidateAudience = true,
                ValidAudience = Audience,
                ValidateLifetime = true,

                IssuerSigningKey = GetSymmetricSecurityKey(),
                ValidateIssuerSigningKey = true
            };
        }
    }
}