﻿using System.Collections.Generic;
using System.Security.Claims;
using VM.DataAccess.Entities;

namespace VM.Authentication.Infrastructure.Authentication.JWT.Interfaces
{
    public interface IJwtTokenManager
    {
        string GenerateToken(User user, IList<string> roles);

        bool ValidateToken(string jsonToken);

        ClaimsPrincipal GetClaims(string jsonToken);
    }
}