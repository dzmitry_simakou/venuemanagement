﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using VM.Authentication.Helpers;
using VM.Authentication.Infrastructure.Authentication.JWT.Interfaces;
using VM.Authentication.Infrastructure.Options;
using VM.DataAccess.Entities;

namespace VM.Authentication.Infrastructure.Authentication.JWT
{
    public class JwtTokenManager : IJwtTokenManager
    {
        private readonly JwtOptions _options;

        public JwtTokenManager(IOptions<JwtOptions> options)
        {
            _options = options.Value;
        }

        public string GenerateToken(User user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Locality, user.Lang),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(UserClaimTypes.TimeZone, user.TimeZone)
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, JwtConstants.CookieName, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var currentDate = DateTime.Now;
            var expiryDate = currentDate.Add(TimeSpan.FromMinutes(_options.Lifetime));
            var credentials = new SigningCredentials(_options.GetSymmetricSecurityKey(), _options.SecurityAlgorithm);

            var token = new JwtSecurityToken(
                _options.Issuer,
                _options.Audience,
                notBefore: currentDate,
                claims: claimsIdentity.Claims,
                expires: expiryDate,
                signingCredentials: credentials);

            var encodedToken = new JwtSecurityTokenHandler().WriteToken(token);

            return encodedToken;
        }

        public bool ValidateToken(string jsonToken)
        {

            try
            {
                var handler = new JwtSecurityTokenHandler();

                handler.ValidateToken(jsonToken, _options.GetTokenValidationParameters(), out var jwtToken);

                return jwtToken != null;

            }
            catch (SecurityTokenException)
            {
                return false;
            }
        }

        public ClaimsPrincipal GetClaims(string jsonToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var validations = _options.GetTokenValidationParameters();
            var claimsIdentity = handler.ValidateToken(jsonToken, validations, out _);

            return claimsIdentity;
        }
    }
}