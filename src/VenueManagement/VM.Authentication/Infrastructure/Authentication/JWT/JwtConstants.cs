﻿namespace VM.Authentication.Infrastructure.Authentication.JWT
{
    internal static class JwtConstants
    {
        public const string CookieName = "access_token";
    }
}