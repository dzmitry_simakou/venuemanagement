﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;

namespace VM.Authentication.Infrastructure.Authentication.Stores
{
    public class UserStore : IUserPasswordStore<User>, IUserRoleStore<User>, IQueryableUserStore<User>, IUserClaimStore<User>
    {
        private bool _disposed;

        private readonly IUserService _userService;

        public IQueryable<User> Users => _userService.GetUsers().AsQueryable();

        public UserStore(IUserService userService)
        {
            _userService = userService;
        }

        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            _userService.CreateUser(user);

            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            _userService.DeleteUser(user);

            return Task.FromResult(IdentityResult.Success);
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.FromResult(_userService.GetUsers().FirstOrDefault(user => user.Id == int.Parse(userId)));
        }

        public Task<User> FindByNameAsync(string userName, CancellationToken cancellationToken)
        {
            return Task.FromResult(_userService.GetUsers().FirstOrDefault(user => user.UserName.ToLower() == userName.ToLower()));
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            _userService.UpdateUser(user);

            return Task.FromResult(IdentityResult.Success);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash = passwordHash);
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var roleId = _userService.GetRole(roleName).Id;
            var link = new UserRole { RoleId = roleId, UserId = user.Id };

            _userService.CreateUserRole(link);

            return Task.CompletedTask;
        }

        public Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var roleId = _userService.GetRole(roleName).Id;
            var link = _userService.GetUserRoles().Single(userRole => userRole.RoleId == roleId && userRole.UserId == user.Id);

            _userService.DeleteUserRole(link);

            return Task.CompletedTask;
        }

        public Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
        {
            IList<string> roles = _userService.GetRoles(user.Id).Select(role => role.Name).ToList();

            return Task.FromResult(roles);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var userRoles = _userService.GetRoles(user.Id).Select(role => role.Name).ToList();

            return Task.FromResult(userRoles.Contains(roleName));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // No dispose needed
            }

            _disposed = true;
        }

        public Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName.ToLower());
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            if (normalizedName == null)
            {
                throw new ArgumentNullException(nameof(normalizedName));
            }

            return Task.FromResult(user.UserName);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            return Task.FromResult(user.UserName = userName);
        }

        public Task AddClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<Claim>> GetClaimsAsync(User user, CancellationToken cancellationToken)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim("TimeZone", user.TimeZone)
            };

            var roles = await GetRolesAsync(user, cancellationToken);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
            }

            return claims;
        }

        public Task<IList<User>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ReplaceClaimAsync(User user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        ~UserStore()
        {
            Dispose(false);
        }
    }
}