﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;

namespace VM.Authentication.Infrastructure.Authentication.Stores
{
    public class RoleStore : IQueryableRoleStore<Role>
    {
        private bool _disposed;
        private readonly IUserService _userService;

        public IQueryable<Role> Roles => _userService.GetRoles().AsQueryable();

        public RoleStore(IUserService userService)
        {
            _userService = userService;
        }

        public Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Role> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            return Task.FromResult(_userService.GetRole(int.Parse(roleId)));
        }

        public Task<Role> FindByNameAsync(string name, CancellationToken cancellationToken)
        {
            return Task.FromResult(_userService.GetRole(name));
        }

        public Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // No dispose needed
            }

            _disposed = true;
        }

        public Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetNormalizedRoleNameAsync(Role role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        ~RoleStore()
        {
            Dispose(false);
        }
    }
}