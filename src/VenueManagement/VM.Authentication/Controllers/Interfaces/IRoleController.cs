﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace VM.Authentication.Controllers.Interfaces
{
    internal interface IRoleController
    {
        Task<ActionResult> AddToRole(int userId, string roleName);

        Task<ActionResult> RemoveFromRole(int userId, string roleName);

        Task<ActionResult> GetUserRoles(string username);
    }
}