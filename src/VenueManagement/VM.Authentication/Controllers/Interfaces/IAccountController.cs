﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VM.Authentication.DTO;

namespace VM.Authentication.Controllers.Interfaces
{
    internal interface IAccountController
    {
        Task<ActionResult> Create(UserDTO user, string password);

        Task<ActionResult> Update(UserDTO user, string oldPassword, string newPassword);

        Task<ActionResult> GetUsers(string username);
    }
}