﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VM.DataAccess.Entities;

namespace VM.Authentication.Controllers.Interfaces
{
    internal interface ITokenController
    {
        Task<ActionResult> GetToken(string username, string password);

        Task<ActionResult> RegisterUser(User user, string password);
    }
}