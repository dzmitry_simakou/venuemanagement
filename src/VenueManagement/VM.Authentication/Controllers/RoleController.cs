﻿using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;
using VM.Authentication.Controllers.Interfaces;
using VM.DataAccess.Entities;

namespace VM.Authentication.Controllers
{
    [ApiController]
    [Route("api/Account/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoleController : ControllerBase, IRoleController
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public RoleController(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost]
        [SwaggerResponse(400)]
        public async Task<ActionResult> AddToRole(int userId, string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            var user = await _userManager.FindByIdAsync(userId.ToString());

            var result = await _userManager.AddToRoleAsync(user, role.Name);
            if (result.Errors.Any())
            {
                return BadRequest(result.Errors);
            }

            return Ok();
        }

        [HttpDelete]
        [SwaggerResponse(400)]
        public async Task<ActionResult> RemoveFromRole(int userId, string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            var user = await _userManager.FindByIdAsync(userId.ToString());

            var result = await _userManager.RemoveFromRoleAsync(user, role.Name);
            if (result.Errors.Any())
            {
                return BadRequest(result.Errors);
            }

            return Ok();
        }

        [HttpGet]
        [SwaggerResponse(404)]
        public async Task<ActionResult> GetUserRoles(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user != null)
            {
                var roles = await _userManager.GetRolesAsync(user);

                return Ok(roles);
            }

            return NotFound();
        }
    }
}