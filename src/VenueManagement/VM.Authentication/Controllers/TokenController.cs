﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Annotations;
using VM.Authentication.Controllers.Interfaces;
using VM.Authentication.Infrastructure.Authentication.JWT;
using VM.Authentication.Infrastructure.Authentication.JWT.Interfaces;
using VM.DataAccess.Entities;

namespace VM.Authentication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class TokenController : ControllerBase, ITokenController
    {
        private readonly UserManager<User> _userManager;
        private readonly IJwtTokenManager _jwtTokenManager;

        private async Task<string> GetToken(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            var token = _jwtTokenManager.GenerateToken(user, roles);

            return token;
        }

        public TokenController(UserManager<User> userManager, IJwtTokenManager jwtTokenManager)
        {
            _userManager = userManager;
            _jwtTokenManager = jwtTokenManager;
        }

        [HttpGet]
        [SwaggerResponse(404)]
        public async Task<ActionResult> GetToken(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);
            if(user != null && await _userManager.CheckPasswordAsync(user, password))
            {
                var token = await GetToken(user);
                HttpContext.Response.Cookies.Append(JwtConstants.CookieName, token);
                return Ok(token);
            }

            return NotFound();
        }

        [HttpPost]
        [SwaggerResponse(400)]
        public async Task<ActionResult> RegisterUser([FromBody]User user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);
            if (result.Errors.Any())
            {
                return BadRequest(result.Errors);
            }

            var token = await GetToken(user);

            return Ok(token);
        }
    }
}