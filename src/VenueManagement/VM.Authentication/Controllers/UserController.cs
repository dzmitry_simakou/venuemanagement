﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Swashbuckle.AspNetCore.Annotations;
using VM.Authentication.Controllers.Interfaces;
using VM.Authentication.DTO;
using VM.DataAccess.Entities;

namespace VM.Authentication.Controllers
{
    [Route("api/Account/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase, IAccountController
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserController(UserManager<User> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpPost]
        [SwaggerResponse(400)]
        public async Task<ActionResult> Create([FromBody]UserDTO user, string password)
        {
            var result = await _userManager.CreateAsync(_mapper.Map<User>(user), password);
            if (result.Errors.Any())
            {
                return BadRequest(new IdentityResultDTO(result.Errors.Select(error => error.Description)));
            }

            return Ok();
        }

        [HttpPut]
        [SwaggerResponse(400)]
        public async Task<ActionResult> Update([FromBody]UserDTO user, string oldPassword = null, string newPassword = null)
        {
            if (oldPassword != null && newPassword != null)
            {
                var result = await _userManager.ChangePasswordAsync(_mapper.Map<User>(user), oldPassword, newPassword);
                if (result.Errors.Any())
                {
                    return BadRequest(new IdentityResultDTO(result.Errors.Select(error => error.Description)));
                }

                return Ok(new IdentityResultDTO());
            }

            if(user != null)
            {
                IdentityResult result = await _userManager.UpdateAsync(_mapper.Map<User>(user));
                if (result.Errors.Any())
                {
                    return BadRequest(new IdentityResultDTO(result.Errors.Select(error => error.Description)));
                }

                return Ok(new IdentityResultDTO());
            }

            return BadRequest();
        }

        [HttpGet]
        [SwaggerResponse(404)]
        public async Task<ActionResult> GetUsers(string username)
        {
            if(string.IsNullOrEmpty(username))
            {
                var users = _userManager.Users.ToList();

                return Ok(users);
            }

            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
    }
}