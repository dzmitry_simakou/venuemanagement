﻿using Ninject.Modules;
using VM.DataAccess.Context;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Repositories;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.Services.Modules
{
    public class RepositoryModule : NinjectModule
    {
        private readonly string _connectionString;

        public RepositoryModule(string connectionString) : base()
        {
            _connectionString = connectionString;
        }

        private void LoadContext()
        {
            Kernel.Bind<IDbContext>().ToMethod(context => new DbContext(_connectionString));
        }

        private void LoadRepositories()
        {
            Kernel.Bind<IAreaRepository>().To<AreaRepository>();
            Kernel.Bind<IBasketItemRepository>().To<BasketItemRepository>();
            Kernel.Bind<IEventAreaRepository>().To<EventAreaRepository>();
            Kernel.Bind<IEventRepository>().To<EventRepository>();
            Kernel.Bind<IEventSeatRepository>().To<EventSeatRepository>();
            Kernel.Bind<ILayoutRepository>().To<LayoutRepository>();
            Kernel.Bind<IOrderRepository>().To<OrderRepository>();
            Kernel.Bind<IOrderSeatRepository>().To<OrderSeatRepository>();
            Kernel.Bind<IRoleRepository>().To<RoleRepository>();
            Kernel.Bind<ISeatRepository>().To<SeatRepository>();
            Kernel.Bind<IUserRepository>().To<UserRepository>();
            Kernel.Bind<IUserRoleRepository>().To<UserRoleRepository>();
            Kernel.Bind<IVenueRepository>().To<VenueRepository>();
        }

        public override void Load()
        {
            LoadContext();
            LoadRepositories();
        }
    }
}