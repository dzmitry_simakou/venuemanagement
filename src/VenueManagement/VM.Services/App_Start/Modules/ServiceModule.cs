﻿using Ninject.Modules;
using VM.BusinessLogic.Interfaces;
using VM.BusinessLogic.Services;

namespace VM.Services.Modules
{
    public class ServiceModule : NinjectModule
    {
        public void LoadServices()
        {
            Kernel.Bind<IEventService>().To<EventService>();
            Kernel.Bind<IVenueService>().To<VenueService>();
            Kernel.Bind<IBasketService>().To<BasketService>();
            Kernel.Bind<IOrderService>().To<OrderService>();
        }

        public override void Load()
        {
            LoadServices();
        }
    }
}