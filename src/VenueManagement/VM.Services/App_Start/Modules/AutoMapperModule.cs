﻿using AutoMapper;
using Ninject.Modules;
using VM.Services.AutoMapper;

namespace VM.Services.Modules
{
    public class AutoMapperModule : NinjectModule
    {
        private IConfigurationProvider GetAutoMapperConfiguration()
        {
            return new MapperConfiguration(config => 
            {
                config.AddProfile<EntityProfile>();
            });
        }

        public override void Load()
        {
            Kernel.Bind<IMapper, Mapper>().ToMethod(mapper => new Mapper(GetAutoMapperConfiguration()));
        }
    }
}