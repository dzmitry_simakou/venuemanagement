﻿using AutoMapper;
using VM.DataAccess.Entities;
using VM.Services.DTO;

namespace VM.Services.AutoMapper
{
    internal class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<Area, AreaDTO>().ReverseMap();
            CreateMap<BasketItem, BasketItemDTO>().ReverseMap();
            CreateMap<Event, EventDTO>().ReverseMap();
            CreateMap<EventArea, EventAreaDTO>().ReverseMap();
            CreateMap<EventSeat, EventSeatDTO>().ReverseMap();
            CreateMap<Layout, LayoutDTO>().ReverseMap();
            CreateMap<Order, OrderDTO>().ReverseMap();
            CreateMap<OrderSeat, OrderSeatDTO>().ReverseMap();
            CreateMap<Role, RoleDTO>().ReverseMap();
            CreateMap<Seat, SeatDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<UserRole, UserRoleDTO>().ReverseMap();
            CreateMap<Venue, VenueDTO>().ReverseMap();
        }
    }
}