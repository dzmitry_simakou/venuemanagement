﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Common.WebHost;
using System.Configuration;
using VM.Services.Modules;

namespace VM.Services
{
    public class Global : NinjectHttpApplication
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["VmDatabase"].ConnectionString;

        protected override void OnApplicationStarted()
        {

        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            LoadModules(kernel);

            return kernel;
        }

        private void LoadModules(IKernel kernel)
        {
            var modules = new NinjectModule[]
            {
                new RepositoryModule(_connectionString),
                new ServiceModule(),
                new AutoMapperModule()
            };

            kernel.Load(modules);
        }
    }
}