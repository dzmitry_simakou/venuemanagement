﻿namespace VM.Services.Infrastructure.Authorization
{
    internal class RoleConstants
    {
        public const string UserRole = "User";
        public const string AdminRole = "Admin";
    }
}