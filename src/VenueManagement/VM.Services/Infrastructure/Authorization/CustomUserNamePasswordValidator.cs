﻿using System;
using System.IdentityModel.Selectors;
using System.Linq;
using System.ServiceModel;
using VM.Services.Infrastructure.Authorization.JWT;
using VM.Services.Infrastructure.Authorization.JWT.Interface;

namespace VM.Services.Infrastructure.Authorization
{
    public class CustomUserNamePasswordValidator : UserNamePasswordValidator
    {
        private readonly IJwtTokenManager _jwtTokenManager;

        public CustomUserNamePasswordValidator()
        {
            _jwtTokenManager = new JwtTokenManager();
        }

        public override void Validate(string userName, string password)
        {
            try
            {
                var roleClaims = _jwtTokenManager.GetRoleClaims(password);
                
                if (!roleClaims.Any(role => role.Value.Equals(RoleConstants.UserRole)))
                {
                    throw new FaultException("Authentication failed");
                }
            }
            catch(Exception exception)
            {
                throw new FaultException(exception.Message);
            }
        }
    }
}