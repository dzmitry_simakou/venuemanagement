﻿using System.Collections.Generic;
using System.Security.Claims;

namespace VM.Services.Infrastructure.Authorization.JWT.Interface
{
    public interface IJwtTokenManager
    {
        bool ValidateToken(string jsonToken);

        ClaimsPrincipal GetClaimsIdentity(string jsonToken);

        IList<Claim> GetRoleClaims(string jsonToken);
    }
}