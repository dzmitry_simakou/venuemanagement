﻿namespace VM.Services.Infrastructure.Authorization.JWT
{
    internal class JwtBearerDefaults
    {
        public const string AuthenticationScheme = "Bearer";
        public const string AuthenticationType = "access_token";
        public const string Namespace = "VM.Web";
    }
}