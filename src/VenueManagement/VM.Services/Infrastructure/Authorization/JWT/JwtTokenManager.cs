﻿using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using VM.Services.Infrastructure.Authorization.JWT.Interface;

namespace VM.Services.Infrastructure.Authorization.JWT
{
    internal class JwtTokenManager : IJwtTokenManager
    {
        private string JwtEncryptionKey => ConfigurationManager.AppSettings["JwtEncryptionKey"];

        private string JwtIssuer => ConfigurationManager.AppSettings["JwtIssuer"];

        private string JwtAudience => ConfigurationManager.AppSettings["JwtAudience"];

        private int JwtLifetime => int.Parse(ConfigurationManager.AppSettings["JwtLifetime"]);

        private string JwtSecurityAlgorithm => ConfigurationManager.AppSettings["JwtSecurityAlgorithm"];

        private SymmetricSecurityKey SymmetricSecurityKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtEncryptionKey));

        private string TryExtractToken(string token)
        {
            if (token.Contains(JwtBearerDefaults.AuthenticationScheme))
            {
                // Authorization Scheme string length plus space between scheme and key
                var extractedTokenPosition = JwtBearerDefaults.AuthenticationScheme.Length + 1;

                return token.Substring(extractedTokenPosition);
            }

            return token;
        }

        private TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = JwtIssuer,

                ValidateAudience = true,
                ValidAudience = JwtAudience,

                ValidateLifetime = true,

                IssuerSigningKey = SymmetricSecurityKey,
                ValidateIssuerSigningKey = true,
            };
        }

        public bool ValidateToken(string jsonToken)
        {
            var handler = new JwtSecurityTokenHandler();
            SecurityToken jwtToken;

            try
            {
                handler.ValidateToken(TryExtractToken(jsonToken), GetTokenValidationParameters(), out jwtToken);
            }
            catch (SecurityTokenException)
            {
                return false;
            }

            return jwtToken != null;
        }

        public ClaimsPrincipal GetClaimsIdentity(string jsonToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var validations = GetTokenValidationParameters();
            var claimsPrinciple = handler.ValidateToken(TryExtractToken(jsonToken), validations, out _);

            return claimsPrinciple;
        }

        public IList<Claim> GetRoleClaims(string jsonToken)
        {
            var claimsPrinciple = GetClaimsIdentity(jsonToken);

            return claimsPrinciple.Claims.Where(claim => claim.Type.Equals(ClaimsIdentity.DefaultRoleClaimType)).ToList();
        }
    }
}