﻿using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Threading;
using VM.Services.Attributes.Authorization;
using VM.Services.Infrastructure.Authorization.JWT;
using VM.Services.Infrastructure.Authorization.JWT.Interface;

namespace VM.Services.Infrastructure.Authorization
{
    public class CustomAuthorizationPolicy : IAuthorizationPolicy
    {
        private readonly IJwtTokenManager _jwtTokenManager;

        public const string AnonymousIdentityUsername = "Anonymous";
        public const string AnonymousIdentityType = "Anonymous";

        private bool IsAllowAnonymous()
        {
            var context = OperationContext.Current;
            var action = context.IncomingMessageHeaders.Action;
            var operation = context.EndpointDispatcher.DispatchRuntime.Operations.First(o => o.Action == action);
            var method = context.Host.Description.ServiceType.GetMethod(operation.Name);

            return method != null && method.IsDefined(typeof(AllowAnonymousAttribute), false);
        }

        private GenericPrincipal GetAnonymousPrincipal()
        {
            return new GenericPrincipal(new GenericIdentity(AnonymousIdentityUsername, AnonymousIdentityType), new string[0]);
        }

        private GenericPrincipal GetPrincipal(EvaluationContext evaluationContext)
        {
            var token = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(HeaderNames.Authorization, JwtBearerDefaults.Namespace);
            var username = ((evaluationContext.Properties["Identities"] as List<IIdentity>) ?? throw new InvalidOperationException()).Single().Name;
            var roles = _jwtTokenManager.GetRoleClaims(token).Select(claim => claim.Value).ToArray();

            return new GenericPrincipal(new GenericIdentity(username, JwtBearerDefaults.AuthenticationType), roles);
        }

        public CustomAuthorizationPolicy()
        {
            _jwtTokenManager = new JwtTokenManager();
            Id = Guid.NewGuid().ToString();
        }

        public bool Evaluate(EvaluationContext evaluationContext, ref object state)
        {
            GenericPrincipal principal;
            if (IsAllowAnonymous())
            {
                // Get anonymous principal for AllowAnonymous attribute
                principal = GetAnonymousPrincipal();
                evaluationContext.Properties["Principal"] = principal;
                Thread.CurrentPrincipal = principal;

                // Not request policy anymore
                state = true;

                // Policy applied
                return true;
            }

            // Get principal for PrincipalPermission attribute
            principal = GetPrincipal(evaluationContext);

            // Set principal for PrincipalPermission attribute
            evaluationContext.Properties["Principal"] = principal;
            Thread.CurrentPrincipal = principal;

            // Not request policy anymore
            state = true;

            // Policy applied
            return true;
        }

        public ClaimSet Issuer
        {
            get { return ClaimSet.System; }
        }

        public string Id { get; private set; }
    }
}