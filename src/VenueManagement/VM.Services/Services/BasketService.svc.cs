﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Security.Permissions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Services
{
    public class BasketService : IBasketServiceContract
    {
        private readonly IBasketService _basketService;
        private readonly IMapper _mapper;

        public BasketService(IBasketService basketService, IMapper mapper)
        {
            _basketService = basketService;
            _mapper = mapper;
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void ClearBasketItems(int userId)
        {
            _basketService.ClearBasketItems(userId);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void ClearExpiredBasketItems(int userId)
        {
            _basketService.ClearExpiredBasketItems(userId);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void CreateBasketItem(BasketItemDTO basket)
        {
            _basketService.CreateBasketItem(_mapper.Map<BasketItemDTO,BasketItem>(basket));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void DeleteBasketItemById(int id)
        {
            _basketService.DeleteBasketItemById(id);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void DeleteBasketItem(BasketItemDTO basket)
        {
            _basketService.DeleteBasketItem(_mapper.Map<BasketItemDTO, BasketItem>(basket));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public int GetBasketSummaryPrice(int userId)
        {
            return _basketService.GetBasketSummaryPrice(userId);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public IEnumerable<BasketItemDTO> GetCurrentBasketItems(int userId)
        {
            return _mapper.Map<IEnumerable<BasketItem>, IEnumerable<BasketItemDTO>>(_basketService.GetCurrentBasketItems(userId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public IEnumerable<BasketItemDTO> GetExpiredBasketItems(int userId)
        {
            return _mapper.Map<IEnumerable<BasketItem>, IEnumerable<BasketItemDTO>>(_basketService.GetExpiredBasketItems(userId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public bool IsBasketItemAvailable(int id)
        {
            return _basketService.IsBasketItemAvailable(id);
        }

        public void UpdateBasketItemExpiry(int userId)
        {
            _basketService.UpdateBasketItemExpiry(userId);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public void UpdateBasketItemExpiryWithExpiryDate(int userId, DateTime expiry)
        {
            _basketService.UpdateBasketItemExpiryWithExpiryDate(userId, expiry);
        }
    }
}