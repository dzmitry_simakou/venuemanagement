﻿using AutoMapper;
using System.Collections.Generic;
using System.Security.Permissions;
using System.ServiceModel;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.Services.Contracts;
using VM.Services.Contracts.Faults;
using VM.Services.DTO;

namespace VM.Services.Services
{
    public class OrderService : IOrderServiceContract
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrderService(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public IEnumerable<OrderDTO> GetOrders(int userId)
        {
            return _mapper.Map< IEnumerable<Order>, IEnumerable<OrderDTO>>(_orderService.GetOrders(userId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateOrderByUserIdAndPrice(int userId, int price)
        {
            try 
            { 
                _orderService.CreateOrderByUserIdAndPrice(userId, price);
            }
            catch(BalanceException exception)
            {
                throw new FaultException<BalanceFault>(new BalanceFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateOrder(OrderDTO order)
        {
            try
            {
                _orderService.CreateOrder(_mapper.Map<OrderDTO, Order>(order));
            }
            catch (BalanceException exception)
            {
                throw new FaultException<BalanceFault>(new BalanceFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public IEnumerable<OrderSeatDTO> GetOrderSeats(int userId)
        {
            return _mapper.Map<IEnumerable<OrderSeat>, IEnumerable<OrderSeatDTO>>(_orderService.GetOrderSeats(userId));
        }
    }
}