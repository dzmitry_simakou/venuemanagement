﻿using AutoMapper;
using System.Collections.Generic;
using System.Security.Permissions;
using System.ServiceModel;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.Services.Attributes.Authorization;
using VM.Services.Contracts;
using VM.Services.Contracts.Faults;
using VM.Services.DTO;

namespace VM.Services.Services
{
    public class EventService : IEventServiceContract
    {
        private readonly IEventService _eventService;
        private readonly IMapper _mapper;

        public EventService(IEventService eventService, IMapper mapper)
        {
            _eventService = eventService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        public void ClearExpiredBasketItemsByEventAreaId(int eventAreaId)
        {
            _eventService.ClearExpiredBasketItemsByEventAreaId(eventAreaId);
        }
        
        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateEvent(EventDTO ev)
        {
            try
            {
                _eventService.CreateEvent(_mapper.Map<EventDTO,Event>(ev));
            }
            catch (EventDateException exception)
            {
                throw new FaultException<EventDateFault>(new EventDateFault(), new FaultReason(exception.Message));
            }
            catch (NoSeatsException exception)
            {
                throw new FaultException<NoSeatsFault>(new NoSeatsFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateEventArea(EventAreaDTO area)
        {
            try
            {
                _eventService.CreateEventArea(_mapper.Map<EventAreaDTO, EventArea>(area));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateEventSeat(EventSeatDTO seat)
        {
            try
            {
                _eventService.CreateEventSeat(_mapper.Map<EventSeatDTO, EventSeat>(seat));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteEvent(EventDTO ev)
        {
            try
            {
                _eventService.DeleteEvent(_mapper.Map<EventDTO, Event>(ev));
            }
            catch (AlreadyPaidException exception)
            {
                throw new FaultException<AlreadyPaidFault>(new AlreadyPaidFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteEventArea(EventAreaDTO area)
        {
            _eventService.DeleteEventArea(_mapper.Map<EventAreaDTO, EventArea>(area));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteEventByEventId(int eventId)
        {
            try
            {
                _eventService.DeleteEventByEventId(eventId);
            }
            catch (AlreadyPaidException exception)
            {
                throw new FaultException<AlreadyPaidFault>(new AlreadyPaidFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteEventSeat(EventSeatDTO seat)
        {
            _eventService.DeleteEventSeat(_mapper.Map<EventSeatDTO, EventSeat>(seat));
        }

        [AllowAnonymous]
        public IEnumerable<EventSeatDTO> GetAvailableEventSeats(int eventAreaId)
        {
            return _mapper.Map<IEnumerable<EventSeat>, IEnumerable<EventSeatDTO>>(_eventService.GetAvailableEventSeats(eventAreaId));
        }

        [AllowAnonymous]
        public EventDTO GetEvent(int eventId)
        {
            return _mapper.Map<Event, EventDTO>(_eventService.GetEvent(eventId));
        }

        [AllowAnonymous]
        public EventAreaDTO GetEventArea(int eventAreaId)
        {
            return _mapper.Map<EventArea, EventAreaDTO>(_eventService.GetEventArea(eventAreaId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public IEnumerable<EventAreaDTO> GetEventAreas()
        {
            return _mapper.Map<IEnumerable<EventArea>, IEnumerable<EventAreaDTO>>(_eventService.GetEventAreas());
        }

        [AllowAnonymous]
        public IEnumerable<EventAreaDTO> GetEventAreasByEventId(int eventId)
        {
            return _mapper.Map<IEnumerable<EventArea>, IEnumerable<EventAreaDTO>>(_eventService.GetEventAreasByEventId(eventId));
        }

        [AllowAnonymous]

        public IEnumerable<EventDTO> GetEvents()
        {
            return _mapper.Map<IEnumerable<Event>, IEnumerable<EventDTO>>(_eventService.GetEvents());
        }

        [AllowAnonymous]
        public IEnumerable<EventDTO> GetEventsByLayoutId(int layoutId)
        {
            return _mapper.Map<IEnumerable<Event>, IEnumerable<EventDTO>>(_eventService.GetEventsByLayoutId(layoutId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public IEnumerable<EventSeatDTO> GetEventSeats()
        {
            return _mapper.Map<IEnumerable<EventSeat>, IEnumerable<EventSeatDTO>>(_eventService.GetEventSeats());
        }

        [AllowAnonymous]
        public IEnumerable<EventSeatDTO> GetEventSeatsByEventAreaId(int eventAreaId)
        {
            return _mapper.Map<IEnumerable<EventSeat>, IEnumerable<EventSeatDTO>>(_eventService.GetEventSeatsByEventAreaId(eventAreaId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "User")]
        public bool HasPurchasedSeats(int eventId)
        {
            return _eventService.HasPurchasedSeats(eventId);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateEvent(EventDTO ev)
        {
            try
            {
                _eventService.UpdateEvent(_mapper.Map<EventDTO, Event>(ev));
            }
            catch (AlreadyPaidException exception)
            {
                throw new FaultException<AlreadyPaidFault>(new AlreadyPaidFault(), new FaultReason(exception.Message));
            }
            catch (EventDateException exception)
            {
                throw new FaultException<EventDateFault>(new EventDateFault(), new FaultReason(exception.Message));
            }
            catch (NoSeatsException exception)
            {
                throw new FaultException<NoSeatsFault>(new NoSeatsFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateEventArea(EventAreaDTO area)
        {
            try
            {
                _eventService.UpdateEventArea(_mapper.Map<EventAreaDTO, EventArea>(area));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateEventSeat(EventSeatDTO seat)
        {
            try 
            {
                _eventService.UpdateEventSeat(_mapper.Map<EventSeatDTO, EventSeat>(seat));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }
    }
}