﻿using AutoMapper;
using System.Collections.Generic;
using System.Security.Permissions;
using System.ServiceModel;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Interfaces;
using VM.DataAccess.Entities;
using VM.Services.Contracts;
using VM.Services.Contracts.Faults;
using VM.Services.DTO;

namespace VM.Services.Services
{
    public class VenueService : IVenueServiceContract
    {
        private readonly IVenueService _venueService;
        private readonly IMapper _mapper;

        public VenueService(IVenueService venueService, IMapper mapper)
        {
            _venueService = venueService;
            _mapper = mapper;
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateArea(AreaDTO area)
        {
            try
            {
                _venueService.CreateArea(_mapper.Map<AreaDTO, Area>(area));
            }
            catch (NegativeCoordinateException exception)
            {
                throw new FaultException<NegativeCoordinateFault>(new NegativeCoordinateFault(), new FaultReason(exception.Message));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateLayout(LayoutDTO layout)
        {
            try
            {
                _venueService.CreateLayout(_mapper.Map<LayoutDTO, Layout>(layout));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateSeat(SeatDTO seat)
        {
            try 
            {
                _venueService.CreateSeat(_mapper.Map<SeatDTO, Seat>(seat));
            }
            catch (NegativeCoordinateException exception)
            {
                throw new FaultException<NegativeCoordinateFault>(new NegativeCoordinateFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void CreateVenue(VenueDTO venue)
        {
            try
            {
                _venueService.CreateVenue(_mapper.Map<VenueDTO, Venue>(venue));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteArea(AreaDTO area)
        {
            _venueService.DeleteArea(_mapper.Map<AreaDTO, Area>(area));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteLayout(LayoutDTO layout)
        {
            _venueService.DeleteLayout(_mapper.Map<LayoutDTO, Layout>(layout));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteSeat(SeatDTO seat)
        {
            _venueService.DeleteSeat(_mapper.Map<SeatDTO, Seat>(seat));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void DeleteVenue(VenueDTO venue)
        {
            _venueService.DeleteVenue(_mapper.Map<VenueDTO, Venue>(venue));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public IEnumerable<AreaDTO> GetAreas(int layoutId)
        {
            return _mapper.Map<IEnumerable<Area>, IEnumerable<AreaDTO>>(_venueService.GetAreas(layoutId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public IEnumerable<LayoutDTO> GetLayouts()
        {
            return _mapper.Map<IEnumerable<Layout>, IEnumerable<LayoutDTO>>(_venueService.GetLayouts());
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public IEnumerable<LayoutDTO> GetLayoutsByVenueId(int venueId)
        {
            return _mapper.Map<IEnumerable<Layout>, IEnumerable<LayoutDTO>>(_venueService.GetLayoutsByVenueId(venueId));
        }
        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]

        public IEnumerable<SeatDTO> GetSeats(int areaId)
        {
            return _mapper.Map<IEnumerable<Seat>, IEnumerable<SeatDTO>>(_venueService.GetSeats(areaId));
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public IEnumerable<VenueDTO> GetVenues()
        {
            return _mapper.Map<IEnumerable<Venue>, IEnumerable<VenueDTO>>(_venueService.GetVenues());
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateArea(AreaDTO area)
        {
            try
            {
                _venueService.UpdateArea(_mapper.Map<AreaDTO, Area>(area));
            }
            catch (NegativeCoordinateException exception)
            {
                throw new FaultException<NegativeCoordinateFault>(new NegativeCoordinateFault(), new FaultReason(exception.Message));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateLayout(LayoutDTO layout)
        {
            try
            {
                _venueService.UpdateLayout(_mapper.Map<LayoutDTO, Layout>(layout));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateSeat(SeatDTO seat)
        {
            try
            {
                _venueService.UpdateSeat(_mapper.Map<SeatDTO, Seat>(seat));
            }
            catch (NegativeCoordinateException exception)
            {
                throw new FaultException<NegativeCoordinateFault>(new NegativeCoordinateFault(), new FaultReason(exception.Message));
            }
            catch (OverlapException exception)
            {
                throw new FaultException<OverlapFault>(new OverlapFault(), new FaultReason(exception.Message));
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Admin")]
        public void UpdateVenue(VenueDTO venue)
        {
            try
            {
                _venueService.UpdateVenue(_mapper.Map<VenueDTO, Venue>(venue));
            }
            catch (NotUniqueException exception)
            {
                throw new FaultException<NotUniqueFault>(new NotUniqueFault(), new FaultReason(exception.Message));
            }
        }
    }
}