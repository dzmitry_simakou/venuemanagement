﻿using FluentAssertions;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Services;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using static FluentAssertions.FluentActions;

namespace VM.UnitTests
{
    [TestFixture]
    public class EventServiceTests
    {
        [Test]
        public void CreateEvent_EventInThePast_ThrowsEventDateException()
        {
            // Arrange
            var service = new AutoMocker().CreateInstance<EventService>();
            var newEvent = new Event { Id = 1, LayoutId = 2 };

            // Act
            // Assert
            Invoking(() => service.CreateEvent(newEvent)).Should().Throw<EventDateException>();
        }

        [Test]
        public void CreateEvent_LayoutHasNoAnySeats_ThrowsValidationException()
        {
            // Arrange
            var newEvent = new Event { LayoutId = 2, Description = "New description", Date = new DateTime(2020, 12, 12, 12, 00, 01) };

            var seats = new List<Seat>()
            { 
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 } 
            };

            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEvent(newEvent)).Should().Throw<NoSeatsException>();
        }
        
        [Test]
        public void CreateEvent_NewEventDateOverlapExistingEventDate_ThrowsEventDateException()
        {
            // Arrange
            var newEvent = new Event { LayoutId = 1, Description = "New description", Date = new DateTime(2020, 12, 12, 12, 00, 01) };
        
            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };
        
            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };
        
            var events = new List<Event>
            {
                new Event { Id = 1, LayoutId = 1, Description = "Description", Date = newEvent.Date }
            };
        
            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Layout Name", Description = "Description" }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();
            var layoutRepository = new Mock<ILayoutRepository>();
            var eventRepository = new Mock<IEventRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);
            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);
            eventRepository.Setup(x => x.GetAll()).Returns(events);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);
            mocker.Use(layoutRepository);
            mocker.Use(eventRepository);

            var service = mocker.CreateInstance<EventService>();
        
            // Act
            // Assert
            Invoking(() => service.CreateEvent(newEvent)).Should().Throw<EventDateException>();
        }
        
        [Test]
        public void CreateEvent_NewEvent_NoException()
        {
            // Arrange
            var newEvent = new Event { LayoutId = 1, Description = "New Description", Date = new DateTime(2020, 12, 12, 13, 00, 00) };
        
            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };
        
            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };
        
            var events = new List<Event>()
            {
                new Event { Id = 1, LayoutId = 1, Description = "Description", Date = new DateTime(2020, 12, 12, 12, 00, 01) }
            };
        
            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Layout Name", Description = "Description" }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();
            var layoutRepository = new Mock<ILayoutRepository>();
            var eventRepository = new Mock<IEventRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);
            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);
            eventRepository.Setup(x => x.GetAll()).Returns(events);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);
            mocker.Use(layoutRepository);
            mocker.Use(eventRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEvent(newEvent)).Should().NotThrow();
        }
        
        [Test]
        public void UpdateEvent_EventInThePast_ThrowsEventDateException()
        {
            // Arrange
            var service = new AutoMocker().CreateInstance<EventService>();
            var newEvent = new Event { Id = 1, LayoutId = 2 };
        
            // Act
            // Assert
            Invoking(() => service.UpdateEvent(newEvent)).Should().Throw<EventDateException>();
        }
        
        [Test]
        public void UpdateEvent_LayoutHasNoSeats_ThrowsValidationException()
        {
            // Arrange
            var updatedEvent = new Event { Id = 1, LayoutId = 2, Description = "New Description", Date = new DateTime(2020, 12, 12, 13, 00, 00) };
        
            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };
        
            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };
        
            var events = new List<Event>()
            {
                new Event { Id = 1, LayoutId = 1, Description = "Description", Date = new DateTime(2020, 12, 12, 12, 00, 01) }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();
            var eventRepository = new Mock<IEventRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);
            eventRepository.Setup(x => x.GetAll()).Returns(events);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);
            mocker.Use(eventRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEvent(updatedEvent)).Should().Throw<NoSeatsException>();
        }
        
        [Test]
        public void UpdateEvent_UpdatingEventDateOverlapExistingEventDate_ThrowsValidationException()
        {
            // Arrange
            var updatedEvent = new Event { Id = 2, LayoutId = 1, Description = "New Description", Date = new DateTime(2020, 12, 12, 12, 00, 00) };
        
            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };
        
            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };
        
            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Layout Name", Description = "Description" }
            };

            var events = new List<Event>()
            {
                new Event { Id = 1, LayoutId = 1, Description = "New Description", Date = new DateTime(2020, 12, 12, 12, 00, 00) }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();
            var layoutRepository = new Mock<ILayoutRepository>();
            var eventRepository = new Mock<IEventRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);
            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);
            eventRepository.Setup(x => x.GetAll()).Returns(events);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);
            mocker.Use(layoutRepository);
            mocker.Use(eventRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEvent(updatedEvent)).Should().Throw<EventDateException>();
        }
        
        [Test]
        public void UpdateEvent_UpdatingEventWithPurchasedSeats_ThrowsValidationException()
        {
            // Arrange
            var updatedEvent = new Event { Id = 1 };
        
            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, OrderId = 1 },
                new EventSeat { Id = 2, EventAreaId = 1, OrderId = 2 },
                new EventSeat { Id = 3, EventAreaId = 2, OrderId = 2 },
                new EventSeat { Id = 4, EventAreaId = 2, OrderId = 3 }
            };
        
            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique 1" },
                new EventArea { Id = 2, EventId = 1, Description = "Unique 2" },
                new EventArea { Id = 3, EventId = 2, Description = "Unique 3" }
            };

            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();
            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);
            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventSeatRepository);
            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEvent(updatedEvent)).Should().Throw<AlreadyPaidException>();
        }
        
        [Test]
        public void UpdateEvent_UpdateExistingEvent_NoException()
        {
            // Arrange
            var newEvent = new Event { Id = 1, LayoutId = 1, Description = "New Description", Date = new DateTime(2020, 12, 12, 13, 00, 00) };
        
            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };
        
            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };
        
            var events = new List<Event>()
            {
                new Event { Id = 1, LayoutId = 1, Description = "Description", Date = new DateTime(2020, 12, 12, 12, 00, 01) }
            };
        
            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Layout Name", Description = "Description" }
            };
            
            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();
            var areaRepository = new Mock<IAreaRepository>();
            var layoutRepository = new Mock<ILayoutRepository>();
            var eventRepository = new Mock<IEventRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);
            areaRepository.Setup(x => x.GetAll()).Returns(areas);
            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);
            eventRepository.Setup(x => x.GetAll()).Returns(events);

            mocker.Use(seatRepository);
            mocker.Use(areaRepository);
            mocker.Use(layoutRepository);
            mocker.Use(eventRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEvent(newEvent)).Should().NotThrow();
        }

        [Test]
        public void DeleteEvent_DeletingEventWithPurchasedSeats_ThrowsValidationException()
        {
            // Arrange
            var ev = new Event { Id = 1 };

            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, OrderId = 1 },
                new EventSeat { Id = 2, EventAreaId = 1, OrderId = 2 },
                new EventSeat { Id = 3, EventAreaId = 2, OrderId = 2 },
                new EventSeat { Id = 4, EventAreaId = 2, OrderId = 3 }
            };

            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique 1" },
                new EventArea { Id = 2, EventId = 1, Description = "Unique 2" },
                new EventArea { Id = 3, EventId = 2, Description = "Unique 3" }
            };

            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();
            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);
            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventSeatRepository);
            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.DeleteEvent(ev)).Should().Throw<AlreadyPaidException>();
        }

        [Test]
        public void CreateEventSeat_EventAreaAlreadyContainsSeatWithSameRowAndNumber_ThrowsValidationException()
        {
            // Arrange
            var newEventSeat = new EventSeat { EventAreaId = 1, Row = 1, Number = 1 };
        
            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, Row = 1, Number = 1 }
            }; 
            
            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);

            mocker.Use(eventSeatRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEventSeat(newEventSeat)).Should().Throw<OverlapException>();
        }
        
        [Test]
        public void CreateEventSeat_CreateNewEventSeat_NoException()
        {
            // Arrange
            var newEventSeat = new EventSeat { EventAreaId = 1, Row = 2, Number = 1 };
        
            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, Row = 1, Number = 1 }
            }; 
            
            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);

            mocker.Use(eventSeatRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEventSeat(newEventSeat)).Should().NotThrow<OverlapException>();
        }
        
        [Test]
        public void UpdateEventSeat_AreaAlreadyContainsSeatWithSameRowAndNumber_ThrowsValidationException()
        {
            // Arrange
            var updatedEventSeat = new EventSeat { Id = 2, EventAreaId = 1, Row = 1, Number = 1 };
        
            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, Row = 1, Number = 1 },
                new EventSeat { Id = 2, EventAreaId = 1, Row = 2, Number = 1 }
            };
            
            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);

            mocker.Use(eventSeatRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEventSeat(updatedEventSeat)).Should().Throw<OverlapException>();
        }
        
        [Test]
        public void UpdateEventSeat_UpdateExistingEventSeat_NoException()
        {
            // Arrange
            var updatedEventSeat = new EventSeat { Id = 2, EventAreaId = 2, Row = 2, Number = 1 };
        
            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1, Row = 1, Number = 1 },
                new EventSeat { Id = 2, EventAreaId = 1, Row = 2, Number = 1 }
            };
            
            var mocker = new AutoMocker();

            var eventSeatRepository = new Mock<IEventSeatRepository>();

            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);

            mocker.Use(eventSeatRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEventSeat(updatedEventSeat)).Should().NotThrow<OverlapException>();
        }
        
        [Test]
        public void CreateEventArea_EventAlreadyContainsAreaWithSameDescription_ThrowsValidationException()
        {
            // Arrange
            var newEventArea = new EventArea { EventId = 1, CoordX = 5, CoordY = 5, Description = "Unique" };
        
            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique" }
            };
            
            var mocker = new AutoMocker();

            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEventArea(newEventArea)).Should().Throw<NotUniqueException>();
        }
        
        [Test]
        public void CreateEventArea_CreateNewEventArea_NoException()
        {
            // Arrange
            var newEventArea = new EventArea { EventId = 1, CoordX = 5, CoordY = 5, Description = "Not Unique" };
        
            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.CreateEventArea(newEventArea)).Should().NotThrow<NotUniqueException>();
        }
        
        [Test]
        public void UpdateEventArea_EventAlreadyContainsAreaWithSameDescription_ThrowsValidationException()
        {
            // Arrange
            var updatedEventArea = new EventArea { Id = 2, EventId = 1, CoordX = 5, CoordY = 5, Description = "Unique" };
        
            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEventArea(updatedEventArea)).Should().Throw<NotUniqueException>();
        }
        
        [Test]
        public void UpdateEventArea_EventDoesntContainAreaWithSameDescription_NoException()
        {
            // Arrange
            var updatedEventArea = new EventArea { Id = 2, EventId = 1, CoordX = 5, CoordY = 5, Description = "Not unique" };
        
            var eventAreas = new List<EventArea>()
            {
                new EventArea { Id = 1, EventId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var eventAreaRepository = new Mock<IEventAreaRepository>();

            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(eventAreaRepository);

            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.UpdateEventArea(updatedEventArea)).Should().NotThrow<NotUniqueException>();
        }

        [Test]
        public void ClearExpiredBasketItemsByEventAreaId_ClearExpiredBasketItemsByEventAreaId_NoException()
        {
            // Arrange
            var eventAreaId = 1;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EventService>();

            // Act
            // Assert
            Invoking(() => service.ClearExpiredBasketItemsByEventAreaId(eventAreaId)).Should().NotThrow();
        }
    }
}