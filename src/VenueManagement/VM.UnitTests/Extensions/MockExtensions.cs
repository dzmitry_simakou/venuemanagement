﻿using Moq;
using System.Linq;

namespace VM.UnitTests.Extensions
{
    public static class MockExtensions
    {
        public static void SetupIQueryable<T>(this Mock<T> mock, IQueryable queryable) where T : class, IQueryable
        {
            mock.Setup(r => r.GetEnumerator()).Returns(queryable.GetEnumerator());
            mock.As<IQueryable<T>>().Setup(r => r.Provider).Returns(queryable.Provider);
            mock.As<IQueryable<T>>().Setup(r => r.ElementType).Returns(queryable.ElementType);
            mock.As<IQueryable<T>>().Setup(r => r.Expression).Returns(queryable.Expression);
        }
    }
}