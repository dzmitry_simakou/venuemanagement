﻿using FluentAssertions;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VM.BusinessLogic.Services;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using static FluentAssertions.FluentActions;

namespace VM.UnitTests
{
    [TestFixture]
    public class BasketServiceTests
    {
        [Test]
        public void CreateBasketItem_NewBasketItemWithDefaultDate_NoException()
        {
            // Arrange
            var newBasketItem = new BasketItem();
            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.CreateBasketItem(newBasketItem)).Should().NotThrow();
        }

        [Test]
        public void CreateBasketItem_NewBasketItemWithNotDefaultDate_NoException()
        {
            // Arrange
            var newBasketItem = new BasketItem { ExpiryDate = DateTime.Now };
            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.CreateBasketItem(newBasketItem)).Should().NotThrow();
        }

        [Test]
        public void DeleteBasketItem_DeleteById_NoException()
        {
            // Arrange
            var basketItemId = -1;
            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.DeleteBasketItemById(basketItemId)).Should().NotThrow();
        }

        [Test]
        public void DeleteBasketItem_DeleteByBasketItem_NoException()
        {
            // Arrange
            var basketItem = new BasketItem();
            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.DeleteBasketItem(basketItem)).Should().NotThrow();
        }

        [Test]
        public void GetBasketSummaryPrice_NoBasketItems_ReturnsZero()
        {
            // Arrange
            const int userId = 1;

            var eventSeats = new List<EventSeat>
            {
                new EventSeat { Id = 1, EventAreaId = 1 }
            };

            var eventAreas = new List<EventArea>
            {
                new EventArea { Id = 1, Price = 1000 }
            };

            var mocker = new AutoMocker();

            var basketItemRepository = new Mock<IBasketItemRepository>();
            var eventSeatRepository = new Mock<IEventSeatRepository>();
            var eventAreaRepository = new Mock<IEventAreaRepository>();

            basketItemRepository.Setup(x => x.GetAll()).Returns(new List<BasketItem>());
            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);
            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.GetBasketSummaryPrice(userId).Should().Be(0);
        }

        [Test]
        public void GetBasketSummaryPrice_AnyBasketItem_ReturnsBasketItemsSummary()
        {
            // Arrange
            var userId = 1;

            var basketItems = new List<BasketItem>
            {
                new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 1, UserId = userId },
                new BasketItem { Id = 2, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 2, UserId = userId },
                new BasketItem { Id = 3, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 3, UserId = userId }
            };

            var eventSeats = new List<EventSeat>()
            {
                new EventSeat { Id = 1, EventAreaId = 1 },
                new EventSeat { Id = 2, EventAreaId = 1 },
                new EventSeat { Id = 3, EventAreaId = 2 }
            };

            var eventAreas = new List<EventArea>
            {
                new EventArea { Id = 1, Price = 1000 },
                new EventArea { Id = 2, Price = 1500 }
            };

            // Two basket item with price 10.00 plus one basket item with price 15.00 = 35.00
            var summary = 3500;

            var mocker = new AutoMocker();

            var basketItemRepository = new Mock<IBasketItemRepository>();
            var eventSeatRepository = new Mock<IEventSeatRepository>();
            var eventAreaRepository = new Mock<IEventAreaRepository>();

            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);
            eventSeatRepository.Setup(x => x.GetAll()).Returns(eventSeats);
            eventAreaRepository.Setup(x => x.GetAll()).Returns(eventAreas);

            mocker.Use(basketItemRepository);
            mocker.Use(eventSeatRepository);
            mocker.Use(eventAreaRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.GetBasketSummaryPrice(userId).Should().Be(summary);
        }

        [Test]
        public void IsBasketItemAvailable_Available_ReturnsTrue()
        {
            // Arrange
            var basketItem = new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 1, UserId = 1 };
            var basketItems = new List<BasketItem>()
            {
                basketItem
            };

            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();
            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.IsBasketItemAvailable(basketItem.Id).Should().BeTrue();
        }

        [Test]
        public void IsBasketItemAvailable_NotAvailable_ReturnsFalse()
        {
            // Arrange
            var basketItem = new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(-10), SeatId = 1, UserId = 1 };
            var basketItems = new List<BasketItem>()
            {
                basketItem
            };

            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();
            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.IsBasketItemAvailable(basketItem.Id).Should().BeFalse();
        }

        [Test]
        public void GetExpiredBasketItems_ExpiredAndNotExpiredBasketItems_ReturnsExpiredBasketItems()
        {
            // Arrange
            var userId = 1;
            var expiredItem = new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(-10), SeatId = 1, UserId = userId };
            var basketItems = new List<BasketItem>()
            {
                expiredItem,
                new BasketItem { Id = 2, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 2, UserId = userId }
            };

            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();
            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.GetExpiredBasketItems(userId).Should().Equal(new List<BasketItem> { expiredItem });
        }

        [Test]
        public void ClearBasketItems_ClearBasketItemsByUserId_NoException()
        {
            // Arrange
            var userId = 1;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.ClearBasketItems(userId)).Should().NotThrow();
        }

        [Test]
        public void UpdateBasketItemExpiry_UpdateBasketItemExpiryByUserIdAndExpiryDate_NoException()
        {
            // Arrange
            var userId = 1;
            var expiryDate = DateTime.Now.AddSeconds(30);
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            Invoking(() => service.UpdateBasketItemExpiryWithExpiryDate(userId, expiryDate)).Should().NotThrow();
        }

        [Test]
        public void GetCurrentBasketItems_GetAllCurrentBasketItems_ReturnsCurrentBasketItem()
        {
            // Arrange
            var userId = 1;
            var currentItem = new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 1, UserId = userId };
            var basketItems = new List<BasketItem>()
            {
                currentItem,
                new BasketItem { Id = 2, ExpiryDate = DateTime.Now.AddSeconds(-10), SeatId = 2, UserId = userId }
            };

            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();
            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.GetCurrentBasketItems(userId).Should().Equal(new List<BasketItem> { currentItem });
        }

        [Test]
        public void GetCurrentBasketItems_GetAllCurrentBasketItemsByUserId_ReturnsUserBasketItem()
        {
            // Arrange
            var userId = 1;
            var currentItem = new BasketItem { Id = 1, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 1, UserId = userId };
            var basketItems = new List<BasketItem>()
            {
                currentItem,
                new BasketItem { Id = 2, ExpiryDate = DateTime.Now.AddSeconds(10), SeatId = 2, UserId = 2 },
                new BasketItem { Id = 3, ExpiryDate = DateTime.Now.AddSeconds(-10), SeatId = 3, UserId = userId }
            };

            var mocker = new AutoMocker();
            var basketItemRepository = new Mock<IBasketItemRepository>();
            basketItemRepository.Setup(x => x.GetAll()).Returns(basketItems);

            mocker.Use(basketItemRepository);
            var service = mocker.CreateInstance<BasketService>();

            // Act
            // Assert
            service.GetCurrentBasketItems(userId).Should().Equal(new List<BasketItem> { currentItem });
        }
    }
}