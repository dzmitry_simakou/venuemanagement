﻿using FluentAssertions;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using System.Collections.Generic;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Services;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using static FluentAssertions.FluentActions;

namespace VM.UnitTests
{
    [TestFixture]
    public class OrderServiceTests
    {
        [Test]
        public void GetOrders_GetOrdersByUserId_ReturnsUserOrders()
        {
            // Arrange
            var userId = 1;
            var orders = new List<Order>()
            {
                new Order { Id = 1, UserId = userId }
            };

            var mocker = new AutoMocker();
            var orderRepository = new Mock<IOrderRepository>();
            orderRepository.Setup(x => x.GetAll()).Returns(orders);

            mocker.Use(orderRepository);
            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            service.GetOrders(userId).Should().Equal(orders);
        }

        [Test]
        public void CreateOrder_CreateOrderByUserIdAndPriceHavingUserWithNegativeNewBalance_ThrowsException()
        {
            // Arrange
            var userId = 1;
            var orderPrice = 1000;
            var user = new User { Id = userId, Balance = 0 };
            var users = new List<User>
            { 
                user,
                new User { Id = 2, Balance = 1000 }
            };

            var mocker = new AutoMocker();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetAll()).Returns(users);

            mocker.Use(userRepository);
            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            Invoking(() => service.CreateOrderByUserIdAndPrice(userId, orderPrice)).Should().Throw<BalanceException>();
        }

        [Test]
        public void CreateOrder_CreateOrderByUserIdAndPrice_NoException()
        {            
            // Arrange
            var userId = 1;
            var orderPrice = 1000;
            var user = new User { Id = userId, Balance = 1500 };
            var users = new List<User>
            {
                user,
                new User { Id = 2, Balance = 1000 }
            };

            var mocker = new AutoMocker();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetAll()).Returns(users);

            mocker.Use(userRepository);
            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            Invoking(() => service.CreateOrderByUserIdAndPrice(userId, orderPrice)).Should().NotThrow();
        }

        [Test]
        public void CreateOrder_CreateOrderByOrderWithNegativeNewBalance_ThrowsException()
        {
            // Arrange
            var userId = 1;
            var order = new Order { UserId = userId, Price = 1000 };
            var user = new User { Id = userId, Balance = 0 };
            var users = new List<User>
            { 
                user,
                new User { Id = 2, Balance = 1000 }
            };

            var mocker = new AutoMocker();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetAll()).Returns(users);

            mocker.Use(userRepository);
            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            Invoking(() => service.CreateOrder(order)).Should().Throw<BalanceException>();
        }

        [Test]
        public void CreateOrder_CreateOrderByOrder_NoException()
        {            
            // Arrange
            var userId = 1;
            var order = new Order { UserId = userId, Price = 1000 };
            var user = new User { Id = userId, Balance = 1500 };
            var users = new List<User>
            {
                user,
                new User { Id = 2, Balance = 1000 }
            };

            var mocker = new AutoMocker();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(x => x.GetAll()).Returns(users);

            mocker.Use(userRepository);
            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            Invoking(() => service.CreateOrder(order)).Should().NotThrow();
        }

        [Test]
        public void GetOrderSeats_GetOrderSeatsByUserId_ReturnsOrderSeatsByUserId()
        {            
            // Arrange
            var userId = 1;
            var orders = new List<Order>
            {
                new Order { Id = 1, UserId = userId },
                new Order { Id = 2, UserId = userId },
                new Order { Id = 3, UserId = 2 }
            };

            var firstOrderSeat = new OrderSeat { Id = 1, OrderId = 1, EventSeatId = 1 };
            var secondOrderSeat = new OrderSeat { Id = 2, OrderId = 2, EventSeatId = 2 };

            var orderSeats = new List<OrderSeat>
            {
                firstOrderSeat,
                secondOrderSeat,
                new OrderSeat { Id = 3, OrderId = 3, EventSeatId = 3 }
            };

            var mocker = new AutoMocker();
            var orderRepository = new Mock<IOrderRepository>();
            var orderSeatRepository = new Mock<IOrderSeatRepository>();

            orderRepository.Setup(x => x.GetAll()).Returns(orders);
            orderSeatRepository.Setup(x => x.GetAll()).Returns(orderSeats);

            mocker.Use(orderRepository);
            mocker.Use(orderSeatRepository);

            var service = mocker.CreateInstance<OrderService>();

            // Act
            // Assert
            service.GetOrderSeats(userId).Should().Equal( new List<OrderSeat> { firstOrderSeat, secondOrderSeat } );
        }
    }
}