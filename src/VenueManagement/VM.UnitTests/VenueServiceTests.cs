﻿using FluentAssertions;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using System.Collections.Generic;
using VM.BusinessLogic.Exceptions;
using VM.BusinessLogic.Services;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using static FluentAssertions.FluentActions;

namespace VM.UnitTests
{
    [TestFixture]
    public class VenueServiceTests
    {
        [Test]
        public void CreateSeat_AreaAlreadyContainsSeatWithSameRowAndNumber_ThrowsOverlapException()
        {
            // Arrange
            var newSeat = new Seat { AreaId = 1, Row = 1, Number = 1 };

            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);

            mocker.Use(seatRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateSeat(newSeat)).Should().Throw<OverlapException>();
        }

        [Test]
        public void CreateSeat_NegativeRow_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var seat = new Seat { Row = -1, Number = 0 };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateSeat(seat)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void CreateSeat_NegativeNumber_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var seat = new Seat { Row = 0, Number = -1 };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateSeat(seat)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void CreateSeat_CreateNewSeat_NoException()
        {
            // Arrange
            var newSeat = new Seat { AreaId = 1, Row = 2, Number = 1 };

            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);

            mocker.Use(seatRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateSeat(newSeat)).Should().NotThrow<OverlapException>();
        }

        [Test]
        public void UpdateSeat_AreaAlreadyContainsSeatWithSameRowAndNumber_ThrowsOverlapException()
        {
            // Arrange
            var updatedSeat = new Seat { Id = 2, AreaId = 1, Row = 1, Number = 1 };

            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);

            mocker.Use(seatRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateSeat(updatedSeat)).Should().Throw<OverlapException>();
        }

        [Test]
        public void UpdateSeat_NegativeRow_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var seat = new Seat { Row = -1, Number = 0 };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateSeat(seat)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void UpdateSeat_NegativeNumber_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var seat = new Seat { Row = 0, Number = -1 };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateSeat(seat)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void UpdateSeat_UpdateExistingSeat_NoException()
        {
            // Arrange
            var updatedSeat = new Seat { Id = 2, AreaId = 2, Row = 2, Number = 1 };

            var seats = new List<Seat>()
            {
                new Seat { Id = 1, AreaId = 1, Row = 1, Number = 1 }
            };

            var mocker = new AutoMocker();

            var seatRepository = new Mock<ISeatRepository>();

            seatRepository.Setup(x => x.GetAll()).Returns(seats);

            mocker.Use(seatRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateSeat(updatedSeat)).Should().NotThrow<OverlapException>();
        }

        [Test]
        public void CreateArea_LayoutAlreadyContainsAreaWithSameDescription_ThrowsNotUniqueException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = 5, CoordY = 5, Description = "Unique" };

            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var areaRepository = new Mock<IAreaRepository>();

            areaRepository.Setup(x => x.GetAll()).Returns(areas);

            mocker.Use(areaRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateArea(newArea)).Should().Throw<NotUniqueException>();
        }

        [Test]
        public void CreateArea_NegativeCoordinateX_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = -1, CoordY = 0, Description = "Unique" };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateArea(newArea)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void CreateArea_NegativeCoordinateY_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = 0, CoordY = -1, Description = "Unique" };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateArea(newArea)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void CreateArea_CreateNewArea_NoException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = 5, CoordY = 5, Description = "Not unique" };

            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var areaRepository = new Mock<IAreaRepository>();

            areaRepository.Setup(x => x.GetAll()).Returns(areas);

            mocker.Use(areaRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateArea(newArea)).Should().NotThrow<NotUniqueException>();
        }

        [Test]
        public void UpdateArea_LayoutAlreadyContainsAreaWithSameDescription_ThrowsNotUniqueException()
        {
            // Arrange
            var updatedArea = new Area { Id = 2, LayoutId = 1, CoordX = 5, CoordY = 5, Description = "Unique" };

            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var areaRepository = new Mock<IAreaRepository>();

            areaRepository.Setup(x => x.GetAll()).Returns(areas);

            mocker.Use(areaRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateArea(updatedArea)).Should().Throw<NotUniqueException>();
        }

        [Test]
        public void UpdateArea_UpdateExistingArea_NoException()
        {
            // Arrange
            var updatedArea = new Area { Id = 2, LayoutId = 1, CoordX = 5, CoordY = 5, Description = "Not unique" };

            var areas = new List<Area>()
            {
                new Area { Id = 1, LayoutId = 1, Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var areaRepository = new Mock<IAreaRepository>();

            areaRepository.Setup(x => x.GetAll()).Returns(areas);

            mocker.Use(areaRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateArea(updatedArea)).Should().NotThrow<NotUniqueException>();
        }

        [Test]
        public void UpdateArea_NegativeCoordinateX_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = -1, CoordY = 0, Description = "Unique" };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateArea(newArea)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void UpdateArea_NegativeCoordinateY_ThrowsNegativeCoordinateException()
        {
            // Arrange
            var newArea = new Area { LayoutId = 1, CoordX = 0, CoordY = -1, Description = "Unique" };
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateArea(newArea)).Should().Throw<NegativeCoordinateException>();
        }

        [Test]
        public void UpdateLayout_VenueAlreadyContainsLayoutWithSameName_ThrowsNotUniqueException()
        {
            // Arrange
            var updatedLayout = new Layout { Id = 2, VenueId = 1, Name = "Unique" };

            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Unique" }
            };

            var mocker = new AutoMocker();

            var layoutRepository = new Mock<ILayoutRepository>();

            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);

            mocker.Use(layoutRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateLayout(updatedLayout)).Should().Throw<NotUniqueException>();
        }

        [Test]
        public void UpdateLayout_UpdateExistingLayout_NoException()
        {
            // Arrange
            var updatedLayout = new Layout { Id = 2, VenueId = 1, Name = "Not unique" };

            var layouts = new List<Layout>()
            {
                new Layout { Id = 1, VenueId = 1, Name = "Unique" }
            };

            var mocker = new AutoMocker();

            var layoutRepository = new Mock<ILayoutRepository>();

            layoutRepository.Setup(x => x.GetAll()).Returns(layouts);

            mocker.Use(layoutRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateLayout(updatedLayout)).Should().NotThrow<NotUniqueException>();
        }

        [Test]
        public void CreateVenue_VenueWithSameNameAlreadyExist_ThrowsNotUniqueException()
        {
            // Arrange
            var newVenue = new Venue { Name = "New venue", Description = "Description" };

            var venues = new List<Venue>()
            {
                new Venue { Id = 1, Name = "New venue", Description = "Unique" }
            };

            var mocker = new AutoMocker();

            var venueRepository = new Mock<IVenueRepository>();

            venueRepository.Setup(x => x.GetAll()).Returns(venues);

            mocker.Use(venueRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateVenue(newVenue)).Should().Throw<NotUniqueException>();
        }

        [Test]
        public void CreateVenue_CreateNewVenue_NoException()
        {
            // Arrange
            var newVenue = new Venue { Name = "New venue", Description = "Unique" };

            var venues = new List<Venue>()
            {
                new Venue { Id = 1, Name = "Venue", Description = "Not unique" }
            };

            var mocker = new AutoMocker();

            var venueRepository = new Mock<IVenueRepository>();

            venueRepository.Setup(x => x.GetAll()).Returns(venues);

            mocker.Use(venueRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.CreateVenue(newVenue)).Should().NotThrow<NotUniqueException>();
        }

        [Test]
        public void UpdateVenue_VenueWithSameNameAlreadyExist_ThrowsNotUniqueException()
        {
            // Arrange
            var updatedVenue = new Venue { Id = 2, Name = "New venue 1", Description = "Description" };

            var venues = new List<Venue>()
            {
                new Venue { Id = 1, Name = "New venue 1", Description = "Unique 1" },
                new Venue { Id = 2, Name = "New venue 2", Description = "Unique 2" }
            };

            var mocker = new AutoMocker();

            var venueRepository = new Mock<IVenueRepository>();

            venueRepository.Setup(x => x.GetAll()).Returns(venues);

            mocker.Use(venueRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateVenue(updatedVenue)).Should().Throw<NotUniqueException>();
        }

        [Test]
        public void UpdateVenue_UpdateExistingVenue_NoException()
        {
            // Arrange
            var updatedVenue = new Venue { Id = 2, Name = "New venue 2", Description = "Description" };

            var venues = new List<Venue>()
            {
                new Venue { Id = 1, Name = "New venue 1", Description = "Unique 1" },
                new Venue { Id = 2, Name = "New venue 2", Description = "Unique 2" }
            };

            var mocker = new AutoMocker();

            var venueRepository = new Mock<IVenueRepository>();

            venueRepository.Setup(x => x.GetAll()).Returns(venues);

            mocker.Use(venueRepository);

            var service = mocker.CreateInstance<VenueService>();

            // Act
            // Assert
            Invoking(() => service.UpdateVenue(updatedVenue)).Should().NotThrow<NotUniqueException>();
        }
    }
}