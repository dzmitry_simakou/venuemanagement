﻿using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class RoleDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}