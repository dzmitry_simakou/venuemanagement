﻿using System;
using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class BasketItemDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int SeatId { get; set; }

        [DataMember]
        public DateTime ExpiryDate { get; set; }
    }
}