﻿using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class AreaDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int LayoutId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int CoordX { get; set; }

        [DataMember]
        public int CoordY { get; set; }
    }
}