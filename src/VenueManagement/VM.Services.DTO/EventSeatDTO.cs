﻿using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class EventSeatDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int EventAreaId { get; set; }

        [DataMember]
        public int Row { get; set; }

        [DataMember]
        public int Number { get; set; }

        [DataMember]
        public int? OrderId { get; set; }
    }
}
