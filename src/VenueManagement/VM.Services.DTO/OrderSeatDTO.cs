﻿using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class OrderSeatDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int OrderId { get; set; }

        [DataMember]
        public int EventSeatId { get; set; }

        [DataMember]
        public int Price { get; set; }
    }
}