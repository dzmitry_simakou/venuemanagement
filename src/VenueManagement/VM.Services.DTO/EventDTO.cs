﻿using System;
using System.Runtime.Serialization;
using VM.Services.DTO.Interfaces;

namespace VM.Services.DTO
{
    [DataContract]
    public class EventDTO : IEntityDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int LayoutId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Img { get; set; }
    }
}