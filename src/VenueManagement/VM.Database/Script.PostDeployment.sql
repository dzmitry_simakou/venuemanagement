﻿INSERT INTO dbo.Venue (Name, Description, Address, Phone) VALUES ('Venue 1','Description for Venue 1','Example Street 1','333-333-333')

INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (1, 'Description for Layout 1 of Venue 1', 'Layout Name 1')
INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (1, 'Description for Layout 2 of Venue 1', 'Layout Name 2')
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (1, 'Description for Area 1 of Layout 1', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (1, 'Description for Area 2 of Layout 1', 1, 2)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (2, 'Description for Area 1 of Layout 2', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (2, 'Description for Area 2 of Layout 2', 1, 2)
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 3, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 3, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 3, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 3, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (1, 3, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (2, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (3, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (4, 2, 5);

INSERT INTO dbo.Venue (Name, Description, Address, Phone) VALUES ('Venue 2','Description for Venue 2','Example Street 2','444-444-444')
INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (2, 'Description for Layout 3 of Venue 2', 'Layout Name 3')
INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (2, 'Description for Layout 4 of Venue 2', 'Layout Name 4')
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (3, 'Description for Area 1 of Layout 3', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (3, 'Description for Area 2 of Layout 3', 1, 2)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (4, 'Description for Area 1 of Layout 4', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (4, 'Description for Area 2 of Layout 4', 1, 2)
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (5, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (6, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (7, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (8, 2, 5);

INSERT INTO dbo.Venue (Name, Description, Address, Phone) VALUES ('Venue 3','Description for Venue 3','Example Street 3','555-555-555')
INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (3, 'Description for Layout 5 of Venue 3', 'Layout Name 5')
INSERT INTO dbo.Layout (VenueId, Description, Name)	VALUES (3, 'Description for Layout 6 of Venue 3', 'Layout Name 6')
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (5, 'Description for Area 1 of Layout 5', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (5, 'Description for Area 2 of Layout 5', 1, 2)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (6, 'Description for Area 1 of Layout 6', 1, 1)
INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY) VALUES (6, 'Description for Area 2 of Layout 6', 1, 2)
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (9, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (10, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (11, 2, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 1, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 1, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 1, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 1, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 1, 5);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 2, 1);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 2, 2);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 2, 3);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 2, 4);
INSERT INTO dbo.Seat (AreaId, Row, Number) VALUES (12, 2, 5);

EXEC CreateEvent 'Event Name 1','Event Description 1',1,'2020-11-11 11:00:00','https://i.pinimg.com/originals/e1/a0/52/e1a0522e6ae0bb42c4860945f046969a.png';
EXEC CreateEvent 'Event Name 2','Event Description 2',2,'2021-11-12 11:00:00','https://pbs.twimg.com/profile_images/1177326955907014656/mX-b8IRl.jpg';

INSERT INTO dbo.[Role] (Name) VALUES ('Admin');
INSERT INTO dbo.[Role] (Name) VALUES ('User');
INSERT INTO dbo.[Role] (Name) VALUES ('TestRole1');
INSERT INTO dbo.[Role] (Name) VALUES ('TestRole2');
INSERT INTO dbo.[Role] (Name) VALUES ('TestRole3');

INSERT INTO dbo.[User] (UserName, PasswordHash, Balance, TimeZone, FirstName, LastName, Email) VALUES ('admin','AHKnluAMkf9mI9taYE6QmovqFSelkqorYT17T9PYg71E5tLNIWNBuwo/8LJB0r6kAg==', 100000, 'GMT Standard Time', 'AdminName', 'AdminSurname', 'admin@email.com');
INSERT INTO dbo.[User] (UserName, PasswordHash, Balance, Lang, FirstName, LastName, Email) VALUES ('user','AHKnluAMkf9mI9taYE6QmovqFSelkqorYT17T9PYg71E5tLNIWNBuwo/8LJB0r6kAg==', 100000, 'ru', 'UserName', 'UserSurname', 'user@email.com');

INSERT INTO dbo.[UserRole] (UserId, RoleId) VALUES (1,1);
INSERT INTO dbo.[UserRole] (UserId, RoleId) VALUES (1,2);
INSERT INTO dbo.[UserRole] (UserId, RoleId) VALUES (2,2);