﻿CREATE PROCEDURE [dbo].[CreateBasketItem]
	@userId int,
	@seatId int,
	@expiryDate datetime
AS
BEGIN
	INSERT INTO dbo.BasketItem (UserId, SeatId, ExpiryDate)
	OUTPUT Inserted.Id
	VALUES (@userId, @seatId, @expiryDate)
END