﻿CREATE PROCEDURE [dbo].[UpdateBasketItem]
	@id int,
	@userId int,
	@seatId int,
	@expiryDate datetime
AS
BEGIN
	UPDATE dbo.BasketItem
	SET UserId=@userId, SeatId=@seatId, ExpiryDate = @expiryDate
	WHERE Id=@id
END