﻿CREATE PROCEDURE [dbo].[UpdateOrder]
	@id int,
	@userId int,
	@date datetime,
	@price int
AS
BEGIN
	UPDATE dbo.[Order]
	SET UserId=@userId, Date=@date, Price = @price
	WHERE Id=@id
END