﻿CREATE PROCEDURE [dbo].[CreateOrder]
	@userId int,
	@date datetime,
	@price int
AS
BEGIN
	declare @orderIdTable table (id int)

	INSERT INTO dbo.[Order] (UserId, Date, Price)
	OUTPUT Inserted.Id
	into @orderIdTable
	VALUES (@userId, @date, @price)

	declare @orderId int = (select Id from @orderIdTable)

	insert into OrderSeat (OrderId, EventSeatId, Price)
	select @orderId, eventseat.id, EventArea.Price from eventseat
    join eventarea on eventseat.EventAreaId = eventarea.Id
    where eventseat.Id in (select SeatId from BasketItem where UserId = @userId)

	update EventSeat set OrderId=@orderId where Id in (select SeatId from BasketItem where UserId = @userId)

	select @orderId
END