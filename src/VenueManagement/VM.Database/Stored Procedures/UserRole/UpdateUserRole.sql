﻿CREATE PROCEDURE [dbo].[UpdateUserRole]
	@id int,
	@userId int,
	@roleId int
AS
BEGIN
	UPDATE dbo.[UserRole]
	SET UserId = @userId, RoleId = @roleId
	WHERE Id=@id
END