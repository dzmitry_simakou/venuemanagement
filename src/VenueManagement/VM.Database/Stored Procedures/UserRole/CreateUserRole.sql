﻿CREATE PROCEDURE [dbo].[CreateUserRole]
	@userId int,
	@roleId int
AS
BEGIN
	INSERT INTO [dbo].[UserRole] (UserId, RoleId)
	VALUES (@userId, @roleId)
END