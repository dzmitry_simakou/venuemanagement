﻿CREATE PROCEDURE [dbo].[CreateArea]
	@layoutId int,
	@description nvarchar(50),
	@coordX int,
	@coordY int
AS
BEGIN
	INSERT INTO dbo.Area (LayoutId, Description, CoordX, CoordY)
	OUTPUT Inserted.Id
	VALUES (@layoutId, @description, @coordX, @coordY)
END