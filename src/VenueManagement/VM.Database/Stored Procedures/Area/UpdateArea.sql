﻿CREATE PROCEDURE [dbo].[UpdateArea]
	@id int,
	@layoutId int,
	@description nvarchar(200),
	@coordX int,
	@coordY int
AS
BEGIN
	UPDATE dbo.Area
	SET LayoutId=@layoutId, Description=@description, CoordX=@coordX, CoordY=@coordY
	WHERE Id=@id
END