﻿CREATE PROCEDURE [dbo].[UpdateRole]
	@id int,
	@name varchar(50)
AS
BEGIN
	UPDATE dbo.[Role]
	SET Name = @name
	WHERE Id=@id
END