﻿CREATE PROCEDURE [dbo].[CreateRole]
	@name varchar(50)
AS
BEGIN
	INSERT INTO [dbo].[Role] (Name)
	OUTPUT Inserted.Id
	VALUES (@name)
END