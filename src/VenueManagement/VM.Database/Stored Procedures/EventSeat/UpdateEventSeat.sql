﻿CREATE PROCEDURE [dbo].[UpdateEventSeat]
	@id int,
	@eventAreaId int,
	@row int,
	@number int,
	@orderId int
AS
BEGIN
	UPDATE dbo.EventSeat
	SET EventAreaId=@eventAreaId, Row=@row, Number=@number, OrderId=@orderId
	WHERE Id=@id
END