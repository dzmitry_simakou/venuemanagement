﻿CREATE PROCEDURE [dbo].[CreateEventSeat]
	@eventAreaId int,
	@row int,
	@number int,
	@orderId int
AS
BEGIN
	INSERT INTO dbo.EventSeat (EventAreaId, Row, Number, OrderId)
	OUTPUT Inserted.Id
	VALUES (@eventAreaId, @row, @number, @orderId)
END