﻿CREATE PROCEDURE [dbo].[UpdateSeat]
	@id int,
	@areaId int,
	@row int,
	@number int
AS
BEGIN
	UPDATE dbo.Seat
	SET AreaId=@areaId, Row=@row, Number=@number
	WHERE Id=@id
END