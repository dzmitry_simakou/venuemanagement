﻿CREATE PROCEDURE [dbo].[CreateSeat]
	@areaId int,
	@row int,
	@number int
AS
BEGIN
	INSERT INTO dbo.Seat (AreaId, Row, Number)
	OUTPUT Inserted.Id
	VALUES (@areaId, @row, @number);
END