﻿CREATE PROCEDURE [dbo].[CreateLayout]
	@venueId int,
	@description nvarchar(50),
	@name nvarchar(50)
AS
BEGIN
	INSERT INTO dbo.Layout (VenueId, Description, Name)
	OUTPUT Inserted.Id
	VALUES (@venueId, @description, @name)
END