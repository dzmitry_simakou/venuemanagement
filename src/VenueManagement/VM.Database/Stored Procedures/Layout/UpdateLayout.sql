﻿CREATE PROCEDURE [dbo].[UpdateLayout]
	@id int,
	@venueId int,
	@description nvarchar(50),
	@name nvarchar(50)
AS
BEGIN
	UPDATE dbo.Layout
	SET VenueId=@venueId, Description=@description, Name=@name
	WHERE Id=@id
END