﻿CREATE PROCEDURE [dbo].[DeleteLayout]
	@id int
AS
BEGIN
	DELETE FROM dbo.Seat WHERE AreaId in (select id from area where LayoutId = @id)
	DELETE FROM dbo.Area WHERE LayoutId=@id
	DELETE FROM dbo.Layout WHERE Id=@id
END