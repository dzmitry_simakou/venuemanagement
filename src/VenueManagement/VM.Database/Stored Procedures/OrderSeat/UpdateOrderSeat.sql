﻿CREATE PROCEDURE [dbo].[UpdateOrderSeat]
	@id int,
	@orderId int,
	@eventSeatId int,
	@price int
AS
BEGIN
	UPDATE dbo.[OrderSeat]
	SET OrderId=@orderId, EventSeatId=@eventSeatId, Price=@price
	WHERE Id=@id
END