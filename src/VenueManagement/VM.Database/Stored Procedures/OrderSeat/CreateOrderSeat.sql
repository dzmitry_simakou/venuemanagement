﻿CREATE PROCEDURE [dbo].[CreateOrderSeat]
	@orderId int,
	@eventSeatId int,
	@price int
AS
BEGIN
	declare @orderSeatIdTable table (id int)

	INSERT INTO dbo.[OrderSeat] (OrderId, EventSeatId, Price)
	OUTPUT Inserted.Id
	INTO @orderSeatIdTable
	VALUES (@orderId, @eventSeatId, @price)

	UPDATE dbo.[EventSeat]
	SET OrderId=@orderId
	WHERE Id=@eventSeatId

	select id from @orderSeatIdTable
END