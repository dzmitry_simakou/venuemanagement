﻿CREATE PROCEDURE [dbo].[CreateEvent]
	@name varchar(50),
	@description varchar(50),
	@layoutId int,
	@date datetime,
	@img varchar(300)
AS
BEGIN
	--- Event.Id table
	declare @eventIdTable table (id int)

	--- Insert Event into dbo.Event and get inserted event ID
	insert into dbo.Event (LayoutId, Description, Name, Date, Img) 
	output inserted.id
	into @eventIdTable
	values (@layoutId, @description, @name, @date, @img)

	--- Event.Id
	declare @eventId int = (select Id from @eventIdTable)

	--- Get Areas to insert
	declare @areas table(id int identity, areaid int, layoutId int, description varchar(50), coordX int, coordY int);

	--- Insert Areas into temporary Areas table
	insert into @areas (areaid, layoutId, description, coordX, coordY)
	select Id, LayoutId, Description, CoordX, CoordY from Area where LayoutId = @layoutId
	
	--- EventAreaIds table
	declare @eventAreaIds table(id int identity, eventAreaId int);
	
	--- Insert Areas into dbo.EventArea and output (EventArea.Id)s to EventAreaIds table
	insert into EventArea (EventId, CoordX, CoordY, Description, Price)
	output inserted.Id into @eventAreaIds (eventAreaId)
	select @eventId, coordX, coordY, description, 0 from @areas
	
	--- Create table for linking records in Area and created records in EventArea
	declare @areaToEventAreaLink table(areaId int, eventAreaId int);
	insert into @areaToEventAreaLink
	select a.areaid, ea.eventAreaId 
	from @areas a
	join @eventAreaIds ea on a.id = ea.id

	--- Insert Seats into dbo.EventSeat using temporary @areaToEventAreaLink
	insert into EventSeat(EventAreaId, Row, Number)
	select link.eventAreaId, row, number from seat s
	join @areaToEventAreaLink link on s.AreaId = link.areaId

	--- Return Event.Id
	select @eventId
END