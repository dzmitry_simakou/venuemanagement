﻿CREATE PROCEDURE [dbo].[DeleteEvent]
	@id int
AS
BEGIN
	delete from EventSeat where EventAreaId in (select Id from EventArea where EventId = @id)
	delete from EventArea where EventId = @id
	delete from dbo.Event where Id=@id
END