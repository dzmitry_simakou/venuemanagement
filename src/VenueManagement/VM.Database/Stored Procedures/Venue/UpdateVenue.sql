﻿CREATE PROCEDURE [dbo].[UpdateVenue]
	@id int,
	@name nvarchar(50),
	@description nvarchar(50),
	@address nvarchar(50),
	@phone nvarchar(50)
AS
BEGIN
	UPDATE dbo.Venue
	SET Name=@name, Description=@description, Address=@address, Phone=@phone
	WHERE Id=@id
END