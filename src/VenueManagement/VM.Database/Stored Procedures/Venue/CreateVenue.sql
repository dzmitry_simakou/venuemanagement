﻿CREATE PROCEDURE [dbo].[CreateVenue]
	@name nvarchar(50),
	@description nvarchar(50),
	@address nvarchar(50),
	@phone nvarchar(50)
AS
BEGIN
	INSERT INTO dbo.Venue (Name, Description, Address, Phone)
	OUTPUT Inserted.Id
	VALUES (@name, @description, @address, @phone)
END