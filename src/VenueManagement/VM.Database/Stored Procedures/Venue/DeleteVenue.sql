﻿CREATE PROCEDURE [dbo].[DeleteVenue]
	@id int
AS
BEGIN
	delete from dbo.Seat where AreaId in (select Id from Area where LayoutId in (select Id from Layout where VenueId = @id))
	delete from dbo.Area where LayoutId in (select Id from Layout where VenueId = @id)
	delete from dbo.Layout where VenueId = @id
	DELETE FROM dbo.Venue WHERE Id=@id
END