﻿CREATE PROCEDURE [dbo].[CreateEventArea]
	@eventId int,
	@description nvarchar(50),
	@coordX int,
	@coordY int,
	@price int
AS
BEGIN
	INSERT INTO dbo.EventArea(EventId, Description, CoordX, CoordY, Price)
	OUTPUT Inserted.Id
	VALUES (@eventId, @description, @coordX, @coordY, @price)
END