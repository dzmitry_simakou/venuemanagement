﻿CREATE PROCEDURE [dbo].[DeleteEventArea]
	@id int
AS
BEGIN
	delete from dbo.EventSeat where EventAreaId = @id
	delete from dbo.EventArea where Id=@id
END