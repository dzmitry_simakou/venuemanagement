﻿CREATE PROCEDURE [dbo].[UpdateEventArea]
	@id int,
	@eventId int,
	@description nvarchar(50),
	@coordX int,
	@coordY int,
	@price int
AS
BEGIN
	UPDATE dbo.EventArea
	SET EventId=@eventId, [Description]=@description, CoordX=@coordX, CoordY=@coordY, Price=@price
	WHERE Id=@id
END