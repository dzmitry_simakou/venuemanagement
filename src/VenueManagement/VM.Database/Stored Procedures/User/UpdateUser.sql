﻿CREATE PROCEDURE [dbo].[UpdateUser]
	@id int,
	@userName varchar(50),
	@passwordHash varchar(255),
	@balance int, 
	@lang varchar(5),
	@timeZone varchar(100),
	@firstName varchar(50) = null,
	@lastName varchar(50) = null,
	@email varchar(50) = null
AS
BEGIN
	UPDATE dbo.[User]
	SET UserName = @userName, PasswordHash = @passwordHash, Balance = @balance, Lang = @lang, TimeZone = @timeZone, Firstname = @firstName, Lastname = @lastName, Email = @email
	WHERE Id=@id
END