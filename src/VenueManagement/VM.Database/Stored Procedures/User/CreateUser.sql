﻿CREATE PROCEDURE [dbo].[CreateUser]
	@userName varchar(50),
	@passwordHash varchar(255),
	@lang varchar(5),
	@timeZone varchar(100),
	@firstName varchar(50),
	@lastName varchar(50),
	@email varchar(50)
AS
BEGIN
	INSERT INTO [dbo].[User] (UserName, PasswordHash, Lang, TimeZone, Firstname, Lastname, Email)
	OUTPUT Inserted.Id
	VALUES (@userName, @passwordHash, @lang, @timeZone, @firstName, @lastName, @email)
END