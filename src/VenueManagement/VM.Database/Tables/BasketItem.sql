﻿CREATE TABLE [dbo].[BasketItem]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserId] INT NOT NULL, 
    [SeatId] INT NOT NULL, 
    [ExpiryDate] DATETIME NOT NULL
)
