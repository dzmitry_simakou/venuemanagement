﻿CREATE TABLE [dbo].[Layout]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [VenueId] INT NOT NULL,
    [Description] VARCHAR(50) NULL, 
    [Name] VARCHAR(50) NOT NULL
)
