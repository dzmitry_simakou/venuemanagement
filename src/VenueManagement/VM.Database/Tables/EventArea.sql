﻿CREATE TABLE [dbo].[EventArea]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [EventId] INT NOT NULL, 
    [Description] VARCHAR(50) NULL, 
    [CoordX] INT NOT NULL, 
    [CoordY] INT NOT NULL, 
    [Price] INT NOT NULL
)
