﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] VARCHAR(50) NOT NULL, 
    [PasswordHash] VARCHAR(255) NOT NULL, 
    [Balance] INT NOT NULL DEFAULT 0, 
    [Lang] VARCHAR(5) NOT NULL DEFAULT 'en', 
    [TimeZone] VARCHAR(100) NOT NULL DEFAULT 'GMT Standard Time', 
    [FirstName] VARCHAR(50) NULL, 
    [LastName] VARCHAR(50) NULL, 
    [Email] VARCHAR(50) NOT NULL 
)
