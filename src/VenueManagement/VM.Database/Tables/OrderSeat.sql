﻿CREATE TABLE [dbo].[OrderSeat]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [OrderId] INT NOT NULL, 
    [EventSeatId] INT NOT NULL, 
    [Price] INT NOT NULL
)
