﻿CREATE TABLE [dbo].[Area]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LayoutId] INT NOT NULL, 
    [Description] VARCHAR(50) NULL, 
    [CoordX] INT NOT NULL, 
    [CoordY] INT NOT NULL
)
