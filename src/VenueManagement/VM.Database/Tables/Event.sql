﻿CREATE TABLE [dbo].[Event]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(50) NULL, 
    [Description] VARCHAR(50) NULL, 
    [LayoutId] INT NOT NULL, 
    [Date] DATETIME NOT NULL, 
    [Img] VARCHAR(300) NULL 
)