﻿CREATE TABLE [dbo].[Venue]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Description] VARCHAR(50) NULL, 
    [Address] VARCHAR(50) NULL, 
    [Phone] VARCHAR(50) NULL, 
    [Name] VARCHAR(50) NULL
)
