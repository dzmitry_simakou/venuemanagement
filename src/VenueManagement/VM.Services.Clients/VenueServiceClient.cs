﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.Clients.Interfaces;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Clients
{
    public class VenueServiceClient : BaseClient<IVenueServiceContract>, IVenueServiceClient
    {
        public VenueServiceClient(ICredentialsProvider provider, string endpointAddress)
            : base(provider, endpointAddress)
        {

        }

        public void CreateArea(AreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateArea(area);
            }
        }

        public void CreateLayout(LayoutDTO layout)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateLayout(layout);
            }
        }

        public void CreateSeat(SeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateSeat(seat);
            }
        }

        public void CreateVenue(VenueDTO venue)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateVenue(venue);
            }
        }

        public void DeleteArea(AreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteArea(area);
            }
        }

        public void DeleteLayout(LayoutDTO layout)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteLayout(layout);
            }
        }

        public void DeleteSeat(SeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteSeat(seat);
            }
        }

        public void DeleteVenue(VenueDTO venue)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteVenue(venue);
            }
        }

        public IEnumerable<AreaDTO> GetAreas(int layoutId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetAreas(layoutId);
            }
        }

        public IEnumerable<LayoutDTO> GetLayouts()
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetLayouts();
            }
        }

        public IEnumerable<LayoutDTO> GetLayoutsByVenueId(int venueId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetLayoutsByVenueId(venueId);
            }
        }

        public IEnumerable<SeatDTO> GetSeats(int areaId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetSeats(areaId);
            }
        }

        public IEnumerable<VenueDTO> GetVenues()
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetVenues();
            }
        }

        public void UpdateArea(AreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateArea(area);
            }
        }

        public void UpdateLayout(LayoutDTO layout)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateLayout(layout);
            }
        }

        public void UpdateSeat(SeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateSeat(seat);
            }
        }

        public void UpdateVenue(VenueDTO venue)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateVenue(venue);
            }
        }
    }
}