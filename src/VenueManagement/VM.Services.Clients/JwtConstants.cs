﻿namespace VM.Services.Clients
{
    internal class JwtConstants
    {
        public const string Header = "Authorization";

        public const string AuthorizationScheme = "Bearer";

        public const string Namespace = "VM.Web";
    }
}
