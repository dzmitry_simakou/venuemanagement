﻿using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using VM.Services.Clients.Interfaces;

namespace VM.Services.Clients
{
    public class AnonymousBaseClient<T> : ClientBase<T>, IClient where T : class
    {
        private X509Certificate2 GetCertificate()
        {
            using (var store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);

                var result = store.Certificates.Find(X509FindType.FindByThumbprint, "e7b527c6306c7cddfbf4af64bb3f27e276e2e60d", true);

                if(result.Count == 0)
                {
                    return null;
                }

                return result[0];
            }
        }

        public AnonymousBaseClient(string endpointAddress)
            : base(new WSHttpBinding()
            {
                Security = new WSHttpSecurity
                {
                    Mode = SecurityMode.Transport,
                    Message = new NonDualMessageSecurityOverHttp()
                    {
                        ClientCredentialType = MessageCredentialType.None
                    },
                    Transport = new HttpTransportSecurity()
                    {
                        ClientCredentialType = HttpClientCredentialType.Certificate,
                    }
                }
            }, new EndpointAddress(endpointAddress))
        {
            ClientCredentials.ClientCertificate.Certificate = GetCertificate();
        }
    }
}