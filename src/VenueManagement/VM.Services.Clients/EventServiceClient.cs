﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.Clients.Interfaces;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Clients
{
    public class EventServiceClient : BaseClient<IEventServiceContract>, IEventServiceClient
    {
        public EventServiceClient(ICredentialsProvider provider, string endpointAddress)
            : base(provider, endpointAddress)
        {

        }

        public void ClearExpiredBasketItemsByEventAreaId(int eventAreaId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.ClearExpiredBasketItemsByEventAreaId(eventAreaId);
            }
        }

        public void CreateEvent(EventDTO ev)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateEvent(ev);
            }
        }

        public void CreateEventArea(EventAreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateEventArea(area);
            }
        }

        public void CreateEventSeat(EventSeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateEventSeat(seat);
            }
        }

        public void DeleteEvent(EventDTO ev)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteEvent(ev);
            }
        }

        public void DeleteEventArea(EventAreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteEventArea(area);
            }
        }

        public void DeleteEventByEventId(int eventId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteEventByEventId(eventId);
            }
        }

        public void DeleteEventSeat(EventSeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteEventSeat(seat);
            }
        }

        public IEnumerable<EventSeatDTO> GetAvailableEventSeats(int eventAreaId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetAvailableEventSeats(eventAreaId);
            }
        }

        public EventDTO GetEvent(int eventId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEvent(eventId);
            }
        }

        public EventAreaDTO GetEventArea(int eventAreaId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventArea(eventAreaId);
            }
        }

        public IEnumerable<EventAreaDTO> GetEventAreas()
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventAreas();
            }
        }

        public IEnumerable<EventAreaDTO> GetEventAreasByEventId(int eventId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventAreasByEventId(eventId);
            }
        }

        public IEnumerable<EventDTO> GetEvents()
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEvents();
            }
        }

        public IEnumerable<EventDTO> GetEventsByLayoutId(int layoutId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventsByLayoutId(layoutId);
            }
        }

        public IEnumerable<EventSeatDTO> GetEventSeats()
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventSeats();
            }
        }

        public IEnumerable<EventSeatDTO> GetEventSeatsByEventAreaId(int eventAreaId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetEventSeatsByEventAreaId(eventAreaId);
            }
        }

        public bool HasPurchasedSeats(int eventId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.HasPurchasedSeats(eventId);
            }
        }

        public void UpdateEvent(EventDTO ev)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateEvent(ev);
            }
        }

        public void UpdateEventArea(EventAreaDTO area)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateEventArea(area);
            }
        }

        public void UpdateEventSeat(EventSeatDTO seat)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateEventSeat(seat);
            }
        }
    }
}