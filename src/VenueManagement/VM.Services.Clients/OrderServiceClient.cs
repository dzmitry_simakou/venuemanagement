﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.Clients.Interfaces;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Clients
{
    public class OrderServiceClient : BaseClient<IOrderServiceContract>, IOrderServiceClient
    {
        public OrderServiceClient(ICredentialsProvider provider, string endpointAddress) 
            : base(provider, endpointAddress)
        {

        }

        public void CreateOrder(OrderDTO order)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateOrder(order);
            }
        }

        public void CreateOrderByUserIdAndPrice(int userId, int price)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateOrderByUserIdAndPrice(userId, price);
            }
        }

        public IEnumerable<OrderDTO> GetOrders(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetOrders(userId);
            }
        }

        public IEnumerable<OrderSeatDTO> GetOrderSeats(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetOrderSeats(userId);
            }
        }
    }
}