﻿using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using VM.Services.Clients.Interfaces;

namespace VM.Services.Clients
{
    public abstract class BaseClient<T> : ClientBase<T>, IClient, ITokenClient where T : class
    {
        private X509Certificate2 GetCertificate()
        {
            using (var store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);

                var thumbprint = ConfigurationManager.AppSettings.Get("X509CertificateThumbprint");
                var result = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, true);

                if (result.Count == 0)
                {
                    return null;
                }

                return result[0];
            }
        }

        public BaseClient(ICredentialsProvider provider, string endpointAddress)
            : base(new WSHttpBinding()
            {
                Security = new WSHttpSecurity
                {
                    Mode = SecurityMode.TransportWithMessageCredential,
                    Message = new NonDualMessageSecurityOverHttp()
                    {
                        ClientCredentialType = MessageCredentialType.UserName,
                        NegotiateServiceCredential = false
                    },
                    Transport = new HttpTransportSecurity()
                    {
                        ClientCredentialType = HttpClientCredentialType.Certificate
                    },
                }
            }, new EndpointAddress(endpointAddress))
        {
            ClientCredentials.UserName.UserName = provider.GetUserName();
            ClientCredentials.UserName.Password = provider.GetPassword();
            ClientCredentials.ClientCertificate.Certificate = GetCertificate();
        }

        public virtual MessageHeader TokenHeader => new MessageHeader<string>($"{JwtConstants.AuthorizationScheme} {ClientCredentials.UserName.Password}").GetUntypedHeader(JwtConstants.Header, JwtConstants.Namespace);

    }
}