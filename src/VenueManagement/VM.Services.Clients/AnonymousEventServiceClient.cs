﻿using System.Collections.Generic;
using VM.Services.Clients.Interfaces;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Clients
{
    public class AnonymousEventServiceClient : AnonymousBaseClient<IAnonymousEventServiceContract>, IAnonymousEventServiceClient
    {
        public AnonymousEventServiceClient(string endpointAddress)
            : base(endpointAddress)
        {

        }

        public void ClearExpiredBasketItemsByEventAreaId(int eventAreaId)
        {
            Channel.ClearExpiredBasketItemsByEventAreaId(eventAreaId);
        }

        public IEnumerable<EventSeatDTO> GetAvailableEventSeats(int eventAreaId)
        {
            return Channel.GetAvailableEventSeats(eventAreaId);
        }

        public EventDTO GetEvent(int eventId)
        {
            return Channel.GetEvent(eventId);
        }

        public EventAreaDTO GetEventArea(int eventAreaId)
        {
            return Channel.GetEventArea(eventAreaId);
        }

        public IEnumerable<EventAreaDTO> GetEventAreasByEventId(int eventId)
        {
            return Channel.GetEventAreasByEventId(eventId);
        }

        public IEnumerable<EventDTO> GetEvents()
        {
            return Channel.GetEvents();
        }

        public IEnumerable<EventDTO> GetEventsByLayoutId(int layoutId)
        {
            return Channel.GetEventsByLayoutId(layoutId);
        }

        public IEnumerable<EventSeatDTO> GetEventSeatsByEventAreaId(int eventAreaId)
        {
            return Channel.GetEventSeatsByEventAreaId(eventAreaId);
        }
    }
}