﻿using System.ServiceModel.Channels;

namespace VM.Services.Clients.Interfaces
{
    public interface ITokenClient
    {
        MessageHeader TokenHeader { get; }
    }
}