﻿namespace VM.Services.Clients.Interfaces
{
    public interface ICredentialsProvider
    {
        string GetUserName();

        string GetPassword();
    }
}