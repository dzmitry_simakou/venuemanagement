﻿using VM.Services.Contracts;

namespace VM.Services.Clients.Interfaces
{
    public interface IOrderServiceClient : IClient, IOrderServiceContract
    {
    }
}