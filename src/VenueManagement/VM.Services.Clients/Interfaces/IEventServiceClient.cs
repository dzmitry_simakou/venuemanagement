﻿using VM.Services.Contracts;

namespace VM.Services.Clients.Interfaces
{
    public interface IEventServiceClient : IClient, IEventServiceContract
    {
    }
}