﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.Clients.Interfaces;
using VM.Services.Contracts;
using VM.Services.DTO;

namespace VM.Services.Clients
{
    public class BasketServiceClient : BaseClient<IBasketServiceContract>, IBasketServiceClient
    {
        public BasketServiceClient(ICredentialsProvider provider, string endpointAddress)
            : base(provider, endpointAddress)
        {

        }

        public void ClearBasketItems(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.ClearBasketItems(userId);
            }
        }

        public void ClearExpiredBasketItems(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.ClearExpiredBasketItems(userId);
            }
        }

        public void CreateBasketItem(BasketItemDTO basket)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.CreateBasketItem(basket);
            }
        }

        public void DeleteBasketItemById(int id)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteBasketItemById(id);
            }
        }

        public void DeleteBasketItem(BasketItemDTO basket)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.DeleteBasketItem(basket);
            }
        }

        public int GetBasketSummaryPrice(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetBasketSummaryPrice(userId);
            }
        }

        public IEnumerable<BasketItemDTO> GetCurrentBasketItems(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetCurrentBasketItems(userId);
            }
        }

        public IEnumerable<BasketItemDTO> GetExpiredBasketItems(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.GetExpiredBasketItems(userId);
            }
        }

        public bool IsBasketItemAvailable(int id)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                return Channel.IsBasketItemAvailable(id);
            }
        }

        public void UpdateBasketItemExpiry(int userId)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateBasketItemExpiry(userId);
            }
        }

        public void UpdateBasketItemExpiryWithExpiryDate(int userId, DateTime expiry)
        {
            using (new OperationContextScope(InnerChannel))
            {
                OperationContext.Current.OutgoingMessageHeaders.Add(TokenHeader);

                Channel.UpdateBasketItemExpiryWithExpiryDate(userId, expiry);
            }
        }
    }
}