﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Linq;
using VM.DataAccess.Entities;
using VM.Web.Infrastructure.Mailing.Interfaces;
using VM.Web.Infrastructure.Authentication;

namespace VM.Web.Infrastructure.Mailing
{
    internal class MailClient : IMailClient
    {
        private readonly SmtpClient _smtpClient;

        public MailClient()
        {
            _smtpClient = new SmtpClient();
        }

        public void SendConfirmation(IdentityUser user, IEnumerable<BasketItem> items, int summary)
        {
            var to = new MailAddress(user.Email, $"{user.FirstName} {user.LastName}");
            
            using(var messageBuilder = new MessageBuilder(MailingDefaults.DefaultFrom, to))
            {
                messageBuilder.AddSubject(MailingDefaults.DefaultSubject);
                messageBuilder.AddBody(MailingDefaults.DefaultContent);
                messageBuilder.AppendBody($"Your order contains following items: ");
                messageBuilder.AppendBody(items.Select(item => item.ToString()).ToArray(), "; ");
                messageBuilder.AppendBody($"Total: {summary}");

                var mailMessage = messageBuilder.GetMessage();

                _smtpClient.Send(mailMessage);
            }

        }

        public void Dispose()
        {
            _smtpClient.Dispose();
        }
    }
}