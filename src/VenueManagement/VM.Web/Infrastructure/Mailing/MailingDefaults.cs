﻿using System.Net.Mail;

namespace VM.Web.Infrastructure.Mailing
{
    internal class MailingDefaults
    {
        public static MailAddress DefaultFrom = new MailAddress("vm.web@mail.com", "VM.Web AutoResponse");
        public const string DefaultContent = "This is confirmation message for your order. ";
        public const string DefaultSubject = "VM.Web Order Confirmation";
    }
}