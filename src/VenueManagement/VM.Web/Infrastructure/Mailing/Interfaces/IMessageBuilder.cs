﻿using System;
using System.Net.Mail;

namespace VM.Web.Infrastructure.Mailing.Interfaces
{
    public interface IMessageBuilder : IDisposable
    {
        void AddTo(MailAddress to);

        void AddTo(MailAddress[] to);

        void RemoveTo(string to);

        void RemoveTo(string[] to);

        void AddSubject(string subject);

        void AddBody(string body);

        void ClearBody();

        void AppendBody(string content);

        void AppendBody(string[] content, string separator = "");

        void AppendLineBody(string content);

        void AppendLineBody(string[] content);

        void AddFrom(MailAddress from);

        MailMessage GetMessage();
    }
}