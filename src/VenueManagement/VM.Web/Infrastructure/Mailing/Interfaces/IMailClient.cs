﻿using System;
using System.Collections.Generic;
using VM.DataAccess.Entities;
using VM.Web.Infrastructure.Authentication;

namespace VM.Web.Infrastructure.Mailing.Interfaces
{
    public interface IMailClient : IDisposable
    {
        void SendConfirmation(IdentityUser user, IEnumerable<BasketItem> items, int summary);
    }
}