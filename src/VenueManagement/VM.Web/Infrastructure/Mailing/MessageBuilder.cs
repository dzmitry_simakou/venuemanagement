﻿using System.Linq;
using System.Net.Mail;
using System.Text;
using VM.Web.Infrastructure.Mailing.Interfaces;

namespace VM.Web.Infrastructure.Mailing
{
    public class MessageBuilder : IMessageBuilder
    {
        private readonly StringBuilder _bodyStringBuilder;
        private readonly MailMessage _message;

        public MessageBuilder()
        {
            _bodyStringBuilder = new StringBuilder();
            _message = new MailMessage();
        }
        
        public MessageBuilder(MailAddress from, MailAddress to)
        {
            _bodyStringBuilder = new StringBuilder();
            _message = new MailMessage(from, to);
        }

        public MessageBuilder(string from, string to)
        {
            _bodyStringBuilder = new StringBuilder();
            _message = new MailMessage(from, to);
        }

        public void AddBody(string content)
        {
            ClearBody();

            _bodyStringBuilder.Append(content);
        }

        public void ClearBody()
        {
            if (_bodyStringBuilder.Length != 0)
            {
                _bodyStringBuilder.Clear();
            }
        }

        public void AppendBody(string content)
        {
            _bodyStringBuilder.Append(content);
        }

        public void AppendBody(string[] content, string separator = "")
        {
            foreach(var item in content)
            {
                _bodyStringBuilder.Append($"{item}{separator}");
            }
        }

        public void AppendLineBody(string content)
        {
            _bodyStringBuilder.AppendLine(content);
        }

        public void AppendLineBody(string[] content)
        {
            foreach(var item in content)
            {
                _bodyStringBuilder.AppendLine(item);
            }
        }

        public void AddFrom(MailAddress from)
        {
            _message.From = from;
        }

        public void AddSubject(string subject)
        {
            _message.Subject = subject;
        }

        public void AddTo(MailAddress to)
        {
            _message.To.Add(to);
        }

        public MailMessage GetMessage()
        {
            _message.Body = _bodyStringBuilder.ToString();

            return _message;
        }

        public void Dispose()
        {
            _message.Dispose();
        }

        public void AddTo(MailAddress[] to)
        {
            foreach(var address in to)
            {
                _message.To.Add(address);
            }
        }

        public void RemoveTo(string to)
        {
            var address = _message.To.Single(addr => addr.Address.Equals(to));

            _message.To.Remove(address);
        }

        public void RemoveTo(string[] toArray)
        {
            foreach (var address in toArray)
            {
                var to = _message.To.Single(addr => addr.Address.Equals(address));

                _message.To.Remove(to);
            }
        }
    }
}