﻿using System.Security.Claims;

namespace VM.Web.Infrastructure.Authentication.JWT.Interfaces
{
    public interface IJwtTokenManager
    {
        bool ValidateToken(string jsonToken);

        ClaimsPrincipal GetClaimsPrinciple(string jsonToken);
    }
}