﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Linq;
using System.Threading.Tasks;
using VM.Web.Infrastructure.Authentication.JWT.Interfaces;

namespace VM.Web.Infrastructure.Authentication.JWT
{
    public class JwtCookieAuthenticationProvider : CookieAuthenticationProvider
    {
        public JwtCookieAuthenticationProvider()
        {
            IJwtTokenManager jwtTokenManager = new JwtTokenManager();

            Func<CookieValidateIdentityContext, Task> TokenValidation = async context =>
            {
                var token = context.Options.CookieManager.GetRequestCookie(context.OwinContext, JwtConstants.CookieName);
                if (!jwtTokenManager.ValidateToken(token))
                {
                    context.RejectIdentity();
                }
            };

            OnValidateIdentity += TokenValidation;

            Action<CookieResponseSignInContext> TokenAuthentication = context =>
            {
                var token = context.Identity.Claims.FirstOrDefault(claim => claim.Type == JwtConstants.ClaimType).Value;
                context.Options.CookieManager.AppendResponseCookie(context.OwinContext, JwtConstants.CookieName, token, new CookieOptions() { HttpOnly = true, Secure = true });
            };

            OnResponseSignIn += TokenAuthentication;
        }
    }
}