﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using VM.Web.Infrastructure.Authentication.JWT.Interfaces;

namespace VM.Web.Infrastructure.Authentication.JWT
{
    internal class JwtTokenManager : IJwtTokenManager
    {
        public bool ValidateToken(string jsonToken)
        {
            var handler = new JwtSecurityTokenHandler();

            try
            {
                handler.ValidateToken(jsonToken, JwtAuthenticationOptions.GetTokenValidationParameters(), out var jwtToken);

                return jwtToken != null;
            }
            catch
            {
                return false;
            }
        }

        public ClaimsPrincipal GetClaimsPrinciple(string jsonToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var validations = JwtAuthenticationOptions.GetTokenValidationParameters();
            var claimsPrinciple = handler.ValidateToken(jsonToken, validations, out _);

            return claimsPrinciple;
        }
    }
}