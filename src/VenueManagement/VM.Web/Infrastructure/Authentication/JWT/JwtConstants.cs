﻿namespace VM.Web.Infrastructure.Authentication.JWT
{
    internal static class JwtConstants
    {
        public const string CookieName = "access_token";

        public const string ClaimType = "access_token";
    }
}