﻿using Microsoft.IdentityModel.Tokens;
using System.Configuration;
using System.Text;

namespace VM.Web.Infrastructure.Authentication.JWT
{
    public class JwtAuthenticationOptions
    {
        public const int Lifetime = 60;

        public const string Issuer = "VM.Authentication";

        public const string Audience = "VM.Web";

        public const string SecurityAlgorithm = SecurityAlgorithms.HmacSha256;

        public static string EncryptionKey => ConfigurationManager.AppSettings.Get("JwtEncryptionKey");

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(EncryptionKey));
        }

        public static TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuer,

                ValidateAudience = true,
                ValidAudience = Audience,
                ValidateLifetime = true,

                IssuerSigningKey = GetSymmetricSecurityKey(),
                ValidateIssuerSigningKey = true,
            };
        }
    }
}