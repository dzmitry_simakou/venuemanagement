﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using VM.Authentication.Clients.Interfaces;
using VM.Web.Infrastructure.Authentication.Stores;

namespace VM.Web.Infrastructure.Authentication.Managers
{
    public class UserManager : UserManager<IdentityUser, int>
    {
        private readonly IUserClient _userService;

        public UserManager(UserStore userStore, IUserClient userService) 
            : base(userStore)
        {
            _userService = userService;
        }

        public override async Task<IdentityResult> ChangePasswordAsync(int userId, string currentPassword, string newPassword)
        {
            var user = (await _userService.GetAllUsers()).Single(usr => usr.Id.Equals(userId));
            if (user == null)
            {
                return IdentityResult.Failed();
            }

            await _userService.UpdatePassword(user, currentPassword, newPassword);

            return IdentityResult.Success;
        }
    }
}