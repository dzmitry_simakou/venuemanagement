﻿using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using VM.Web.Infrastructure.Authentication.JWT.Interfaces;

namespace VM.Web.Infrastructure.Authentication.Managers
{
    public class SignInManager : SignInManager<IdentityUser, int>
    {
        private readonly IJwtTokenManager _jwtTokenManager;

        private ClaimsIdentity GetClaimsIdentity(string token)
        {
            var claims = _jwtTokenManager.GetClaimsPrinciple(token).Claims;
            var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            claimsIdentity.AddClaim(new Claim("access_token", token));
            return claimsIdentity;
        }

        public SignInManager(UserManager userManager, 
            IAuthenticationManager authenticationManager,
            IJwtTokenManager jwtTokenManager)
            : base(userManager, authenticationManager)
        {
            _jwtTokenManager = jwtTokenManager;
        }

        public void SignInAsync(string token, bool isPersistent, bool rememberBrowser)
        {
            ClaimsIdentity claimsIdentity = GetClaimsIdentity(token);
            AuthenticationManager.SignOut("ExternalCookie", "TwoFactorCookie");
            if (rememberBrowser)
            {
                ClaimsIdentity rememberBrowserIdentity = AuthenticationManager.CreateTwoFactorRememberBrowserIdentity(claimsIdentity.GetUserId());
                IAuthenticationManager authenticationManager = AuthenticationManager;
                AuthenticationProperties properties = new AuthenticationProperties();
                properties.IsPersistent = isPersistent;
                ClaimsIdentity[] claimsIdentityArray =
                {
                    claimsIdentity,
                    rememberBrowserIdentity
                };

                authenticationManager.SignIn(properties, claimsIdentityArray);
            }
            else
            {
                IAuthenticationManager authenticationManager = AuthenticationManager;
                AuthenticationProperties properties = new AuthenticationProperties();
                properties.IsPersistent = isPersistent;
                ClaimsIdentity[] claimsIdentityArray =
                {
                    claimsIdentity
                };

                authenticationManager.SignIn(properties, claimsIdentityArray);
            }
        }
    }
}