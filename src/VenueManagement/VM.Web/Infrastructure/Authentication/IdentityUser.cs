﻿using Microsoft.AspNet.Identity;
using VM.DataAccess.Entities;

namespace VM.Web.Infrastructure.Authentication
{
    public class IdentityUser : User, IUser<int>
    {
        public string Token { get; set; }
    }
}