﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VM.Authentication.Clients.Interfaces;
using VM.Authentication.DTO;

namespace VM.Web.Infrastructure.Authentication.Stores
{
    public class UserStore : IUserPasswordStore<IdentityUser, int>, IUserRoleStore<IdentityUser, int>
    {
        private bool _disposed;

        private readonly IUserClient _userClient;
        private readonly IRoleClient _roleClient;
        private readonly IMapper _mapper;

        public UserStore(IUserClient userClient, 
            IRoleClient roleClient,
            IMapper mapper)
        {
            _userClient = userClient;
            _roleClient = roleClient;
            _mapper = mapper;
        }

        public Task CreateAsync(IdentityUser user)
        {
            return _userClient.CreateUser(_mapper.Map<UserDTO>(user), user.PasswordHash);
        }

        public Task DeleteAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<IdentityUser> FindByIdAsync(int userId)
        {
            var users = await _userClient.GetAllUsers();
            var user = users.Single(usr => usr.Id.Equals(userId));

            return _mapper.Map<IdentityUser>(user);
        }

        public async Task<IdentityUser> FindByNameAsync(string userName)
        {
            var user = await _userClient.GetUser(userName);

            return _mapper.Map<UserDTO, IdentityUser>(user);
        }

        public async Task UpdateAsync(IdentityUser user)
        {
            var mappedUser = _mapper.Map<UserDTO>(user);

            await _userClient.UpdateUser(mappedUser);
        }

        public async Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;

            await _userClient.UpdateUser(_mapper.Map<UserDTO>(user));
        }

        public async Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            var userDTO = await _userClient.GetUser(user.UserName);

            return userDTO.PasswordHash;
        }

        public async Task<bool> HasPasswordAsync(IdentityUser user)
        {
            var userDTO = await _userClient.GetUser(user.UserName);

            return string.IsNullOrEmpty(userDTO.PasswordHash);
        }

        public Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            return _roleClient.AddToRole(user.Id, roleName);
        }

        public Task RemoveFromRoleAsync(IdentityUser user, string roleName)
        {
            return _roleClient.RemoveFromRole(user.Id, roleName);
        }

        public async Task<IList<string>> GetRolesAsync(IdentityUser user)
        {
            return await _roleClient.GetUserRoles(user.UserName);
        }

        public async Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
        {
            var roles = (await _userClient.GetUser(user.UserName)).Roles;

            return roles.Contains(roleName);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _userClient.Dispose();
                _roleClient.Dispose();
            }

            _disposed = true;
        }

        ~UserStore()
        {
            Dispose(false);
        }
    }
}