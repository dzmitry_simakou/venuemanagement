﻿using System.ComponentModel.DataAnnotations;
using VM.Web.Infrastructure.Authentication;

namespace VM.Web.Models.Account
{
    public class UpdateAccountModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        public int Balance { get; set; }

        public decimal BalanceToAdd { get; set; }

        [Display(Name = "OldPassword")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Display(Name = "NewPassword")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string SelectedLanguage { get; set; }

        public string SelectedTimeZone { get; set; }

        public bool HasEmptyPasswords => string.IsNullOrEmpty(OldPassword) && string.IsNullOrEmpty(NewPassword) && string.IsNullOrEmpty(ConfirmPassword);

        public UpdateAccountModel()
        {
        }

        public UpdateAccountModel(IdentityUser user)
        {
            Id = user.Id;
            Balance = user.Balance;
            UserName = user.UserName;
            SelectedLanguage = user.Lang;
            SelectedTimeZone = user.TimeZone;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
        }

        public IdentityUser GetUser()
        {
            return new IdentityUser { Id = Id, Lang = SelectedLanguage, TimeZone = SelectedTimeZone, UserName = UserName, FirstName = FirstName, LastName = LastName, Email = Email };
        }

        public int GetBalanceToAdd()
        {
            return (int) (BalanceToAdd * 100);
        }
    }
}