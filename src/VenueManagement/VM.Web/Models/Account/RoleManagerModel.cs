﻿namespace VM.Web.Models.Account
{
    public class RoleManagerModel
    {
        public int UserName { get; set; }

        public int Role { get; set; }
    }
}