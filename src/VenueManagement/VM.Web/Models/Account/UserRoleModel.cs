﻿namespace VM.Web.Models.Account
{
    public class UserRoleModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }
    }
}