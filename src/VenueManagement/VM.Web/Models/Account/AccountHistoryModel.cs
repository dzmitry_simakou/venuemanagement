﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.Web.Models.Account
{
    public class AccountHistoryModel
    {
        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<OrderSeat> OrderSeats { get; set; }
    }
}