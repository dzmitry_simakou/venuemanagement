﻿using System.ComponentModel.DataAnnotations;
using VM.DataAccess.Entities;

namespace VM.Web.Models.Account
{
    public class RegistrationModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string SelectedLanguage { get; set; }

        [Required]
        public string SelectedTimeZone { get; set; }

        public User GetUser()
        {
            return new User { Lang = SelectedLanguage, TimeZone = SelectedTimeZone, UserName = UserName, FirstName = FirstName, LastName = LastName, Email = Email };
        }
    }
}