﻿using System;
using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.Web.Models.Basket
{
    public class BasketModel
    {
        public DateTime BasketExpiryDate { get; set; }

        public IEnumerable<BasketItem> Seats { get; set; }

        public int Summary { get; set; }

        public string SummaryLabel { get; set; }

        public int Balance { get; set; }

        public string BalanceLabel { get; set; }
    }
}