﻿using System;
using System.ComponentModel.DataAnnotations;
using VM.Web.DataAnnotations;

namespace VM.Web.Models.Event
{
    public class CreateEventModel
    {
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        public int LayoutId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime Time { get; set; }

        [FutureDate]
        [DataType(DataType.DateTime)]
        public DateTime DateTime => new DateTime(Date.Year, Date.Month, Date.Day, Time.Hour, Time.Minute, Time.Second).ToUniversalTime();

        public string ImageLink { get; set; }

        public VM.DataAccess.Entities.Event GetEvent()
        {
            return new VM.DataAccess.Entities.Event { Date = DateTime, Description = Description, Img = ImageLink, LayoutId = LayoutId, Name = Name };
        }

        public void SetEvent(VM.DataAccess.Entities.Event ev)
        {
            Name = ev.Name;
            Description = ev.Description;
            LayoutId = ev.LayoutId;
            Date = new DateTime(ev.Date.Year, ev.Date.Month, ev.Date.Day);
            Time = ev.Date.ToLocalTime();
            ImageLink = ev.Img;
        }
    }
}