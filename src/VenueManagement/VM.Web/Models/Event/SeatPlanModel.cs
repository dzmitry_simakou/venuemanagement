﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.Web.Models.Event
{
    public class SeatPlanModel
    {
        public IEnumerable<EventSeat> Seats { get; set; }

        public EventArea EventArea { get; set; }

        public int MaxCoordX { get; set; }

        public int MaxCoordY { get; set; }
    }
}