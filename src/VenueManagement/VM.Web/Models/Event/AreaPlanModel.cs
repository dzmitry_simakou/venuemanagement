﻿using System.Collections.Generic;
using VM.DataAccess.Entities;

namespace VM.Web.Models.Event
{
    public class AreaPlanModel
    {
        public string EventName { get; set; }

        public string EventDescription { get; set; }

        public IEnumerable<EventArea> Areas { get; set; }

        public int X { get; set; }

        public int Y { get; set; }
    }
}