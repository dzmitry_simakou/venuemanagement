﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using VM.DataAccess.Entities;
using VM.Web.DataAnnotations;

namespace VM.Web.Models.Event
{
    public class UpdateEventModel
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        public int LayoutId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime Time { get; set; }

        [FutureDate]
        [DataType(DataType.DateTime)]
        public DateTime DateTime => new DateTime(Date.Year, Date.Month, Date.Day, Time.Hour, Time.Minute, Time.Second).ToUniversalTime();

        public string ImageLink { get; set; }

        public IList<EventAreaModel> EventAreas { get; set; }

        public SelectList Layouts { get; set; }

        public UpdateEventModel()
        {

        }

        public UpdateEventModel(VM.DataAccess.Entities.Event ev, IEnumerable<EventArea> eventAreas, IEnumerable<Layout> layouts)
        {
            Id = ev.Id;
            Name = ev.Name;
            Description = ev.Description;
            LayoutId = ev.LayoutId;
            Date = ev.Date.ToLocalTime();
            ImageLink = ev.Img;
            EventAreas = new List<EventAreaModel>();
            foreach(var area in eventAreas)
            {
                EventAreas.Add(new EventAreaModel(area));
            }

            SetLayouts(layouts);
        }

        public void SetLayouts(IEnumerable<Layout> layouts)
        {
            Layouts = new SelectList(layouts, "Id", "Name");
        }

        public DataAccess.Entities.Event GetEvent()
        {
            return new DataAccess.Entities.Event { Id = Id, Date = DateTime, Description = Description, Img = ImageLink, LayoutId = LayoutId, Name = Name };
        }
    }
}