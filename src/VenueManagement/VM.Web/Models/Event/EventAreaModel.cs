﻿using System.ComponentModel.DataAnnotations;
using VM.DataAccess.Entities;
using VM.Web.DataAnnotations;

namespace VM.Web.Models.Event
{
    public class EventAreaModel
    {
        public int Id { get; set; }

        public int EventId { get; set; }

        public string Description { get; set; }

        public int CoordX { get; set; }

        public int CoordY { get; set; }

        [NotNegativeValidation]
        [DisplayFormat(DataFormatString = "{0:0.##}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        public EventAreaModel()
        {
            
        }

        public EventAreaModel(EventArea area)
        {
            CoordX = area.CoordX;
            CoordY = area.CoordY;
            Description = area.Description;
            EventId = area.EventId;
            Id = area.Id;
            Price = (decimal) area.Price / 100;
        }

        public EventArea ToEventArea()
        {
            return new EventArea { CoordX = CoordX, CoordY = CoordY, Description = Description, EventId = EventId, Id = Id, Price = (int)(Price * 100) };
        }
    }
}