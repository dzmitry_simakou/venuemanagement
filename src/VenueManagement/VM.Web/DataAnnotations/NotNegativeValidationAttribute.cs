﻿using System.ComponentModel.DataAnnotations;

namespace VM.Web.DataAnnotations
{
    public class NotNegativeValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value is decimal)
            {
                if ((decimal)value < 0)
                {
                    return new ValidationResult($"{value} should be positive");
                }
            }

            return null;
        }
    }
}