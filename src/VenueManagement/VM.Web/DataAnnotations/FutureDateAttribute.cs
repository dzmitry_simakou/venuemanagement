﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VM.Web.DataAnnotations
{
    public class FutureDateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value is DateTime)
            {
                if ((DateTime)value < DateTime.Now)
                {
                    return new ValidationResult("Date and time in the past");
                }
            }

            return null;
        }
    }
}