﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common.WebHost;
using VM.Web.Configuration;
using VM.Web.Modules;

namespace VM.Web
{
    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();
            PathConfig.Configure();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            LoadModules(kernel);

            return kernel;
        }

        private void LoadModules(IKernel kernel)
        {
            var modules = new NinjectModule[]
            {
                new WebInfrastructureModule(),
                new ServiceModule(),
                new AutoMapperModule()
            };

            kernel.Load(modules);
        }
    }
}