﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Configuration;

namespace VM.Web.Configuration
{
    public class PathConfig
    {
        private static string BaseDirectory => AppDomain.CurrentDomain.BaseDirectory;

        private static string PickUpDirectory => (ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection)?.SpecifiedPickupDirectory.PickupDirectoryLocation;

        private static void ConfigurePickupDirectory()
        {
            var fullPickUpDirectory = Path.Combine(BaseDirectory, PickUpDirectory);
            if (!Directory.Exists(fullPickUpDirectory))
            {
                Directory.CreateDirectory(Path.Combine(BaseDirectory, PickUpDirectory));
            }
        }

        public static void Configure()
        {
            ConfigurePickupDirectory();
        }
    }
}