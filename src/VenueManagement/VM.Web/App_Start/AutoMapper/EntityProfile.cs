﻿using AutoMapper;
using VM.DataAccess.Entities;
using VM.Services.DTO;
using VM.Web.Infrastructure.Authentication;

namespace VM.Web.AutoMapper
{
    public class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<Area, AreaDTO>().ReverseMap();
            CreateMap<BasketItem, BasketItemDTO>().ReverseMap();
            CreateMap<Event, EventDTO>().ReverseMap();
            CreateMap<EventArea, EventAreaDTO>().ReverseMap();
            CreateMap<EventSeat, EventSeatDTO>().ReverseMap();
            CreateMap<Layout, LayoutDTO>().ReverseMap();
            CreateMap<Order, OrderDTO>().ReverseMap();
            CreateMap<OrderSeat, OrderSeatDTO>().ReverseMap();
            CreateMap<Role, RoleDTO>().ReverseMap();
            CreateMap<Seat, SeatDTO>().ReverseMap();
            CreateMap<Venue, VenueDTO>().ReverseMap();
            CreateMap<User, IdentityUser>().ReverseMap();

            CreateMap<IdentityUser, Authentication.DTO.UserDTO>().ReverseMap();
            CreateMap<Authentication.DTO.UserDTO, User>();
        }
    }
}