﻿using System.Web;
using VM.Authentication.Clients;

namespace VM.Web
{
    public class TokenProvider : ITokenProvider
    {
        public string GetToken()
        {
            if (HttpContext.Current.Request.Cookies["access_token"] != null)
            {
                return HttpContext.Current.Request.Cookies["access_token"].Value;
            }

            return string.Empty;
        }
    }
}