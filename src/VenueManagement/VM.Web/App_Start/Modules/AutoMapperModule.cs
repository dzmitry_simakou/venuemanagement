﻿using AutoMapper;
using Ninject.Modules;
using Ninject.Web.Common;
using VM.Web.AutoMapper;

namespace VM.Web.Modules
{
    internal class AutoMapperModule : NinjectModule
    {
        private IConfigurationProvider GetAutoMapperConfiguration()
        {
            return new MapperConfiguration(config =>
            {
                config.AddProfile<EntityProfile>();
            });
        }

        public override void Load()
        {
            Kernel.Bind<IMapper, Mapper>().ToMethod(mapper => new Mapper(GetAutoMapperConfiguration())).InRequestScope();
        }
    }
}