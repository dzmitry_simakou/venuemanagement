﻿using System.Web;
using Microsoft.Owin.Security;
using Ninject.Modules;
using Ninject.Web.Common;
using VM.Web.Infrastructure.Authentication.JWT;
using VM.Web.Infrastructure.Authentication.JWT.Interfaces;
using VM.Web.Infrastructure.Authentication.Managers;
using VM.Web.Infrastructure.Mailing;
using VM.Web.Infrastructure.Mailing.Interfaces;

namespace VM.Web.Modules
{
    internal class WebInfrastructureModule : NinjectModule
    {
        public void LoadJwtManager()
        {
            Kernel.Bind<IJwtTokenManager>().To<JwtTokenManager>().InRequestScope();
        }

        public void LoadIdentity()
        {
            Kernel.Bind<IAuthenticationManager>().ToMethod(manager => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
            Kernel.Bind<UserManager>().ToSelf().InRequestScope();
            Kernel.Bind<SignInManager>().ToSelf().InRequestScope();
        }

        public void LoadClients()
        {
            Kernel.Bind<IMailClient>().To<MailClient>().InRequestScope();
        }

        public override void Load()
        {
            LoadJwtManager();
            LoadIdentity();
            LoadClients();
        }
    }
}