﻿using System.Web;
using VM.Services.Clients.Interfaces;

namespace VM.Web
{
    public class CredentialsProvider : ICredentialsProvider
    {
        public string GetPassword()
        {
            if(HttpContext.Current.Request.Cookies["access_token"] != null)
            {
                return HttpContext.Current.Request.Cookies["access_token"].Value;
            }

            return string.Empty;
        }

        public string GetUserName()
        {
            if (HttpContext.Current.User != null)
            {
                return HttpContext.Current.User.Identity.Name;
            }

            return string.Empty;
        }
    }
}