﻿using Ninject.Modules;
using Ninject.Web.Common;
using System.Configuration;
using VM.Authentication.Clients;
using VM.Authentication.Clients.Interfaces;
using VM.Services.Clients;
using VM.Services.Contracts;
using ITokenClient = VM.Authentication.Clients.Interfaces.ITokenClient;

namespace VM.Web.Modules
{
    internal class ServiceModule : NinjectModule
    {
        private readonly string _orderEndpoint = ConfigurationManager.AppSettings["OrderServiceEndpoint"];
        private readonly string _basketEndpoint = ConfigurationManager.AppSettings["BasketServiceEndpoint"];
        private readonly string _eventEndpoint = ConfigurationManager.AppSettings["EventServiceEndpoint"];
        private readonly string _unsecuredEventEndpoint = ConfigurationManager.AppSettings["UnsecuredEventServiceEndpoint"];
        private readonly string _venueEndpoint = ConfigurationManager.AppSettings["VenueServiceEndpoint"];

        public void LoadServices()
        {
            Kernel.Bind<IUserClient>().ToMethod(client => new UserClient(new TokenProvider())).InRequestScope();
            Kernel.Bind<IRoleClient>().ToMethod(client => new RoleClient(new TokenProvider())).InRequestScope();
            Kernel.Bind<ITokenClient>().To<TokenClient>().InRequestScope();

            Kernel.Bind<IOrderServiceContract>().ToMethod(client => new OrderServiceClient(new CredentialsProvider(), _orderEndpoint)).InRequestScope();
            Kernel.Bind<IBasketServiceContract>().ToMethod(client => new BasketServiceClient(new CredentialsProvider(), _basketEndpoint)).InRequestScope();
            Kernel.Bind<IEventServiceContract>().ToMethod(client => new EventServiceClient(new CredentialsProvider(), _eventEndpoint)).InRequestScope();
            Kernel.Bind<IAnonymousEventServiceContract>().ToMethod(client => new AnonymousEventServiceClient(_unsecuredEventEndpoint)).InRequestScope();
            Kernel.Bind<IVenueServiceContract>().ToMethod(client => new VenueServiceClient(new CredentialsProvider(), _venueEndpoint)).InRequestScope();
        }

        public override void Load()
        {
            LoadServices();
        }
    }
}