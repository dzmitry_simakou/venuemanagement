﻿using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using VM.Web.Infrastructure.Authentication.JWT;

[assembly: OwinStartup(typeof(VM.Web.Startup))]

namespace VM.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new JwtCookieAuthenticationProvider(),
                CookieHttpOnly = true,
                CookieSecure = CookieSecureOption.Always
            });
        }
    }
}