﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;

namespace VM.Web.Helpers
{
    public static class SupportedLanguages
    {
        public static KeyValuePair<string,string> English => new KeyValuePair<string, string>("en", "English");

        public static KeyValuePair<string, string> Russian => new KeyValuePair<string, string>("ru", "Russian");

        public static KeyValuePair<string, string> Belarusian => new KeyValuePair<string, string>("be", "Belarusian");

        public static IDictionary<string, string> AsDictionary()
        {
            return new Dictionary<string, string>
            {
                { English.Key, English.Value },
                { Russian.Key, Russian.Value },
                { Belarusian.Key, Belarusian.Value }
            };
        }

        public static SelectList AsSelectList()
        {
            return new SelectList(AsDictionary(), "Key", "Value");
        }

        public static string GetRouteConstraints()
        {
            var values = string.Join("|", English.Key, Russian.Key, Belarusian.Key);
            return string.Join("|", values);
        }
    }

    public static class TimeZoneInfos
    {
        private static readonly string DefaultTimeZoneId = "GMT Standard Time";

        public static TimeZoneInfo Local => TimeZoneInfo.Local;

        public static TimeZoneInfo Default => TimeZoneInfo.FindSystemTimeZoneById(DefaultTimeZoneId);

        /// <summary>
        /// TimeZoneInfo.GetSystemTimeZones() transformed into Dictionary
        /// </summary>
        /// <returns>Dictionary of key = TimeZoneInfo.Id and value = TimeZoneInfo.DisplayName.</returns>
        public static IDictionary<string, string> AsDictionary()
        {
            var dictionary = new Dictionary<string, string>();
            foreach(var timeZone in TimeZoneInfo.GetSystemTimeZones())
            {
                dictionary.Add(timeZone.Id, timeZone.DisplayName);
            }

            return dictionary;
        }

        public static SelectList AsSelectList()
        {
            return new SelectList(AsDictionary(), "Key", "Value");
        }
    }
}