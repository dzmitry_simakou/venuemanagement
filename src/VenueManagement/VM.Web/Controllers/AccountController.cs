﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VM.Authentication.Clients.Interfaces;
using VM.DataAccess.Entities;
using VM.Web.Models.Account;
using VM.Web.App_LocalResources;
using VM.Services.Contracts;
using AutoMapper;
using VM.Services.DTO;
using System.Collections.Generic;

namespace VM.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly IOrderServiceContract _orderService;
        private readonly ITokenClient _tokenClient;
        private readonly IMapper _mapper;

        private void SignIn(string token)
        {
            const bool isPersistent = true;
            const bool rememberBrowser = true;

            SignInManager.SignInAsync(token, isPersistent, rememberBrowser);
        }

        public AccountController(IOrderServiceContract orderService,
            ITokenClient tokenClient,
            IMapper mapper)
        {
            _orderService = orderService;
            _tokenClient = tokenClient;
            _mapper = mapper;
        }

        public async Task<ActionResult> UpdateAccount()
        {
            var user = await UserManager.FindByNameAsync(UserName);
            var model = new UpdateAccountModel(user);
            
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateAccount(UpdateAccountModel model)
        {
            var modelUser = model.GetUser();
            if (!model.HasEmptyPasswords)
            {
                var user = await UserManager.FindByNameAsync(UserName);
                if (user == null)
                {
                    ModelState.AddModelError("OldPassword", Resources.WrongOldPasswordLabel);
                    return View(model);
                }

                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                
                await UserManager.ChangePasswordAsync(UserId, model.OldPassword, model.NewPassword);

                SignInManager.AuthenticationManager.SignOut();
            }
            else
            {
                await UserManager.UpdateAsync(modelUser);

                SignInManager.AuthenticationManager.SignOut();
            }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult History()
        {
            var orders = _mapper.Map<IEnumerable<OrderDTO>, IEnumerable<Order>>(_orderService.GetOrders(UserId));
            var orderSeats = _mapper.Map<IEnumerable<OrderSeatDTO>, IEnumerable<OrderSeat>>(_orderService.GetOrderSeats(UserId));
            var model = new AccountHistoryModel { Orders = orders, OrderSeats = orderSeats };

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var token = await _tokenClient.GetToken(model.UserName, model.Password);
            if (token == null)
            {
                ModelState.AddModelError("UserValidation", Resources.UserValidationMessage);

                return View();
            }

            SignIn(token);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logoff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegistrationModel model)
        {          
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var token = await _tokenClient.RegisterUser(model.GetUser(), model.Password); 
            
            SignIn(token);
            
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> AddToBalance(UpdateAccountModel model)
        {
            var user = await UserManager.FindByNameAsync(UserName);

            user.Balance += model.GetBalanceToAdd();

            await UserManager.UpdateAsync(user);

            return RedirectToAction("UpdateAccount", "Account");
        }

    }
}