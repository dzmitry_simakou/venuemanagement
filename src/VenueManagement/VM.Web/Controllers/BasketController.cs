﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using VM.DataAccess.Entities;
using VM.Extensions;
using VM.Services.Contracts;
using VM.Services.DTO;
using VM.Web.App_LocalResources;
using VM.Web.Infrastructure.Mailing.Interfaces;
using VM.Web.Models.Basket;

namespace VM.Web.Controllers
{
    public class BasketController : BaseController
    {
        private readonly IBasketServiceContract _basketService;
        private readonly IOrderServiceContract _orderService;
        private readonly IMailClient _mailClient;
        private readonly IMapper _mapper;

        private BasketModel GetBasketModel()
        {
            var seats = _mapper.Map<IEnumerable<BasketItemDTO>, IEnumerable<BasketItem>>(_basketService.GetCurrentBasketItems(UserId)).ToList();
            var summary = _basketService.GetBasketSummaryPrice(UserId);
            var user = UserManager.FindByName(UserName);

            var model = new BasketModel
            {
                Balance = user.Balance,
                BalanceLabel = user.Balance.ToPriceString(GetUserCulture()),
                Summary = summary,
                SummaryLabel = summary.ToPriceString(GetUserCulture()),
                Seats = seats
            };

            if(seats.Any())
            {
                model.BasketExpiryDate = seats.First().ExpiryDate;
            }

            return model;
        }

        public BasketController(IBasketServiceContract basketService, 
            IOrderServiceContract orderService,
            IMailClient mailClient,
            IMapper mapper)
        {
            _basketService = basketService;
            _orderService = orderService;
            _mailClient = mailClient;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            _basketService.ClearExpiredBasketItems(UserId);
            var basketModel = GetBasketModel();

            return View(basketModel);
        }

        [HttpPost]
        public ActionResult ConfirmBasket()
        {
            var currentBasketItems = _mapper.Map<IEnumerable<BasketItemDTO>, IEnumerable<BasketItem>>(_basketService.GetCurrentBasketItems(UserId)).ToList();
            if (!currentBasketItems.Any())
            {
                var basketModel = GetBasketModel();
                ModelState.AddModelError("BasketExpired", Resources.BasketItemsExpiredErrorMessage);
                return View("Index", basketModel);
            }

            var user = UserManager.FindByName(UserName);
            var summary = _basketService.GetBasketSummaryPrice(UserId);

            _orderService.CreateOrderByUserIdAndPrice(UserId, summary);
            _mailClient.SendConfirmation(user, currentBasketItems, summary);
            _basketService.ClearBasketItems(UserId);

            return RedirectToAction(actionName: "", controllerName: "");
        }

        [HttpPost]
        public ActionResult RemoveFromBasket(int link)
        {
            _basketService.DeleteBasketItemById(link);
            _basketService.UpdateBasketItemExpiry(UserId);

            return RedirectToAction("Index");
        }
    }
}