﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VM.DataAccess.Entities;
using VM.Services.Contracts;
using VM.Services.DTO;
using VM.Web.App_LocalResources;
using VM.Web.Models.Event;

namespace VM.Web.Controllers
{
    public class EventController : BaseController
    {
        private readonly IEventServiceContract _eventService;
        private readonly IBasketServiceContract _basketService;
        private readonly IVenueServiceContract _venueService;
        private readonly IAnonymousEventServiceContract _anonymousEventService;
        private readonly IMapper _mapper;

        private void AddItemToBasket(int id)
        {
            var link = new BasketItem { SeatId = id, UserId = UserId };

            _basketService.UpdateBasketItemExpiry(UserId);
            _basketService.CreateBasketItem(_mapper.Map<BasketItem, BasketItemDTO>(link));
        }

        public EventController(IAnonymousEventServiceContract anonymousEventService, 
            IEventServiceContract eventService, 
            IBasketServiceContract basketService,
            IVenueServiceContract venueService,
            IMapper mapper)
        {
            _eventService = eventService;
            _basketService = basketService;
            _venueService = venueService;
            _anonymousEventService = anonymousEventService;
            _mapper = mapper;
        }

        public ActionResult Events()
        {
            var events = _mapper.Map<IEnumerable<EventDTO>, IEnumerable<Event>>(_anonymousEventService.GetEvents().ToList());
            ViewData["Events"] = events;

            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var layouts = _mapper.Map<IEnumerable<LayoutDTO>, IEnumerable<Layout>>(_venueService.GetLayouts().ToList());
            ViewBag.Layouts = layouts;

            return View();
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(CreateEventModel model)
        {
            if (!ModelState.IsValid)
            {
                var layouts = _mapper.Map<IEnumerable<LayoutDTO>, IEnumerable<Layout>>(_venueService.GetLayouts().ToList());
                ViewBag.Layouts = layouts;

                return View();
            }

            _eventService.CreateEvent(_mapper.Map<Event, EventDTO>(model.GetEvent()));

            return RedirectToAction("Events", "Event");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Update(int eventId)
        {
            var ev = _mapper.Map<EventDTO, Event>(_eventService.GetEvent(eventId));
            var eventAreas = _mapper.Map<IEnumerable<EventAreaDTO>, IEnumerable<EventArea>>(_eventService.GetEventAreasByEventId(eventId));
            var layouts = _mapper.Map<IEnumerable<LayoutDTO>, IEnumerable<Layout>>(_venueService.GetLayouts());

            var model = new UpdateEventModel(ev, eventAreas, layouts);
            if (_eventService.HasPurchasedSeats(eventId))
            {
                ModelState.AddModelError("HasPurchasedSeatsError", Resources.HasPurchasedSeatsError);
            }

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Update(UpdateEventModel model)
        {
            if (ModelState.IsValid)
            {
                foreach (var areaModel in model.EventAreas)
                {
                    _eventService.UpdateEventArea(_mapper.Map<EventArea, EventAreaDTO>(areaModel.ToEventArea()));
                }
                _eventService.UpdateEvent(_mapper.Map<Event, EventDTO>(model.GetEvent()));

                return RedirectToAction("Events", "Event");
            }

            var layouts = _mapper.Map<IEnumerable<LayoutDTO>, IEnumerable<Layout>>(_venueService.GetLayouts());
            model.SetLayouts(layouts);
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int eventId)
        {
            var ev = _mapper.Map<EventDTO, Event>(_eventService.GetEvent(eventId));
            var eventAreas = _mapper.Map<IEnumerable<EventAreaDTO>, IEnumerable<EventArea>>(_eventService.GetEventAreasByEventId(eventId));
            var layouts = _mapper.Map<IEnumerable<LayoutDTO>, IEnumerable<Layout>>(_venueService.GetLayouts());

            var model = new UpdateEventModel(ev, eventAreas, layouts);
            if (_eventService.HasPurchasedSeats(eventId))
            {
                ModelState.AddModelError("HasPurchasedSeatsError", Resources.HasPurchasedSeatsError);
            }

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Delete(UpdateEventModel model)
        {
            _eventService.DeleteEventByEventId(model.Id);

            return RedirectToAction("Events", "Event");
        }

        public ActionResult Areas(int eventId)
        {
            var ev = _anonymousEventService.GetEvent(eventId);
            var areas = _mapper.Map<IEnumerable<EventAreaDTO>, IEnumerable<EventArea>>(_anonymousEventService.GetEventAreasByEventId(eventId).ToList()).ToList();
            var model = new AreaPlanModel { Areas = areas, X = areas.Max(seat => seat.CoordX), Y = areas.Max(seat => seat.CoordY), EventName = ev.Name, EventDescription = ev.Description };

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult SeatPlanPartial(int eventAreaId)
        {
            var eventSeats = _mapper.Map<IEnumerable<EventSeatDTO>, IEnumerable<EventSeat>>(_anonymousEventService.GetAvailableEventSeats(eventAreaId));
            var eventArea = _mapper.Map<EventAreaDTO, EventArea>(_anonymousEventService.GetEventArea(eventAreaId));
            var maxCoordX = _anonymousEventService.GetEventSeatsByEventAreaId(eventArea.Id).Max(seat => seat.Number);
            var maxCoordY = _anonymousEventService.GetEventSeatsByEventAreaId(eventArea.Id).Max(seat => seat.Row);

            var model = new SeatPlanModel { Seats = eventSeats, EventArea = eventArea, MaxCoordX = maxCoordX, MaxCoordY = maxCoordY };

            return PartialView(model);
        }

        public ActionResult Seats(int eventAreaId)
        {
            _anonymousEventService.ClearExpiredBasketItemsByEventAreaId(eventAreaId);

            return View(eventAreaId);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Seats(int eventAreaId, int id)
        {
            _anonymousEventService.ClearExpiredBasketItemsByEventAreaId(eventAreaId);
            if (!_basketService.IsBasketItemAvailable(id))
            {
                AddItemToBasket(id);
            }
            else
            {
                ModelState.AddModelError("AlreadyAdded", Resources.SeatAlreadyInBasketErrorMessage);

                return View(eventAreaId);
            }

            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}