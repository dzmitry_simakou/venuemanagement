﻿using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using VM.Web.Helpers;
using VM.Web.Infrastructure.Authentication.Managers;

namespace VM.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        [Inject]
        public UserManager UserManager { get; set; }

        [Inject]
        public SignInManager SignInManager { get; set; }

        protected int UserId => int.Parse(HttpContext.User.Identity.GetUserId());

        protected string UserName => HttpContext.User.Identity.GetUserName();

        protected IEnumerable<Claim> UserClaims => ((ClaimsPrincipal)Thread.CurrentPrincipal).Claims;

        protected TimeZoneInfo GetUserTimeZone()
        {
            var timeZoneClaim = UserClaims.SingleOrDefault(claim => claim.Type == UserClaimTypes.TimeZone);
            if (timeZoneClaim != null)
            {
                return TimeZoneInfo.FindSystemTimeZoneById(timeZoneClaim.Value);
            }

            return TimeZoneInfos.Local;
        } 

        protected CultureInfo CurrentLanguage { get; private set; }

        protected CultureInfo GetUserCulture()
        {
            var localityClaim = UserClaims.SingleOrDefault(claim => claim.Type == ClaimTypes.Locality);
            if(localityClaim == null)
            {
                return null;
            }

            return new CultureInfo(localityClaim.Value);
        }

        private void SetThreadCulture(CultureInfo culture)
        {
            ViewBag.Culture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
        }

        private void SetTimeZone(TimeZoneInfo timeZoneInfo)
        {
            ViewBag.TimeZone = timeZoneInfo;
        }

        private void SetCulture(RequestContext requestContext)
        {
            var userCulture = GetUserCulture();

            if (requestContext.RouteData.Values["culture"] is string routeCultureValue)
            {
                var routeCulture = new CultureInfo(routeCultureValue);
                CurrentLanguage = routeCulture;
                SetThreadCulture(routeCulture);
            }
            else if (userCulture != null && !Equals(userCulture, CurrentLanguage))
            {
                CurrentLanguage = userCulture;
                SetThreadCulture(userCulture);
            }

            SetTimeZone(GetUserTimeZone());
        }

        protected override void Initialize(RequestContext requestContext)
        {
            SetCulture(requestContext);
            base.Initialize(requestContext);
        }
    }
}