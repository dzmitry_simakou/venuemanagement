﻿using NUnit.Framework;
using System.Linq;
using FluentAssertions;
using System;
using VM.DataAccess.Entities;

namespace VM.IntegrationTests
{
    [TestFixture]
    public class EventServiceTests : BaseTests
    { 
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateSnapshot();
        }

        [Test]
        public void CreateEventSeat_EventSeatCreated()
        {
            // Arrange
            var service = EventService;
            var createdEventSeat = new EventSeat { Id = 41, EventAreaId = 2, Row = 10, Number = 10 };

            // Act
            service.CreateEventSeat(createdEventSeat);

            // Assert
            service.GetEventSeatsByEventAreaId(createdEventSeat.EventAreaId).Single(eventSeat => eventSeat.Id == createdEventSeat.Id).Should().Be(createdEventSeat);
        }

        [Test]
        public void UpdateEventSeat_EventSeatUpdated()
        {
            // Arrange
            var service = EventService;
            var updatedEventSeat = new EventSeat { Id = 20, EventAreaId = 2, Row = 10, Number = 10 };

            // Act
            service.UpdateEventSeat(updatedEventSeat);

            // Assert
            service.GetEventSeatsByEventAreaId(updatedEventSeat.EventAreaId).Single(eventSeat => eventSeat.Id == updatedEventSeat.Id).Should().Be(updatedEventSeat);
        }

        [Test]
        public void DeleteEventSeat_EventSeatDeleted()
        {
            // Arrange
            var service = EventService;
            var deletedEventSeat = new EventSeat { Id = 20, EventAreaId = 2, Row = 2, Number = 5 };

            // Act
            service.DeleteEventSeat(deletedEventSeat);

            // Assert
            service.GetEventSeatsByEventAreaId(deletedEventSeat.EventAreaId).Any(eventSeat => eventSeat.Equals(deletedEventSeat)).Should().BeFalse();
        }

        [Test]
        public void CreateEventArea_EventAreaCreated()
        {
            // Arrange
            var service = EventService;
            var createdEventArea = new EventArea { Id = 5, EventId = 1, Description = "Description", CoordX = 5, CoordY = 5, Price = 0 };

            // Act
            service.CreateEventArea(createdEventArea);

            // Assert
            service.GetEventAreasByEventId(createdEventArea.EventId).Single(eventArea => eventArea.Id == createdEventArea.Id).Should().Be(createdEventArea);
        }

        [Test]
        public void UpdateEventArea_EventAreaUpdated()
        {
            // Arrange
            var service = EventService;
            var updatedEventArea = new EventArea { Id = 2, EventId = 1, Description = "New Description", CoordX = 5, CoordY = 5, Price = 1400 };

            // Act
            service.UpdateEventArea(updatedEventArea);

            // Assert
            service.GetEventAreasByEventId(updatedEventArea.EventId).Single(eventArea => eventArea.Id == updatedEventArea.Id).Should().Be(updatedEventArea);
        }

        [Test]
        public void DeleteEventArea_EventAreaDeleted()
        {
            // Arrange
            var service = EventService;
            var deletedEventArea = new EventArea { Id = 2, EventId = 1, Description = "New Description", CoordX = 5, CoordY = 5, Price = 1400 };

            // Act
            service.DeleteEventArea(deletedEventArea);

            // Assert
            service.GetEventAreasByEventId(deletedEventArea.EventId).Any(eventArea => eventArea.Equals(deletedEventArea)).Should().BeFalse();
        }

        [Test]
        public void CreateEvent_EventCreated()
        {
            // Arrange
            var service = EventService;
            var createdEvent = new Event { Id = 3, Description = "Description for Event 3", LayoutId = 1, Name = "Event Name 3", Date = new DateTime(2020, 12, 12, 19, 00, 00), Img = "http://test.com/nothing"};

            // Act
            service.CreateEvent(createdEvent);

            // Assert
            service.GetEventsByLayoutId(createdEvent.LayoutId).Single(ev => ev.Id == createdEvent.Id).Should().Be(createdEvent);
        }

        [Test]
        public void UpdateEventWithOldLayoutId_OnlyEventUpdated()
        {
            // Arrange
            var service = EventService;
            var updatedEvent = new Event { Id = 1, Description = "Updated Description for Event 1", LayoutId = 1, Name = "Updated Event Name 1", Date = new DateTime(2020, 12, 12, 19, 00, 00), Img = "http://test.com/nothing" };

            // Act
            service.UpdateEvent(updatedEvent);

            // Assert
            service.GetEventsByLayoutId(updatedEvent.LayoutId).Single(ev => ev.Id == updatedEvent.Id).Should().Be(updatedEvent);
        }

        [Test]
        public void UpdateEventWithNewLayoutId_EventWithSeatsAndAreasUpdated()
        {
            // Arrange
            var service = EventService;
            var updatedEvent = new Event { Id = 1, Description = "Updated Description for Event 1", LayoutId = 2, Name = "Updated Event Name 1", Date = new DateTime(2020, 12, 12, 19, 00, 00), Img = "http://test.com/nothing" };

            // Act
            service.UpdateEvent(updatedEvent);

            // Assert
            service.GetEventsByLayoutId(updatedEvent.LayoutId).Single(ev => ev.Id == updatedEvent.Id).Should().Be(updatedEvent);
        }

        [Test]
        public void DeleteEvent_EventDeleted()
        {
            // Arrange
            var service = EventService;
            var deletedEvent = new Event { Id = 1, LayoutId = 1, Name = "Event Name 1", Description = "Event Description 1", Date = new DateTime(2020, 11, 11, 11, 00, 00), Img = "http://test.com/nothing" };

            // Act
            service.DeleteEvent(deletedEvent);

            // Assert
            service.GetEventsByLayoutId(deletedEvent.LayoutId).Any(ev => ev.Equals(deletedEvent)).Should().BeFalse();
        }

        [TearDown]
        public void TearDown()
        {
            RestoreSnapshot();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            RemoveSnapshot();
        }
    }
}