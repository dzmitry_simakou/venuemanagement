﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using VM.BusinessLogic.Services;
using VM.DataAccess.Context;
using VM.DataAccess.Entities;

namespace VM.IntegrationTests
{
    [TestFixture]
    public class VenueServiceTests : BaseTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CreateSnapshot();
        }

        [Test]
        public void CreateSeat_CreateSeat_SeatCreated()
        {
            // Arrange
            var service = VenueService;
            var createdSeat = new Seat { Id = 121, AreaId = 6, Row = 10, Number = 10 };
            var testSeat = new Seat { Id = 121, AreaId = 6, Row = -10, Number = -10 };

            // Act
            service.CreateSeat(createdSeat);

            // Assert
            service.GetSeats(createdSeat.AreaId).Single(seat => seat.Id == createdSeat.Id).Should().Be(createdSeat);
        }

        [Test]
        public void UpdateSeat_UpdateSeat_SeatUpdated()
        {
            // Arrange
            var service = VenueService;
            var updatedSeat = new Seat { Id = 120, AreaId = 2, Row = 10, Number = 10 };

            // Act
            service.UpdateSeat(updatedSeat);

            // Assert
            service.GetSeats(updatedSeat.AreaId).Single(seat => seat.Id == updatedSeat.Id).Should().Be(updatedSeat);
        }

        [Test]
        public void DeleteSeat_DeleteSeat_SeatDeleted()
        {
            // Arrange
            var service = VenueService;
            var deletedSeat = new Seat { Id = 120, AreaId = 12, Row = 2, Number = 5 };

            // Act
            service.DeleteSeat(deletedSeat);

            // Assert
            service.GetSeats(deletedSeat.AreaId).Any(seat => seat.Equals(deletedSeat)).Should().BeFalse();
        }

        [Test]
        public void GetSeats_GetAllSeatsByAreaId_ReturnsAllSeats()
        {
            // Arrange
            var service = VenueService;
            var areaId = 2;
            var seats = new List<Seat> 
            {
                new Seat { Id = 11, AreaId = areaId, Row = 1, Number = 1 },
                new Seat { Id = 12, AreaId = areaId, Row = 1, Number = 2 },
                new Seat { Id = 13, AreaId = areaId, Row = 1, Number = 3 },
                new Seat { Id = 14, AreaId = areaId, Row = 1, Number = 4 },
                new Seat { Id = 15, AreaId = areaId, Row = 1, Number = 5 },
                new Seat { Id = 16, AreaId = areaId, Row = 2, Number = 1 },
                new Seat { Id = 17, AreaId = areaId, Row = 2, Number = 2 },
                new Seat { Id = 18, AreaId = areaId, Row = 2, Number = 3 },
                new Seat { Id = 19, AreaId = areaId, Row = 2, Number = 4 },
                new Seat { Id = 20, AreaId = areaId, Row = 2, Number = 5 }
            };

            // Act
            var serviceSeats = service.GetSeats(areaId);

            // Assert
            serviceSeats.Should().Equal(seats);
        }

        [Test]
        public void CreateArea_AreaCreated()
        {
            // Arrange
            var service = VenueService;
            var createdArea = new Area { Id = 13, LayoutId = 1, Description = "Description", CoordX = 5, CoordY = 5 };

            // Act
            service.CreateArea(createdArea);

            // Assert
            service.GetAreas(createdArea.LayoutId).Single(area => area.Id == createdArea.Id).Should().Be(createdArea);
        }

        [Test]
        public void UpdateArea_AreaUpdated()
        {
            // Arrange
            var service = VenueService;
            var updatedArea = new Area { Id = 11, LayoutId = 1, Description = "New Description", CoordX = 5, CoordY = 5 };

            // Act
            service.UpdateArea(updatedArea);

            // Assert
            service.GetAreas(updatedArea.LayoutId).Single(area => area.Id == updatedArea.Id).Should().Be(updatedArea);
        }

        [Test]
        public void DeleteArea_AreaDeleted()
        {
            // Arrange
            var service = VenueService;
            var deletedArea = new Area { Id = 12, LayoutId = 12, Description = "Description for Area 2 of Layout 2", CoordX = 1, CoordY = 2 };

            // Act
            service.DeleteArea(deletedArea);

            // Assert
            service.GetAreas(deletedArea.LayoutId).Any(area => area.Equals(deletedArea)).Should().BeFalse();
        }

        [Test]
        public void GetAreas_GetAllAreasByLayoutId_ReturnsAllAreas()
        {
            // Arrange
            var service = VenueService;
            var layoutId = 2;
            var areas = new List<Area>
            {
                new Area { Id = 3, LayoutId = layoutId, Description = "Description for Area 1 of Layout 2", CoordX = 1, CoordY = 1 },
                new Area { Id = 4, LayoutId = layoutId, Description = "Description for Area 2 of Layout 2", CoordX = 1, CoordY = 2 }
            };

            // Act
            var serviceAreas = service.GetAreas(layoutId);

            // Assert
            serviceAreas.Should().Equal(areas);
        }

        [Test]
        public void CreateLayout_LayoutCreated()
        {
            // Arrange
            var service = VenueService;
            var createdLayout = new Layout { Id = 7, VenueId = 1, Name = "Name", Description = "Description" };

            // Act
            service.CreateLayout(createdLayout);

            // Assert
            service.GetLayoutsByVenueId(createdLayout.VenueId).Single(layout => layout.Id == createdLayout.Id).Should().Be(createdLayout);
        }

        [Test]
        public void UpdateLayout_LayoutUpdated()
        {
            // Arrange
            var service = VenueService;
            var updatedLayout = new Layout { Id = 3, VenueId = 1, Name = "New name", Description = "New description" };

            // Act
            service.UpdateLayout(updatedLayout);

            // Assert
            service.GetLayoutsByVenueId(updatedLayout.VenueId).Single(layout => layout.Id == updatedLayout.Id).Should().Be(updatedLayout);
        }

        [Test]
        public void DeleteLayout_LayoutDeleted()
        {
            // Arrange
            var service = VenueService;
            var deletedLayout = new Layout { Id = 6, VenueId = 3, Name = "Layout Name 2", Description = "Description for Layout 2 of Venue 2" };

            // Act
            service.DeleteLayout(deletedLayout);

            // Assert
            service.GetLayoutsByVenueId(deletedLayout.VenueId).Any(layout => layout.Equals(deletedLayout)).Should().BeFalse();
        }

        [Test]
        public void GetLayouts_GetAllLayoutsByVenueId_ReturnsAllLayouts()
        {
            // Arrange
            var service = VenueService;
            var venueId = 2;
            var layouts = new List<Layout>
            {
                new Layout { Id = 3, VenueId = 2, Description = "Description for Layout 3 of Venue 2", Name = "Layout Name 3" },
                new Layout { Id = 4, VenueId = 2, Description = "Description for Layout 4 of Venue 2", Name = "Layout Name 4" }
            };

            // Act
            var serviceLayouts = service.GetLayoutsByVenueId(venueId);

            // Assert
            serviceLayouts.Should().Equal(layouts);
        }

        [Test]
        public void CreateVenue_VenueCreated()
        {
            // Arrange
            var service = VenueService;
            var createdVenue = new Venue { Id = 4, Name = "Name", Description = "Description", Phone = "333-333-333", Address = "Address str." };

            // Act
            service.CreateVenue(createdVenue);

            // Assert
            service.GetVenues().Single(venue => venue.Id == createdVenue.Id).Should().Be(createdVenue);
        }

        [Test]
        public void UpdateVenue_VenueUpdated()
        {
            // Arrange
            var service = VenueService;
            var updatedVenue = new Venue { Id = 2, Name = "New name", Description = "New description", Phone = "333-333-333", Address = "Address str." };

            // Act
            service.UpdateVenue(updatedVenue);

            // Assert
            service.GetVenues().Single(layout => layout.Id == updatedVenue.Id).Should().Be(updatedVenue);
        }

        [Test]
        public void DeleteVenue_LayoutVenue()
        {
            // Arrange
            var service = VenueService;
            var deletedVenue = new Venue { Id = 3, Name = "Venue 3", Description = "Description for Venue 3", Phone = "555-555-555", Address = "Example Street 3" };

            // Act
            service.DeleteVenue(deletedVenue);

            // Assert
            service.GetVenues().Any(layout => layout.Equals(deletedVenue)).Should().BeFalse();
        }

        [Test]
        public void GetVenues_GetAllVenues_ReturnsAllVenues()
        {
            // Arrange
            var service = VenueService;
            var venues = new List<Venue>
            {
                new Venue { Id = 1, Description = "Description for Venue 1", Name = "Venue 1", Address = "Example Street 1", Phone = "333-333-333" },
                new Venue { Id = 2, Description = "Description for Venue 2", Name = "Venue 2", Address = "Example Street 2", Phone = "444-444-444" },
                new Venue { Id = 3, Description = "Description for Venue 3", Name = "Venue 3", Address = "Example Street 3", Phone = "555-555-555" }
            };

            // Act
            var serviceVenues = service.GetVenues();

            // Assert
            serviceVenues.Should().Equal(venues);
        }

        [TearDown]
        public void TearDown()
        {
            RestoreSnapshot();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            RemoveSnapshot();
        }
    }
}