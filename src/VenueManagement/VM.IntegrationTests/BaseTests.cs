﻿using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using VM.BusinessLogic.Interfaces;
using VM.BusinessLogic.Services;
using VM.DataAccess.Context;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Repositories;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.IntegrationTests
{
    public class BaseTests
    {
        protected readonly string ConnectionString = ConfigurationManager.ConnectionStrings["VmDatabase"].ConnectionString;
        protected readonly string CreateSnapshotQuery = @"CREATE DATABASE VmSnapshot on (name='VM.Database', filename='" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\VmSnapshot.dat') AS SNAPSHOT of [VM.Database];";
        protected readonly string RestoreSnapshotQuery = @"USE master; RESTORE DATABASE [VM.Database] FROM DATABASE_SNAPSHOT = 'VmSnapshot' WITH REPLACE,RECOVERY";
        protected readonly string RemoveSnapshotQuery = @"DROP DATABASE IF EXISTS VmSnapshot";

        protected IDbContext DbContext => new DbContext(ConnectionString);
        protected IEventRepository EventRepository => new EventRepository(DbContext);
        protected IEventAreaRepository EventAreaRepository => new EventAreaRepository(DbContext);
        protected IEventSeatRepository EventSeatRepository => new EventSeatRepository(DbContext);
        protected IVenueRepository VenueRepository => new VenueRepository(DbContext);
        protected ILayoutRepository LayoutRepository => new LayoutRepository(DbContext);
        protected IAreaRepository AreaRepository => new AreaRepository(DbContext);
        protected ISeatRepository SeatRepository => new SeatRepository(DbContext);
        protected IBasketItemRepository BasketItemRepository => new BasketItemRepository(DbContext);
        protected IEventService EventService => new EventService(EventRepository, EventAreaRepository, EventSeatRepository, LayoutRepository, AreaRepository, SeatRepository, BasketItemRepository);
        protected IVenueService VenueService => new VenueService(SeatRepository, AreaRepository, LayoutRepository, VenueRepository);

        protected void CreateSnapshot()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(CreateSnapshotQuery, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        protected void RestoreSnapshot()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(RestoreSnapshotQuery, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        protected void RemoveSnapshot()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(RemoveSnapshotQuery, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}