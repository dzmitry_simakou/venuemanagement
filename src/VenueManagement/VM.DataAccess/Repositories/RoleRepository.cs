﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public RoleRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@name", role.Name)
            };

            _context.ExecuteSqlCommand(Procedures.CreateRole, parameters);
        }

        public Task CreateAsync(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@name", role.Name)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.CreateRole, parameters);
        }

        public void Update(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@id", role.Id),
                new SqlParameter("@name", role.Name)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateRole, parameters);
        }

        public Task UpdateAsync(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@id", role.Id),
                new SqlParameter("@name", role.Name)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.UpdateRole, parameters);
        }

        public void Delete(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@id", role.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteRole, parameters);
        }

        public Task DeleteAsync(Role role)
        {
            object[] parameters =
            {
                new SqlParameter("@id", role.Id)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.DeleteRole, parameters);
        }

        public IEnumerable<Role> GetAll()
        {
            return _context.ExecuteSqlQuery<Role>(Queries.GetAllRoles);
        }

        public Task<IEnumerable<Role>> GetAllAsync()
        {
            return _context.ExecuteSqlQueryAsync<Role>(Queries.GetAllRoles);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~RoleRepository()
        {
            Dispose(false);
        }
    }
}