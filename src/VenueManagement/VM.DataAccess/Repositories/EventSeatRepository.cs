﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class EventSeatRepository : IEventSeatRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public EventSeatRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(EventSeat eventSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@eventAreaId", eventSeat.EventAreaId),
                new SqlParameter("@row", eventSeat.Row),
                new SqlParameter("@number", eventSeat.Number),
                new SqlParameter("@orderId", (object)eventSeat.OrderId ?? DBNull.Value)
            };

            _context.ExecuteSqlCommand(Procedures.CreateEventSeat, parameters);
        }

        public void Update(EventSeat eventSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", eventSeat.Id),
                new SqlParameter("@eventAreaId", eventSeat.EventAreaId),
                new SqlParameter("@row", eventSeat.Row),
                new SqlParameter("@number", eventSeat.Number),
                new SqlParameter("@orderId", (object)eventSeat.OrderId ?? DBNull.Value)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateEventSeat, parameters);
        }

        public void Delete(EventSeat eventSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", eventSeat.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteEventSeat, parameters);
        }

        public IEnumerable<EventSeat> GetAll()
        {
            return _context.ExecuteSqlQuery<EventSeat>(Queries.GetAllEventSeats);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~EventSeatRepository()
        {
            Dispose(false);
        }
    }
}