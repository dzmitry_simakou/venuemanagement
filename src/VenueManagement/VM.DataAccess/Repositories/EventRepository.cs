﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class EventRepository : IEventRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public EventRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Event ev)
        {
            object[] parameters =
            {
                new SqlParameter("@name", ev.Name),
                new SqlParameter("@description", ev.Description),
                new SqlParameter("@layoutId", ev.LayoutId),
                new SqlParameter("@date", ev.Date),
                new SqlParameter("@img", ev.Img)
            };

            _context.ExecuteSqlCommand(Procedures.CreateEvent, parameters);
        }

        public void Update(Event ev)
        {
            object[] parameters =
            {
                new SqlParameter("@id", ev.Id),
                new SqlParameter("@name", ev.Name),
                new SqlParameter("@description", ev.Description),
                new SqlParameter("@layoutId", ev.LayoutId),
                new SqlParameter("@date", ev.Date),
                new SqlParameter("@img", ev.Img)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateEvent, parameters);
        }

        public void Delete(Event ev)
        {
            object[] parameters =
            {
                new SqlParameter("@id", ev.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteEvent, parameters);
        }

        public IEnumerable<Event> GetAll()
        {
            return _context.ExecuteSqlQuery<Event>(Queries.GetAllEvents);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~EventRepository()
        {
            Dispose(false);
        }
    }
}