﻿using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using VM.DataAccess.Context.Interfaces;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace VM.DataAccess.Repositories
{
    public class VenueRepository : IVenueRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public VenueRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Venue venue)
        {
            object[] parameters = 
            {
                new SqlParameter("@name", venue.Name),
                new SqlParameter("@description", venue.Description),
                new SqlParameter("@address", venue.Address),
                new SqlParameter("@phone", venue.Phone)
            };

            _context.ExecuteSqlCommand(Procedures.CreateVenue, parameters);
        }

        public void Update(Venue venue)
        {
            object[] parameters =
            {
                new SqlParameter("@id", venue.Id),
                new SqlParameter("@name", venue.Name),
                new SqlParameter("@description", venue.Description),
                new SqlParameter("@address", venue.Address),
                new SqlParameter("@phone", venue.Phone)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateVenue, parameters);
        }

        public void Delete(Venue venue)
        {
            object[] parameters =
            {
                new SqlParameter("@id", venue.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteVenue, parameters);
        }

        public IEnumerable<Venue> GetAll()
        {
            return _context.ExecuteSqlQuery<Venue>(Queries.GetAllVenues);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~VenueRepository()
        {
            Dispose(false);
        }
    }
}