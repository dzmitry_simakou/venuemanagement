﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public OrderRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Order order)
        {
            object[] parameters =
            {
                new SqlParameter("@userId", order.UserId),
                new SqlParameter("@date", order.Date),
                new SqlParameter("@price", order.Price)
            };

            _context.ExecuteSqlCommand(Procedures.CreateOrder, parameters);
        }

        public void Update(Order order)
        {
            object[] parameters =
            {
                new SqlParameter("@id", order.Id),
                new SqlParameter("@userId", order.UserId),
                new SqlParameter("@date", order.Date),
                new SqlParameter("@price", order.Price)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateOrder, parameters);
        }

        public void Delete(Order order)
        {
            object[] parameters =
            {
                new SqlParameter("@id", order.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteOrder, parameters);
        }

        public IEnumerable<Order> GetAll()
        {
            return _context.ExecuteSqlQuery<Order>(Queries.GetAllOrders);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~OrderRepository()
        {
            Dispose(false);
        }
    }
}