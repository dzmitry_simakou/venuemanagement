﻿namespace VM.DataAccess.Repositories
{
    internal static class Queries
    {
        public const string GetAllEvents = @"select * from [dbo].[Event]";

        public const string GetAllEventAreas = @"select * from dbo.EventArea";

        public const string GetAllEventSeats = @"select * from dbo.EventSeat";

        public const string GetAllVenues = @"select * from dbo.Venue";

        public const string GetAllLayouts = @"select * from dbo.Layout";

        public const string GetAllAreas = @"select * from dbo.Area";

        public const string GetAllSeats = @"select * from dbo.Seat";

        public const string GetAllOrders = @"select * from dbo.[Order]";

        public const string GetAllOrderSeats = @"select * from dbo.OrderSeat";

        public const string GetAllBasketItems = @"select * from dbo.BasketItem";

        public const string GetAllRoles = @"select * from dbo.Role";

        public const string GetAllUserRoles = @"select * from dbo.UserRole";

        public const string GetAllUsers = @"select * from [dbo].[User]";
    }
}