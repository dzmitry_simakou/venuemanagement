﻿using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using System.Collections.Generic;
using System;

namespace VM.DataAccess.Repositories
{
    public class LayoutRepository : ILayoutRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public LayoutRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Layout layout)
        {
            object[] parameters =
            {
                new SqlParameter("@venueId", layout.VenueId),
                new SqlParameter("@description", layout.Description),
                new SqlParameter("@name", layout.Name)
            };

            _context.ExecuteSqlCommand(Procedures.CreateLayout, parameters);
        }

        public void Update(Layout layout)
        {

            object[] parameters =
            {
                new SqlParameter("@id", layout.Id),
                new SqlParameter("@venueId", layout.VenueId),
                new SqlParameter("@description", layout.Description),
                new SqlParameter("@name", layout.Name)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateLayout, parameters);
        }

        public void Delete(Layout layout)
        {
            object[] parameters =
            {
                new SqlParameter("@id", layout.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteLayout, parameters);
        }

        public IEnumerable<Layout> GetAll()
        {
            return _context.ExecuteSqlQuery<Layout>(Queries.GetAllLayouts);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~LayoutRepository()
        {
            Dispose(false);
        }
    }
}