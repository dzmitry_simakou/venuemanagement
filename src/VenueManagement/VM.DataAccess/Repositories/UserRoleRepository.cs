﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public UserRoleRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(UserRole userRole)
        {
            object[] parameters =
            {
                new SqlParameter("@userId", userRole.UserId),
                new SqlParameter("@roleId", userRole.RoleId)
            };

            _context.ExecuteSqlCommand(Procedures.CreateUserRole, parameters);
        }

        public Task CreateAsync(UserRole userRole)
        {
            object[] parameters =
            {
                new SqlParameter("@userId", userRole.UserId),
                new SqlParameter("@roleId", userRole.RoleId)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.CreateUserRole, parameters);
        }

        public void Update(UserRole userRole)
        {
            object[] parameters =
            {
                new SqlParameter("@id", userRole.Id),
                new SqlParameter("@userId", userRole.UserId),
                new SqlParameter("@roleId", userRole.RoleId)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateUserRole, parameters);
        }

        public Task UpdateAsync(UserRole userRole)
        {
            object[] parameters =
            {
                new SqlParameter("@id", userRole.Id),
                new SqlParameter("@userId", userRole.UserId),
                new SqlParameter("@roleId", userRole.RoleId)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.UpdateUserRole, parameters);
        }

        public void Delete(UserRole userRole)
        {
            object[] parameters = 
            {
                new SqlParameter("@id", userRole.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteUserRole, parameters);
        }

        public Task DeleteAsync(UserRole userRole)
        {
            object[] parameters =
            {
                new SqlParameter("@id", userRole.Id)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.DeleteUserRole, parameters);
        }

        public IEnumerable<UserRole> GetAll()
        {
            return _context.ExecuteSqlQuery<UserRole>(Queries.GetAllUserRoles).AsQueryable();
        }

        public Task<IEnumerable<UserRole>> GetAllAsync()
        {
            return _context.ExecuteSqlQueryAsync<UserRole>(Queries.GetAllUserRoles);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~UserRoleRepository()
        {
            Dispose(false);
        }
    }
}