﻿using System;
using System.Collections.Generic;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface IRepository<T> : IDisposable where T : IEntity
    {
        void Create(T obj);

        void Update(T obj);

        void Delete(T obj);

        IEnumerable<T> GetAll();
    }
}