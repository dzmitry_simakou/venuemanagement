﻿using VM.DataAccess.Entities;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface ISeatRepository : IRepository<Seat>
    {

    }
}