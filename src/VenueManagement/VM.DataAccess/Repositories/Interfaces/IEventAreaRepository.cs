﻿using VM.DataAccess.Entities;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface IEventAreaRepository : IRepository<EventArea>
    {

    }
}