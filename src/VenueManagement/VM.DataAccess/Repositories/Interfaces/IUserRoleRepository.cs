﻿using VM.DataAccess.Entities;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface IUserRoleRepository : IRepositoryAsync<UserRole>
    {

    }
}