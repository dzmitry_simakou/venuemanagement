﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VM.DataAccess.Entities.Interfaces;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface IRepositoryAsync<T> : IRepository<T> where T : IEntity
    {
        Task CreateAsync(T obj);

        Task UpdateAsync(T obj);

        Task DeleteAsync(T obj);

        Task<IEnumerable<T>> GetAllAsync();
    }
}