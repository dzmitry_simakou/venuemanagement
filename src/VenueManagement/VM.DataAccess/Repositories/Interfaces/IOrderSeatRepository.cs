﻿using VM.DataAccess.Entities;

namespace VM.DataAccess.Repositories.Interfaces
{
    public interface IOrderSeatRepository : IRepository<OrderSeat>
    {

    }
}