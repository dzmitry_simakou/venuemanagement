﻿using System.Collections.Generic;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using VM.DataAccess.Context.Interfaces;
using System.Data.SqlClient;
using System;

namespace VM.DataAccess.Repositories
{
    public class SeatRepository : ISeatRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public SeatRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Seat seat)
        {
            object[] parameters =
            {
                new SqlParameter("@areaId", seat.AreaId),
                new SqlParameter("@row", seat.Row),
                new SqlParameter("@number", seat.Number)
            };

            _context.ExecuteSqlCommand(Procedures.CreateSeat, parameters);
        }

        public void Update(Seat seat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", seat.Id),
                new SqlParameter("@areaId", seat.AreaId),
                new SqlParameter("@row", seat.Row),
                new SqlParameter("@number", seat.Number)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateSeat, parameters);
        }

        public void Delete(Seat seat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", seat.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteSeat, parameters);
        }

        public IEnumerable<Seat> GetAll()
        {
            return _context.ExecuteSqlQuery<Seat>(Queries.GetAllSeats);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~SeatRepository()
        {
            Dispose(false);
        }
    }
}