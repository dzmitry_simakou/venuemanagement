﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class BasketItemRepository : IBasketItemRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public BasketItemRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(BasketItem basketItem)
        {
            object[] parameters =
            {
                new SqlParameter("@userId", basketItem.UserId),
                new SqlParameter("@seatId", basketItem.SeatId),
                new SqlParameter("@expiryDate", basketItem.ExpiryDate)
            };

            _context.ExecuteSqlCommand(Procedures.CreateBasketItem, parameters);
        }

        public void Update(BasketItem basketItem)
        {
            object[] parameters =
            {
                new SqlParameter("@id", basketItem.Id),
                new SqlParameter("@userId", basketItem.UserId),
                new SqlParameter("@seatId", basketItem.SeatId),
                new SqlParameter("@expiryDate", basketItem.ExpiryDate)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateBasketItem, parameters);
        }

        public void Delete(BasketItem basketItem)
        {
            object[] parameters =
            {
                new SqlParameter("@id", basketItem.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteBasketItem, parameters);
        }

        public IEnumerable<BasketItem> GetAll()
        {
            return _context.ExecuteSqlQuery<BasketItem>(Queries.GetAllBasketItems);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~BasketItemRepository()
        {
            Dispose(false);
        }
    }
}