﻿namespace VM.DataAccess.Repositories
{
    internal class Procedures
    {
        public const string CreateEvent = @"CreateEvent @name, @description, @layoutId, @date, @img";

        public const string UpdateEvent = @"UpdateEvent @id, @name, @description, @layoutId, @date, @img";

        public const string DeleteEvent = @"DeleteEvent @id";

        public const string CreateEventArea = @"CreateEventArea @eventId, @description, @coordX, @coordY, @price";

        public const string UpdateEventArea = @"UpdateEventArea @id, @eventId, @description, @coordX, @coordY, @price";

        public const string DeleteEventArea = @"DeleteEventArea @id";

        public const string CreateEventSeat = @"CreateEventSeat @eventAreaId, @row, @number, @orderId";

        public const string UpdateEventSeat = @"UpdateEventSeat @id, @eventAreaId, @row, @number, @orderId";

        public const string DeleteEventSeat = @"DeleteEventSeat @id";

        public const string CreateVenue = @"CreateVenue @name, @description, @address, @phone";

        public const string UpdateVenue = @"UpdateVenue @id, @name, @description, @address, @phone";

        public const string DeleteVenue = @"DeleteVenue @id";

        public const string CreateLayout = @"CreateLayout @venueId, @description, @name";

        public const string UpdateLayout = @"UpdateLayout @id, @venueId, @description, @name";

        public const string DeleteLayout = @"DeleteLayout @id";

        public const string CreateArea = @"CreateArea @layoutId, @description, @coordX, @coordY";

        public const string UpdateArea = @"UpdateArea @id, @layoutId, @description, @coordX, @coordY";

        public const string DeleteArea = @"DeleteArea @id";

        public const string CreateSeat = @"CreateSeat @areaId, @row, @number";

        public const string UpdateSeat = @"UpdateSeat @id, @areaId, @row, @number";

        public const string DeleteSeat = @"DeleteSeat @id";

        public const string CreateOrder = @"CreateOrder @userId, @date, @price";

        public const string UpdateOrder = @"UpdateOrder @id, @userId, @date, @price";

        public const string DeleteOrder = @"DeleteOrder @id";

        public const string CreateOrderSeat = @"CreateOrderSeat @orderId, @eventSeatId, @price";

        public const string UpdateOrderSeat = @"UpdateOrderSeat @id, @orderId, @eventSeatId, @price";

        public const string DeleteOrderSeat = @"DeleteOrderSeat @id";

        public const string CreateRole = @"CreateRole @name";

        public const string UpdateRole = @"UpdateRole @id, @name";

        public const string DeleteRole = @"DeleteRole @id";

        public const string CreateUserRole = @"CreateUserRole @userId, @roleId";

        public const string UpdateUserRole = @"UpdateUserRole @id, @userId, @roleId";

        public const string DeleteUserRole = @"DeleteUserRole @id";

        public const string CreateUser = @"CreateUser @userName, @passwordHash, @lang, @timeZone, @firstName, @lastName, @email";

        public const string UpdateUser = @"UpdateUser @id, @userName, @passwordHash, @balance, @lang, @timeZone, @firstName, @lastName, @email";

        public const string DeleteUser = @"DeleteUser @id";

        public const string CreateBasketItem = @"CreateBasketItem @userId, @seatId, @expiryDate";

        public const string UpdateBasketItem = @"UpdateBasketItem @id, @userId, @seatId, @expiryDate";

        public const string DeleteBasketItem = @"DeleteBasketItem @id";
    }
}