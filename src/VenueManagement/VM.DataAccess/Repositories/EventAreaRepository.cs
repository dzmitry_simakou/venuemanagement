﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class EventAreaRepository : IEventAreaRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public EventAreaRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(EventArea eventArea)
        {
            object[] parameters =
            {
                new SqlParameter("@eventId", eventArea.EventId),
                new SqlParameter("@description", eventArea.Description),
                new SqlParameter("@coordX", eventArea.CoordX),
                new SqlParameter("@coordY", eventArea.CoordY),
                new SqlParameter("@price", eventArea.Price)
            };

            _context.ExecuteSqlCommand(Procedures.CreateEventArea, parameters);
        }

        public void Update(EventArea eventArea)
        {
            object[] parameters =
            {
                new SqlParameter("@id", eventArea.Id),
                new SqlParameter("@eventId", eventArea.EventId),
                new SqlParameter("@description", eventArea.Description),
                new SqlParameter("@coordX", eventArea.CoordX),
                new SqlParameter("@coordY", eventArea.CoordY),
                new SqlParameter("@price", eventArea.Price)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateEventArea, parameters);
        }

        public void Delete(EventArea eventArea)
        {
            object[] parameters =
            {
                new SqlParameter("@id", eventArea.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteEventArea, parameters);
        }

        public IEnumerable<EventArea> GetAll()
        {
            return _context.ExecuteSqlQuery<EventArea>(Queries.GetAllEventAreas);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~EventAreaRepository()
        {
            Dispose(false);
        }
    }
}