﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public UserRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@userName", user.UserName),
                new SqlParameter("@passwordHash", user.PasswordHash),
                new SqlParameter("@lang", user.Lang),
                new SqlParameter("@timeZone", user.TimeZone),
                new SqlParameter("@firstName", user.FirstName),
                new SqlParameter("@lastName", user.LastName),
                new SqlParameter("@email", user.Email),
            };

            _context.ExecuteSqlCommand(Procedures.CreateUser, parameters);
        }

        public Task CreateAsync(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@userName", user.UserName),
                new SqlParameter("@passwordHash", user.PasswordHash),
                new SqlParameter("@lang", user.Lang),
                new SqlParameter("@timeZone", user.TimeZone),
                new SqlParameter("@firstName", user.FirstName),
                new SqlParameter("@lastName", user.LastName),
                new SqlParameter("@email", user.Email),
            };

            return _context.ExecuteSqlCommandAsync(Procedures.CreateUser, parameters);
        }

        public void Update(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@id", user.Id),
                new SqlParameter("@userName", user.UserName),
                new SqlParameter("@passwordHash", user.PasswordHash),
                new SqlParameter("@balance", user.Balance),
                new SqlParameter("@lang", user.Lang),
                new SqlParameter("@timeZone", user.TimeZone),
                new SqlParameter("@firstName", user.FirstName),
                new SqlParameter("@lastName", user.LastName),
                new SqlParameter("@email", user.Email),
            };

            _context.ExecuteSqlCommand(Procedures.UpdateUser, parameters);
        }

        public Task UpdateAsync(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@id", user.Id),
                new SqlParameter("@userName", user.UserName),
                new SqlParameter("@passwordHash", user.PasswordHash),
                new SqlParameter("@balance", user.Balance),
                new SqlParameter("@lang", user.Lang),
                new SqlParameter("@timeZone", user.TimeZone),
                new SqlParameter("@firstName", user.FirstName),
                new SqlParameter("@lastName", user.LastName),
                new SqlParameter("@email", user.Email),
            };

            return _context.ExecuteSqlCommandAsync(Procedures.UpdateUser, parameters);
        }

        public void Delete(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@id", user.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteUser, parameters);
        }

        public Task DeleteAsync(User user)
        {
            object[] parameters =
            {
                new SqlParameter("@id", user.Id)
            };

            return _context.ExecuteSqlCommandAsync(Procedures.DeleteUser, parameters);
        }

        public IEnumerable<User> GetAll()
        {
            return _context.ExecuteSqlQuery<User>(Queries.GetAllUsers);
        }

        public Task<IEnumerable<User>> GetAllAsync()
        {
            return _context.ExecuteSqlQueryAsync<User>(Queries.GetAllUsers);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~UserRepository()
        {
            Dispose(false);
        }
    }
}