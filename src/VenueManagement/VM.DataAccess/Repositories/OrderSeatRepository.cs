﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VM.DataAccess.Context.Interfaces;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;

namespace VM.DataAccess.Repositories
{
    public class OrderSeatRepository : IOrderSeatRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public OrderSeatRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(OrderSeat orderSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@orderId", orderSeat.OrderId),
                new SqlParameter("@eventSeatId", orderSeat.EventSeatId),
                new SqlParameter("@price", orderSeat.Price)
            };

            _context.ExecuteSqlCommand(Procedures.CreateOrderSeat, parameters);
        }

        public void Update(OrderSeat orderSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", orderSeat.Id),
                new SqlParameter("@orderId", orderSeat.OrderId),
                new SqlParameter("@eventSeatId", orderSeat.EventSeatId),
                new SqlParameter("@price", orderSeat.Price)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateOrderSeat, parameters);
        }

        public void Delete(OrderSeat orderSeat)
        {
            object[] parameters =
            {
                new SqlParameter("@id", orderSeat.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteOrderSeat, parameters);
        }

        public IEnumerable<OrderSeat> GetAll()
        {
            return _context.ExecuteSqlQuery<OrderSeat>(Queries.GetAllOrderSeats);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~OrderSeatRepository()
        {
            Dispose(false);
        }
    }
}