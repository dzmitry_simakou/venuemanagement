﻿using System.Data.SqlClient;
using VM.DataAccess.Entities;
using VM.DataAccess.Repositories.Interfaces;
using VM.DataAccess.Context.Interfaces;
using System.Collections.Generic;
using System;

namespace VM.DataAccess.Repositories
{
    public class AreaRepository : IAreaRepository
    {
        private bool _disposed;
        private readonly IDbContext _context;

        public AreaRepository(IDbContext context)
        {
            _context = context;
        }

        public void Create(Area area)
        {
            object[] parameters =
            {
                new SqlParameter("@layoutId", area.LayoutId),
                new SqlParameter("@description", area.Description),
                new SqlParameter("@coordX", area.CoordX),
                new SqlParameter("@coordY", area.CoordY)
            };

            _context.ExecuteSqlCommand(Procedures.CreateArea, parameters);
        }

        public void Update(Area area)
        {
            object[] parameters =
            {
                new SqlParameter("@id", area.Id),
                new SqlParameter("@layoutId", area.LayoutId),
                new SqlParameter("@description", area.Description),
                new SqlParameter("@coordX", area.CoordX),
                new SqlParameter("@coordY", area.CoordY)
            };

            _context.ExecuteSqlCommand(Procedures.UpdateArea, parameters);
        }

        public void Delete(Area area)
        {
            object[] parameters =
            {
                new SqlParameter("@id", area.Id)
            };

            _context.ExecuteSqlCommand(Procedures.DeleteArea, parameters);
        }

        public IEnumerable<Area> GetAll()
        {
            return _context.ExecuteSqlQuery<Area>(Queries.GetAllAreas);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }

        ~AreaRepository()
        {
            Dispose(false);
        }
    }
}