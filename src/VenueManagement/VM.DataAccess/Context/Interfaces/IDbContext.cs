﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace VM.DataAccess.Context.Interfaces
{
    public interface IDbContext : IDisposable
    {
        IDbConnection GetConnection();

        IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] parameters);

        Task<IEnumerable<T>> ExecuteSqlQueryAsync<T>(string query, params object[] parameters);

        int ExecuteSqlCommand(string query, params object[] parameters);

        Task<int> ExecuteSqlCommandAsync(string query, params object[] parameters);

        int SaveChanges();
    }
}