﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VM.DataAccess.Context.Interfaces;

namespace VM.DataAccess.Context
{
    public class DbContext : System.Data.Entity.DbContext, IDbContext
    {
        public DbContext(string connectionString) : base(connectionString)
        {

        }

        public IDbConnection GetConnection()
        {
            return Database.Connection;
        }

        public IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] parameters)
        {
            return Database.SqlQuery<T>(query, parameters).ToList();
        }

        public async Task<IEnumerable<T>> ExecuteSqlQueryAsync<T>(string query, params object[] parameters)
        {
            return await Database.SqlQuery<T>(query, parameters).ToListAsync();
        }

        public int ExecuteSqlCommand(string query, params object[] parameters)
        {
            return Database.ExecuteSqlCommand(query, parameters);
        }

        public Task<int> ExecuteSqlCommandAsync(string query, params object[] parameters)
        {
            return Database.ExecuteSqlCommandAsync(query, parameters);
        }
    }
}