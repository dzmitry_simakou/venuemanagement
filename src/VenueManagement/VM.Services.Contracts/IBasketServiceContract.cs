﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.DTO;

namespace VM.Services.Contracts
{
    [ServiceContract]
    public interface IBasketServiceContract : IContract
    {
        [OperationContract]
        void CreateBasketItem(BasketItemDTO basket);

        [OperationContract]
        void DeleteBasketItemById(int id);

        [OperationContract]
        void DeleteBasketItem(BasketItemDTO basket);

        [OperationContract]
        int GetBasketSummaryPrice(int userId);

        [OperationContract]
        bool IsBasketItemAvailable(int id);

        [OperationContract]
        IEnumerable<BasketItemDTO> GetCurrentBasketItems(int userId);

        [OperationContract]
        IEnumerable<BasketItemDTO> GetExpiredBasketItems(int userId);

        [OperationContract]
        void ClearExpiredBasketItems(int userId);

        [OperationContract]
        void ClearBasketItems(int userId);

        [OperationContract]
        void UpdateBasketItemExpiry(int userId);

        [OperationContract]
        void UpdateBasketItemExpiryWithExpiryDate(int userId, DateTime expiry);
    }
}