﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.Contracts.Faults;
using VM.Services.DTO;

namespace VM.Services.Contracts
{
    [ServiceContract]
    public interface IEventServiceContract : IAnonymousEventServiceContract
    {
        [OperationContract]
        [FaultContract(typeof(OverlapFault))]
        void CreateEventSeat(EventSeatDTO seat);

        [OperationContract]
        [FaultContract(typeof(OverlapFault))]
        void UpdateEventSeat(EventSeatDTO seat);

        [OperationContract]
        void DeleteEventSeat(EventSeatDTO seat);

        [OperationContract]
        IEnumerable<EventSeatDTO> GetEventSeats();

        [OperationContract]
        [FaultContract(typeof(NotUniqueFault))]
        [FaultContract(typeof(OverlapFault))]
        void CreateEventArea(EventAreaDTO area);

        [OperationContract]
        [FaultContract(typeof(NotUniqueFault))]
        [FaultContract(typeof(OverlapFault))]
        void UpdateEventArea(EventAreaDTO area);

        [OperationContract]
        void DeleteEventArea(EventAreaDTO area);

        [OperationContract]
        IEnumerable<EventAreaDTO> GetEventAreas();

        [OperationContract]
        [FaultContract(typeof(EventDateFault))]
        [FaultContract(typeof(NoSeatsFault))]
        void CreateEvent(EventDTO ev);

        [OperationContract]
        [FaultContract(typeof(EventDateFault))]
        [FaultContract(typeof(NoSeatsFault))]
        [FaultContract(typeof(AlreadyPaidFault))]
        void UpdateEvent(EventDTO ev);

        [OperationContract]
        [FaultContract(typeof(AlreadyPaidFault))]
        void DeleteEvent(EventDTO ev);

        [OperationContract]
        [FaultContract(typeof(AlreadyPaidFault))]
        void DeleteEventByEventId(int eventId);

        [OperationContract]
        bool HasPurchasedSeats(int eventId);
    }
}