﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.DTO;

namespace VM.Services.Contracts
{
    [ServiceContract]
    public interface IVenueServiceContract : IContract
    {
        [OperationContract]
        void CreateSeat(SeatDTO seat);

        [OperationContract]
        void UpdateSeat(SeatDTO seat);

        [OperationContract]
        void DeleteSeat(SeatDTO seat);

        [OperationContract]
        IEnumerable<SeatDTO> GetSeats(int areaId);

        [OperationContract]
        void CreateArea(AreaDTO area);

        [OperationContract]
        void UpdateArea(AreaDTO area);

        [OperationContract]
        void DeleteArea(AreaDTO area);

        [OperationContract]
        IEnumerable<AreaDTO> GetAreas(int layoutId);

        [OperationContract]
        void CreateLayout(LayoutDTO layout);

        [OperationContract]
        void UpdateLayout(LayoutDTO layout);

        [OperationContract]
        void DeleteLayout(LayoutDTO layout);

        [OperationContract]
        IEnumerable<LayoutDTO> GetLayouts();

        [OperationContract]
        IEnumerable<LayoutDTO> GetLayoutsByVenueId(int venueId);

        [OperationContract]
        void CreateVenue(VenueDTO venue);

        [OperationContract]
        void UpdateVenue(VenueDTO venue);

        [OperationContract]
        void DeleteVenue(VenueDTO venue);

        [OperationContract]
        IEnumerable<VenueDTO> GetVenues();
    }
}