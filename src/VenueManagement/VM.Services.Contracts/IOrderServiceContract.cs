﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.DTO;

namespace VM.Services.Contracts
{
    [ServiceContract]
    public interface IOrderServiceContract : IContract
    {
        [OperationContract]
        IEnumerable<OrderDTO> GetOrders(int userId);

        [OperationContract]
        void CreateOrderByUserIdAndPrice(int userId, int price);

        [OperationContract]
        void CreateOrder(OrderDTO order);

        [OperationContract]
        IEnumerable<OrderSeatDTO> GetOrderSeats(int userId);
    }
}