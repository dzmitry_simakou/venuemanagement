﻿using System.Runtime.Serialization;

namespace VM.Services.Contracts.Faults
{
    [DataContract]
    public class NoSeatsFault : IFault
    {

    }
}