﻿using System.Collections.Generic;
using System.ServiceModel;
using VM.Services.DTO;

namespace VM.Services.Contracts
{
    [ServiceContract]
    public interface IAnonymousEventServiceContract : IContract
    {
        [OperationContract]
        EventDTO GetEvent(int eventId);

        [OperationContract]
        IEnumerable<EventDTO> GetEvents();

        [OperationContract]
        IEnumerable<EventDTO> GetEventsByLayoutId(int layoutId);

        [OperationContract]
        void ClearExpiredBasketItemsByEventAreaId(int eventAreaId);

        [OperationContract]
        EventAreaDTO GetEventArea(int eventAreaId);

        [OperationContract]
        IEnumerable<EventAreaDTO> GetEventAreasByEventId(int eventId);

        [OperationContract]
        IEnumerable<EventSeatDTO> GetAvailableEventSeats(int eventAreaId);

        [OperationContract]
        IEnumerable<EventSeatDTO> GetEventSeatsByEventAreaId(int eventAreaId);
    }
}